/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define LIMIT_SWITCH2_Pin GPIO_PIN_13
#define LIMIT_SWITCH2_GPIO_Port GPIOC
#define PC14_Pin GPIO_PIN_14
#define PC14_GPIO_Port GPIOC
#define PC15_Pin GPIO_PIN_15
#define PC15_GPIO_Port GPIOC
#define PH0_Pin GPIO_PIN_0
#define PH0_GPIO_Port GPIOH
#define PH1_Pin GPIO_PIN_1
#define PH1_GPIO_Port GPIOH
#define ADC_Pin GPIO_PIN_0
#define ADC_GPIO_Port GPIOC
#define SPI2_MOSI_Pin GPIO_PIN_1
#define SPI2_MOSI_GPIO_Port GPIOC
#define SPI2_MISO_Pin GPIO_PIN_2
#define SPI2_MISO_GPIO_Port GPIOC
#define IR_INTERRUPT3_Pin GPIO_PIN_3
#define IR_INTERRUPT3_GPIO_Port GPIOC
#define IR_INTERRUPT3_EXTI_IRQn EXTI3_IRQn
#define IR_INTERRUPT1_Pin GPIO_PIN_0
#define IR_INTERRUPT1_GPIO_Port GPIOA
#define IR_INTERRUPT1_EXTI_IRQn EXTI0_IRQn
#define U3_1_2EN_Pin GPIO_PIN_1
#define U3_1_2EN_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LIMIT_SWITCH1_Pin GPIO_PIN_4
#define LIMIT_SWITCH1_GPIO_Port GPIOA
#define U2_2A_Pin GPIO_PIN_5
#define U2_2A_GPIO_Port GPIOA
#define U2_1A_Pin GPIO_PIN_6
#define U2_1A_GPIO_Port GPIOA
#define U2_1_2EN_Pin GPIO_PIN_7
#define U2_1_2EN_GPIO_Port GPIOA
#define LIMIT_SWITCH3_Pin GPIO_PIN_4
#define LIMIT_SWITCH3_GPIO_Port GPIOC
#define RGB_INPUT_0_Pin GPIO_PIN_5
#define RGB_INPUT_0_GPIO_Port GPIOC
#define LIMIT_SWITCH4_Pin GPIO_PIN_0
#define LIMIT_SWITCH4_GPIO_Port GPIOB
#define RF_INTERRUPT_Pin GPIO_PIN_1
#define RF_INTERRUPT_GPIO_Port GPIOB
#define RF_INTERRUPT_EXTI_IRQn EXTI1_IRQn
#define SPI_CE_Pin GPIO_PIN_2
#define SPI_CE_GPIO_Port GPIOB
#define U1_3A_Pin GPIO_PIN_10
#define U1_3A_GPIO_Port GPIOB
#define SPI_NSS_Pin GPIO_PIN_12
#define SPI_NSS_GPIO_Port GPIOB
#define U1_1A_Pin GPIO_PIN_13
#define U1_1A_GPIO_Port GPIOB
#define U1_3_4EN_Pin GPIO_PIN_14
#define U1_3_4EN_GPIO_Port GPIOB
#define IR_INTERRUPT2_Pin GPIO_PIN_15
#define IR_INTERRUPT2_GPIO_Port GPIOB
#define IR_INTERRUPT2_EXTI_IRQn EXTI15_10_IRQn
#define RGB_INPUT_1_Pin GPIO_PIN_6
#define RGB_INPUT_1_GPIO_Port GPIOC
#define ENCODEUR1_Pin GPIO_PIN_7
#define ENCODEUR1_GPIO_Port GPIOC
#define ENCODEUR1_EXTI_IRQn EXTI9_5_IRQn
#define RGB_INPUT_2_Pin GPIO_PIN_8
#define RGB_INPUT_2_GPIO_Port GPIOC
#define U2_4A_Pin GPIO_PIN_9
#define U2_4A_GPIO_Port GPIOC
#define U1_4A_Pin GPIO_PIN_8
#define U1_4A_GPIO_Port GPIOA
#define SPI2_SCK_Pin GPIO_PIN_9
#define SPI2_SCK_GPIO_Port GPIOA
#define U1_1_2EN_Pin GPIO_PIN_10
#define U1_1_2EN_GPIO_Port GPIOA
#define RGB_OUTPUT_S2_Pin GPIO_PIN_11
#define RGB_OUTPUT_S2_GPIO_Port GPIOA
#define RGB_OUTPUT_S3_Pin GPIO_PIN_12
#define RGB_OUTPUT_S3_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define U3_1A_Pin GPIO_PIN_10
#define U3_1A_GPIO_Port GPIOC
#define U3_2A_Pin GPIO_PIN_11
#define U3_2A_GPIO_Port GPIOC
#define ENCODEUR2_Pin GPIO_PIN_12
#define ENCODEUR2_GPIO_Port GPIOC
#define ENCODEUR2_EXTI_IRQn EXTI15_10_IRQn
#define U3_3A_Pin GPIO_PIN_2
#define U3_3A_GPIO_Port GPIOD
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define U1_2A_Pin GPIO_PIN_5
#define U1_2A_GPIO_Port GPIOB
#define LASER_INTERRUPT_Pin GPIO_PIN_6
#define LASER_INTERRUPT_GPIO_Port GPIOB
#define LASER_INTERRUPT_EXTI_IRQn EXTI9_5_IRQn
#define U3_3_4EN_Pin GPIO_PIN_7
#define U3_3_4EN_GPIO_Port GPIOB
#define U2_3A_Pin GPIO_PIN_8
#define U2_3A_GPIO_Port GPIOB
#define U2_3_4EN_Pin GPIO_PIN_9
#define U2_3_4EN_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
#define SPI_CSN_Pin GPIO_PIN_12
#define SPI_CSN_GPIO_Port GPIOB
/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
