var searchData=
[
  ['bartocode',['barToCode',['../class_rgb_scan_manager.html#ada4916770ff782205a309325aa351336',1,'RgbScanManager']]],
  ['battery',['battery',['../class_automation_manager.html#a993251dcfc3842dd78fb9d0d31df359d',1,'AutomationManager']]],
  ['batterymonitoring',['BatteryMonitoring',['../class_battery_monitoring.html#ab60a4647f4ceb0850ee40ea1ffa1b3f1',1,'BatteryMonitoring::BatteryMonitoring()'],['../class_battery_monitoring.html#a438d16a3224acc32a54c970436c90bf6',1,'BatteryMonitoring::BatteryMonitoring(ADC_HandleTypeDef *adc, float minTension, float maxTension)']]],
  ['batteryneedcharging',['batteryNeedCharging',['../class_battery_monitoring.html#afcab069b0012deca86881ad2f08ed3c5',1,'BatteryMonitoring']]]
];
