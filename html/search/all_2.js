var searchData=
[
  ['b',['B',['../_color_bar_8h.html#a111da81ae5883147168bbb8366377b10',1,'ColorBar.h']]],
  ['bartocode',['barToCode',['../class_rgb_scan_manager.html#ada4916770ff782205a309325aa351336',1,'RgbScanManager']]],
  ['battery',['battery',['../class_automation_manager.html#a993251dcfc3842dd78fb9d0d31df359d',1,'AutomationManager']]],
  ['batterymonitoring',['BatteryMonitoring',['../class_battery_monitoring.html',1,'BatteryMonitoring'],['../class_battery_monitoring.html#ab60a4647f4ceb0850ee40ea1ffa1b3f1',1,'BatteryMonitoring::BatteryMonitoring()'],['../class_battery_monitoring.html#a438d16a3224acc32a54c970436c90bf6',1,'BatteryMonitoring::BatteryMonitoring(ADC_HandleTypeDef *adc, float minTension, float maxTension)']]],
  ['batterymonitoring_2ecpp',['BatteryMonitoring.cpp',['../_battery_monitoring_8cpp.html',1,'']]],
  ['batterymonitoring_2ehpp',['BatteryMonitoring.hpp',['../_battery_monitoring_8hpp.html',1,'']]],
  ['batteryneedcharging',['batteryNeedCharging',['../class_battery_monitoring.html#afcab069b0012deca86881ad2f08ed3c5',1,'BatteryMonitoring']]],
  ['blue_5fcode',['BLUE_CODE',['../_color_codes_8h.html#ad86ac677734a5385ea84c0e7f0684746',1,'ColorCodes.h']]],
  ['blue_5fcode_5fmax_5fwl_5fb',['BLUE_CODE_MAX_WL_B',['../_color_codes_8h.html#a5c23375301cd2e4e98acfeda96b8c776',1,'ColorCodes.h']]],
  ['blue_5fcode_5fmax_5fwl_5fg',['BLUE_CODE_MAX_WL_G',['../_color_codes_8h.html#a2c3e29f58e1b96b329ceb184646c8ea4',1,'ColorCodes.h']]],
  ['blue_5fcode_5fmax_5fwl_5fr',['BLUE_CODE_MAX_WL_R',['../_color_codes_8h.html#a1bb11e88b7d07a8bb7dc1db48f14604c',1,'ColorCodes.h']]],
  ['blue_5fcode_5fmin_5fwl_5fb',['BLUE_CODE_MIN_WL_B',['../_color_codes_8h.html#a3d82e64fa3ac690fd676115379c07886',1,'ColorCodes.h']]],
  ['blue_5fcode_5fmin_5fwl_5fg',['BLUE_CODE_MIN_WL_G',['../_color_codes_8h.html#aeec77ddee3c7d26102858dfe0cd8fa1e',1,'ColorCodes.h']]],
  ['blue_5fcode_5fmin_5fwl_5fr',['BLUE_CODE_MIN_WL_R',['../_color_codes_8h.html#a87e1fdd45a98d5bcde11fa20115529e1',1,'ColorCodes.h']]],
  ['body_5flen',['BODY_LEN',['../_rf_packet_8h.html#a3245aea271b88db0de55f58278db20d5',1,'RfPacket.h']]]
];
