################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Src/System/SPI/SpiComm.cpp 

OBJS += \
./Src/System/SPI/SpiComm.o 

CPP_DEPS += \
./Src/System/SPI/SpiComm.d 


# Each subdirectory must supply rules for building sources it contributes
Src/System/SPI/%.o: ../Src/System/SPI/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU G++ Compiler'
	@echo $(PWD)
	arm-none-eabi-g++ -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16  -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Inc" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Clock" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/DigitalSensors" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Drivers/CMSIS/Include" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Clutches" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Hardware" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Motors" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/RF" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/RGB" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/System" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Utils" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Inc"  -O1 -Wall -fmessage-length=0 -c -fno-exceptions -fno-rtti -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


