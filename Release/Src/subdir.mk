################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/stm32f4xx_hal_msp.c \
../Src/stm32f4xx_it.c \
../Src/system_stm32f4xx.c 

CPP_SRCS += \
../Src/BatteryMonitoring.cpp \
../Src/MovementManager.cpp \
../Src/TheOneToRuleThemAll.cpp \
../Src/main.cpp 

OBJS += \
./Src/BatteryMonitoring.o \
./Src/MovementManager.o \
./Src/TheOneToRuleThemAll.o \
./Src/main.o \
./Src/stm32f4xx_hal_msp.o \
./Src/stm32f4xx_it.o \
./Src/system_stm32f4xx.o 

C_DEPS += \
./Src/stm32f4xx_hal_msp.d \
./Src/stm32f4xx_it.d \
./Src/system_stm32f4xx.d 

CPP_DEPS += \
./Src/BatteryMonitoring.d \
./Src/MovementManager.d \
./Src/TheOneToRuleThemAll.d \
./Src/main.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU G++ Compiler'
	@echo $(PWD)
	arm-none-eabi-g++ -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16  -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Inc" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Clock" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/DigitalSensors" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Drivers/CMSIS/Include" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Clutches" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Hardware" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Motors" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/RF" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/RGB" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/System" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Utils" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Inc"  -O1 -Wall -fmessage-length=0 -c -fno-exceptions -fno-rtti -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DUSE_HAL_DRIVER -DSTM32F446xx '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Inc" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Drivers/CMSIS/Include" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Clock" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Clutches" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Hardware" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/DigitalSensors" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Motors" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/RF" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/RGB" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/System" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Src/Utils" -I"C:/Users/jeanc/Documents/MercuryBot/Fetcher/Inc"  -O1 -Wall -fmessage-length=0 -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


