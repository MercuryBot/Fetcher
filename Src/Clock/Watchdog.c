/**
 * @file Watchdog.h
 * @brief Definition file generated by CubeMx for the Watchdog
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 */
#include "Watchdog.h"

IWDG_HandleTypeDef hiwdg;

/* IWDG init function */
void MX_IWDG_Init(void)
{
	hiwdg.Instance = IWDG;
	hiwdg.Init.Prescaler = IWDG_PRESCALER_32;
	hiwdg.Init.Reload = 4095;
	if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
	{
		//string str = __FILE__; char* temp = new char[str.length() + 1]; strcpy(temp, str.c_str()); _Error_Handler(temp, __LINE__);
		_Error_Handler(__FILE__, __LINE__);
	}
}
