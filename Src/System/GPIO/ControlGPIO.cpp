/**
* @file ControlGPIO.cpp
* @brief Definition file for the ControlGPIO class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 19th, 2018
*
* The ControlGPIO class provides control over basic GPIO pins and ports
*
*/

#include "ControlGPIO.hpp"

/**
 * @brief The class' base constructor
 */
ControlGPIO::ControlGPIO()
{
	m_hadc[0].Instance = ADC1;
	m_hadc[0].Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
	m_hadc[0].Init.Resolution = ADC_RESOLUTION_12B;
	m_hadc[0].Init.ScanConvMode = ENABLE;
	m_hadc[0].Init.ContinuousConvMode = ENABLE;
	m_hadc[0].Init.DiscontinuousConvMode = DISABLE;
	m_hadc[0].Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	m_hadc[0].Init.ExternalTrigConv = ADC_SOFTWARE_START;
	m_hadc[0].Init.DataAlign = ADC_DATAALIGN_RIGHT;
	m_hadc[0].Init.NbrOfConversion = 1;
	m_hadc[0].Init.DMAContinuousRequests = ENABLE;
	m_hadc[0].Init.EOCSelection = ADC_EOC_SEQ_CONV;
	m_hadc[0].State = 0x00;

	m_pwm[0] = *(new PWM{&htim1, 1, TIM_CHANNEL_3});		//U1-1/2
	m_pwm[1] = *(new PWM{&htim2, 1, TIM_CHANNEL_2});		//U3-1/2
	m_pwm[2] = *(new PWM{&htim3, 1, TIM_CHANNEL_2});		//U2-1/2
	m_pwm[3] = *(new PWM{&htim4, 1, TIM_CHANNEL_2});		//U3-3/4
	m_pwm[4] = *(new PWM{&htim4, 1, TIM_CHANNEL_4});		//U2-3/4
	m_pwm[5] = *(new PWM{&htim12, 1, TIM_CHANNEL_1});		//U1-3/4

	m_isPinoutInit = false;
	memset(m_isADCInit, false, (sizeof(m_isADCInit)/sizeof(m_isADCInit[0])));
}


/**
 * @brief The class' base destructor
 */
ControlGPIO::~ControlGPIO()
{

}

/**
 * @brief Setter for and ADC handle in the member ADC handles array
 * @param hadc New handle value
 * @param index Index in the handles array
 */
void ControlGPIO::setADCHandle(ADC_HandleTypeDef hadc, uint8_t index)
{
	if(index > sizeof(m_hadc)/sizeof(m_hadc[0]))
		return;

	m_hadc[index] = hadc;
}

/**
 * @brief Manual setter for the confirmation that the digital pinout is initialized
 * @param state New variable value
 */
void ControlGPIO::setIsPinoutInit(bool state)
{
	m_isPinoutInit = state;
}

/**
 * @brief Manual setter for the confirmation that the ADCs are initialized
 * @param state New variable value applied to all ADCs in the member array
 */
void ControlGPIO::setIsADCInit(bool state, uint8_t index)
{
	m_isADCInit[index] = state;
}

/**
 * @brief Manual setter for the confirmation that the digital pinout is initialized
 * @param state New variable value
 * @param index Index in the member array to target
 */
void ControlGPIO::setIsADCInit(bool state)
{
	memset(m_isADCInit, state, (sizeof(m_isADCInit)/sizeof(m_isADCInit[0])));
}

/**
 * @brief Getter for a specified ADC handle in the member array
 * @param index Index in the member array to target
 * @return A copy of the ADC handle targeted
 */
ADC_HandleTypeDef ControlGPIO::getADCHandle(uint8_t index) const
{
	return m_hadc[index];
}

/**
 * @brief Getter for the pointer to a specified ADC handle in the member array
 * @param index Index in the member array to target
 * @return A pointer to the ADC handle targeted
 */
ADC_HandleTypeDef* ControlGPIO::getADCHandle_ptr(uint8_t index)
{
	return &(m_hadc[index]);
}

/**
 * @brief Confirms if the digital pinout has been initialized
 * @return False if it hasn't been initialized, True if it has
 */
bool ControlGPIO::isPinoutInit(void) const
{
	return m_isPinoutInit;
}

/**
 * @brief Confirms if and ADC handle in the member array has been initialized
 * @param index Index of the ADC handle to target
 * @return False if it hasn't been initialized, True if it has
 */
bool ControlGPIO::isADCInit(uint8_t index) const
{
	if(index > (sizeof(m_isADCInit)/sizeof(m_isADCInit[0])))
		return 0;

	return m_isADCInit[index];
}

/**
 * @brief Confirms if all the ADC handles have been initialized
 * @return False if they haven't been initialized, True if they have
 */
bool ControlGPIO::isADCInit(void) const
{
	const int a0 = m_isADCInit[0];

	for(uint8_t i = 1; i < (sizeof(m_isADCInit)/sizeof(m_isADCInit[0])); ++i)
	{
		if (m_isADCInit[i] != a0)
			return true;
	}
	return false;
}

/**
 * @brief Confirms if all the ADC handles  and the digital pinout have been initialized
 * @return False if they haven't been initialized, True if they have
 */
bool ControlGPIO::isInit(void) const
{
	return (m_isPinoutInit && isADCInit());
}

/**
 * @brief Getter for the number of active ADC handles
 * @return Number of active ADC handles
 */
uint8_t ControlGPIO::getADCQty(void) const
{
	return (sizeof(m_hadc)/sizeof(m_hadc[0]));
}

/**
 * @brief Getter for the number of active PWMs
 * @return Number of active PWMs
 */
uint8_t ControlGPIO::getPWMQty(void) const
{
	return (sizeof(m_pwm)/sizeof(m_pwm[0]));
}

/**
 * @brief Initializes a targeted ADC handle
 * @param index Index in the member array of the ADC handle to initialize
 */
void ControlGPIO::initADC(uint8_t index)
{
	ADC_AnalogWDGConfTypeDef AnalogWDGConfig;
	ADC_ChannelConfTypeDef sConfig;

	std::string str = __FILE__;
	char* temp = new char[str.length() + 1];
	strcpy(temp, str.c_str());

	if(index > sizeof(m_isADCInit)/sizeof(m_isADCInit[0]))
		return;
	volatile int tmp = HAL_ADC_Init(&(m_hadc[index]));
	if (tmp != HAL_OK)
	{
		_Error_Handler(temp, __LINE__);
	}

	/* Configure the analog watchdog */
	AnalogWDGConfig.WatchdogMode = ADC_ANALOGWATCHDOG_SINGLE_REG;
	AnalogWDGConfig.HighThreshold = 0;
	AnalogWDGConfig.LowThreshold = 0;
	AnalogWDGConfig.Channel = ADC_CHANNEL_10;
	AnalogWDGConfig.ITMode = DISABLE;
	if (HAL_ADC_AnalogWDGConfig(&(m_hadc[index]), &AnalogWDGConfig) != HAL_OK)
	{
		_Error_Handler(temp, __LINE__);
	}

	/* Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time */
	sConfig.Channel = ADC_CHANNEL_10;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
	if (HAL_ADC_ConfigChannel(&(m_hadc[index]), &sConfig) != HAL_OK)
	{
		_Error_Handler(temp, __LINE__);
	}

	m_isADCInit[index] = true;

}

/**
 * @brief Initializes all the active ADC handles
 */
void ControlGPIO::initADC(void)
{
	for(uint8_t i = 0; i < sizeof(m_hadc)/sizeof(m_hadc[0]); ++i)
		initADC(i);

	setIsADCInit(true);
}

/**
 * @brief Initializes the digital pins
 */
void ControlGPIO::initPinout(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, U2_2A_Pin|U2_1A_Pin|U1_4A_Pin|RGB_OUTPUT_S2_Pin
			|RGB_OUTPUT_S3_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, SPI_CE_Pin|U1_3A_Pin|SPI_NSS_Pin|U1_2A_Pin|U2_3A_Pin, GPIO_PIN_RESET);


	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, U2_2A_Pin|U2_1A_Pin|U1_4A_Pin|RGB_OUTPUT_S2_Pin|RGB_OUTPUT_S3_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(U1_1A_GPIO_Port, U1_1A_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, U2_4A_Pin|U3_1A_Pin|U3_2A_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(U3_3A_GPIO_Port, U3_3A_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pins : LIMIT_SWITCH4_Pin LIMIT_SWITCH3_Pin RGB_INPUT_0_Pin RGB_INPUT_1_Pin
	                           RGB_INPUT_2_Pin */
	GPIO_InitStruct.Pin = LIMIT_SWITCH2_Pin|LIMIT_SWITCH3_Pin|RGB_INPUT_0_Pin|RGB_INPUT_1_Pin
			|RGB_INPUT_2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : ADC_Pin */
	GPIO_InitStruct.Pin = ADC_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ADC_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : IR_INTERRUPT3_Pin */
	GPIO_InitStruct.Pin = IR_INTERRUPT3_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(IR_INTERRUPT3_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : IR_INTERRUPT1_Pin */
	GPIO_InitStruct.Pin = IR_INTERRUPT1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(IR_INTERRUPT1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : USART_TX_Pin USART_RX_Pin */
	GPIO_InitStruct.Pin = USART_TX_Pin|USART_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : LIMIT_SWITCH1_Pin */
	GPIO_InitStruct.Pin = LIMIT_SWITCH1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(LIMIT_SWITCH1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : U2_2A_Pin U2_1A_Pin U1_4A_Pin RGB_OUTPUT_S2_Pin
	                           RGB_OUTPUT_S3_Pin */
	GPIO_InitStruct.Pin = U2_2A_Pin|U2_1A_Pin|U1_4A_Pin|RGB_OUTPUT_S2_Pin
			|RGB_OUTPUT_S3_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : LIMIT_SWITCH2_Pin */
	GPIO_InitStruct.Pin = LIMIT_SWITCH4_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(LIMIT_SWITCH4_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : RF_INTERRUPT_Pin IR_INTERRUPT2_Pin LASER_INTERRUPT_Pin */
	GPIO_InitStruct.Pin = RF_INTERRUPT_Pin|IR_INTERRUPT2_Pin|LASER_INTERRUPT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : SPI_CE_Pin U1_3A_Pin SPI_NSS_Pin U1_1A_Pin
	                           U1_2A_Pin U2_3A_Pin */
	GPIO_InitStruct.Pin = SPI_CE_Pin|U1_3A_Pin|SPI_NSS_Pin|U1_1A_Pin
			|U1_2A_Pin|U2_3A_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : ENCODEUR1_Pin ENCODEUR2_Pin */
	GPIO_InitStruct.Pin = ENCODEUR1_Pin|ENCODEUR2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : U2_4A_Pin U3_1A_Pin U3_2A_Pin */
	GPIO_InitStruct.Pin = U2_4A_Pin|U3_1A_Pin|U3_2A_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : PA15 */
	GPIO_InitStruct.Pin = GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : U3_3A_Pin */
	GPIO_InitStruct.Pin = U3_3A_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(U3_3A_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PB4 */
	GPIO_InitStruct.Pin = GPIO_PIN_4;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : U3_3-4EN */
	GPIO_InitStruct.Pin = U3_3_4EN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(U3_3_4EN_GPIO_Port, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);

	HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI1_IRQn);

	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);

	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

	m_isPinoutInit = true;
}

/**
 * @brief Initializes both all the active ADC handles and the digital pins
 */
void ControlGPIO::initGPIO(void)
{
	initPinout();
	initADC();
}

/**
 * @brief Activates a targeted ADC handle
 * @param index Index in member array corresponding to targeted ADC handle
 */
void ControlGPIO::startADC(uint8_t index)
{
	if(index > sizeof(m_hadc)/sizeof(m_hadc[0]))
		return;

	return startADCHandle(&(m_hadc[index]));
}

/**
 * @brief Activates a targeted ADC handle
 * @param hadc Handle of the ADC to activate
 */
void ControlGPIO::startADCHandle(ADC_HandleTypeDef* hadc)
{
	HAL_ADC_Start(hadc);
}

/**
 * @brief Sets a specified pin to a high logic state
 * @param GPIOx GPIO port of the targeted pin
 * @param GPIO_Pin Pin number of the the targeted pin
 */
void ControlGPIO::setPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	HAL_GPIO_WritePin(GPIOx,GPIO_Pin,GPIO_PIN_SET);
}

/**
 * @brief Sets a specified pin to a low logic state
 * @param GPIOx GPIO port of the targeted pin
 * @param GPIO_Pin Pin number of the the targeted pin
 */
void ControlGPIO::resetPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	HAL_GPIO_WritePin(GPIOx,GPIO_Pin,GPIO_PIN_RESET);
}

/**
 * @brief Read the logic state of a digital pin
 * @param GPIOx GPIO port of the targeted pin
 * @param GPIO_Pin Pin number of the the targeted pin
 * @return 0 if the pin is low, 1 if the pin is high
 */
GPIO_PinState ControlGPIO::readPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	return HAL_GPIO_ReadPin(GPIOx,GPIO_Pin);
}

/**
 * @brief Updates the duty cycle value of a PWM
 * @param htim Handle of the associated timer
 * @param channel Channel associated to the PWM
 * @param value Value (0-100) to set the duty cycle to
 */
void ControlGPIO::updatePWMHandle(TIM_HandleTypeDef *htim, uint32_t Channel, uint16_t value)
{
	__HAL_TIM_SET_COMPARE(htim, Channel, value*2000/100);
}

/**
 * @brief Updates the duty cycle value of a PWM
 * @param index Index in the member PWM array to target
 * @param value Value (0-100) to set the duty cycle to
 */
void ControlGPIO::updatePWM(uint8_t index, uint16_t value)
{
	if(index > (sizeof(m_pwm)/sizeof(m_pwm[0])))
		return;
	updatePWMHandle(m_pwm[index].htim, m_pwm[index].channel, value);
}


/**
 * @brief Reads the value of an ADC pin
 * @param index Index in the ADC member array of the handle to target
 */
int64_t ControlGPIO::readADC(uint8_t index)
{
	if(index > sizeof(m_hadc)/sizeof(m_hadc[0]))
		return -3;

	return readADCHandle(&(m_hadc[index]));
}

/**
 * @brief Reads the value of an ADC pin
 * @param hadc ADC handle to read from
 */
int64_t ControlGPIO::readADCHandle(ADC_HandleTypeDef* ptr_hadc)
{
	uint32_t response = HAL_ADC_PollForConversion(ptr_hadc, 0);

	if(response == HAL_ERROR)
		return -1;
	else if(response == HAL_BUSY)
		return -2;

	while(response != HAL_OK)
		response = HAL_ADC_PollForConversion(ptr_hadc, 0);

	return HAL_ADC_GetValue(ptr_hadc);
}
