/**
* @file ControlGPIO.hpp
* @brief Header file for the ControlGPIO class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 19th, 2018
*
* The ControlGPIO class provides control over basic GPIO pins and ports
*
*/

#ifndef CONTROLGPIO_HPP_
#define CONTROLGPIO_HPP_

/**
 * @def ACTIVE_ADC_QTY
 * @brief Number of used ADCs throughout the program
 */
#define ACTIVE_ADC_QTY 1

/**
 * @def PWM_QTY
 * @brief Number of used PWMs throughout the program
 */
#define PWM_QTY 6

#include <cstring>
#include <string>

#include "main.h"
#include "stm32f4xx_hal.h"
#include "PWM.h"
#include "timers.h"

/**
* @class ControlGPIO
* @brief Provides control over basic GPIO pins and ports
*/
class ControlGPIO {

private:

	/**
	 * @brief Array containing handles to all the active ADCs
	 */
	ADC_HandleTypeDef m_hadc[ACTIVE_ADC_QTY];

	/**
	 * @brief Array containing structures corresponding to all the active PWMs
	 */
	PWM m_pwm[PWM_QTY];

	/**
	 * @brief Confirms if the digital pinout has been initialized
	 */
	bool m_isPinoutInit;

	/**
	 * @brief Array of variables confirming if the active ADCs have been initialized
	 */
	bool m_isADCInit[ACTIVE_ADC_QTY];

public:

	/**
	 * @brief The class' base constructor
	 */
	ControlGPIO();

	/**
	 * @brief The class' base destructor
	 */
	~ControlGPIO();

	/**
	 * @brief Setter for and ADC handle in the member ADC handles array
	 * @param hadc New handle value
	 * @param index Index in the handles array
	 */
	void setADCHandle(ADC_HandleTypeDef hadc, uint8_t index);

	/**
	 * @brief Manual setter for the confirmation that the digital pinout is initialized
	 * @param state New variable value
	 */
	void setIsPinoutInit(bool state);

	/**
	 * @brief Manual setter for the confirmation that the ADCs are initialized
	 * @param state New variable value applied to all ADCs in the member array
	 */
	void setIsADCInit(bool state);

	/**
	 * @brief Manual setter for the confirmation that the digital pinout is initialized
	 * @param state New variable value
	 * @param index Index in the member array to target
	 */
	void setIsADCInit(bool state, uint8_t index);

	/**
	 * @brief Getter for a specified ADC handle in the member array
	 * @param index Index in the member array to target
	 * @return A copy of the ADC handle targeted
	 */
	ADC_HandleTypeDef getADCHandle(uint8_t index) const;

	/**
	 * @brief Getter for the pointer to a specified ADC handle in the member array
	 * @param index Index in the member array to target
	 * @return A pointer to the ADC handle targeted
	 */
	ADC_HandleTypeDef* getADCHandle_ptr(uint8_t index);

	/**
	 * @brief Confirms if the digital pinout has been initialized
	 * @return False if it hasn't been initialized, True if it has
	 */
	bool isPinoutInit(void) const;

	/**
	 * @brief Confirms if and ADC handle in the member array has been initialized
	 * @param index Index of the ADC handle to target
	 * @return False if it hasn't been initialized, True if it has
	 */
	bool isADCInit(uint8_t index) const;

	/**
	 * @brief Confirms if all the ADC handles have been initialized
	 * @return False if they haven't been initialized, True if they have
	 */
	bool isADCInit(void) const;

	/**
	 * @brief Confirms if all the ADC handles  and the digital pinout have been initialized
	 * @return False if they haven't been initialized, True if they have
	 */
	bool isInit(void) const;

	/**
	 * @brief Getter for the number of active ADC handles
	 * @return Number of active ADC handles
	 */
	uint8_t getADCQty(void) const;

	/**
	 * @brief Getter for the number of active PWMs
	 * @return Number of active PWMs
	 */
	uint8_t getPWMQty(void) const;

	/**
	 * @brief Initializes a targeted ADC handle
	 * @param index Index in the member array of the ADC handle to initialize
	 */
	void initADC(uint8_t index);

	/**
	 * @brief Initializes all the active ADC handles
	 */
	void initADC(void);

	/**
	 * @brief Initializes the digital pins
	 */
	void initPinout(void);

	/**
	 * @brief Initializes both all the active ADC handles and the digital pins
	 */
	void initGPIO(void);

	/**
	 * @brief Activates a targeted ADC handle
	 * @param index Index in member array corresponding to targeted ADC handle
	 */
	void startADC(uint8_t index);

	/**
	 * @brief Activates a targeted ADC handle
	 * @param hadc Handle of the ADC to activate
	 */
	void startADCHandle(ADC_HandleTypeDef* hadc);

	/**
	 * @brief Sets a specified pin to a high logic state
	 * @param GPIOx GPIO port of the targeted pin
	 * @param GPIO_Pin Pin number of the the targeted pin
	 */
	void setPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

	/**
	 * @brief Sets a specified pin to a low logic state
	 * @param GPIOx GPIO port of the targeted pin
	 * @param GPIO_Pin Pin number of the the targeted pin
	 */
	void resetPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

	/**
	 * @brief Read the logic state of a digital pin
	 * @param GPIOx GPIO port of the targeted pin
	 * @param GPIO_Pin Pin number of the the targeted pin
	 * @return 0 if the pin is low, 1 if the pin is high
	 */
	GPIO_PinState readPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

	/**
	 * @brief Updates the duty cycle value of a PWM
	 * @param htim Handle of the associated timer
	 * @param channel Channel associated to the PWM
	 * @param value Value (0-100) to set the duty cycle to
	 */
	void updatePWMHandle(TIM_HandleTypeDef *htim, uint32_t channel, uint16_t value);

	/**
	 * @brief Updates the duty cycle value of a PWM
	 * @param index Index in the member PWM array to target
	 * @param value Value (0-100) to set the duty cycle to
	 */
	void updatePWM(uint8_t index, uint16_t value);

	/**
	 * @brief Reads the value of an ADC pin
	 * @param index Index in the ADC member array of the handle to target
	 */
	int64_t readADC(uint8_t index);

	/**
	 * @brief Reads the value of an ADC pin
	 * @param hadc ADC handle to read from
	 */
	int64_t readADCHandle(ADC_HandleTypeDef* hadc);
};

#endif /* CONTROLGPIO_H_ */
