/**
* @file PWM.h
* @brief Header file for the PWM struct
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 19th, 2018
*
* The PWM struct encompasses all the elements necessary to interact with a PWM
* timer based on HAL libraries
*
*/

#ifndef PWM_H_
#define PWM_H_

#include "main.h"
#include "stm32f4xx_hal.h"

/**
 * @struct PWM
 * @brief  Encompasses all the elements necessary to interact with a PWM
 * timer based on HAL libraries
 */
struct PWM {

	/**
	 * @brief Pointer to the timer handle
	 */
	TIM_HandleTypeDef *htim;

	/**
	 * @brief Quantity of channels associated with the PWM
	 */
	uint8_t channelQty;

	/**
	 * @brief The associated channel with PWM timer
	 */
	uint32_t channel;
};

#endif /* PWM_H_ */
