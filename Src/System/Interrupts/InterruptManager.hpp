/**
* @file InterruptManager.hpp
* @brief Header file for the InterruptManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 22nd, 2018
*
* The InterruptManager class provides a certain control over interrupts and indexes them
*
*/

#ifndef INTERRUPTMANAGER_HPP_
#define INTERRUPTMANAGER_HPP_

/**
 * @def LASER_INTERRUP_IDX
 * @brief Index of the external interrupt associated with the laser
 */
#define LASER_INTERRUPT_IDX 0
/**
 * @def RGB_INTERRUPT_IDX
 * @brief Index of the external interrupt associated with the RGB code IR sensor
 */
#define RGB_INTERRUPT_IDX 1
/**
 * @def RIGHT_INTERRUPT_IDX
 * @brief Index of the external interrupt associated with the right IR sensor
 */
#define RIGHT_INTERRUPT_IDX 2
/**
 * @def LEFT_INTERRUPT_IDX
 * @brief Index of the external interrupt associated with the left IR sensor
 */
#define LEFT_INTERRUPT_IDX 3
/**
 * @def RF_INTERRUPT_IDX
 * @brief Index of the external interrupt associated with the RF module IRQ pin
 */
#define RF_INTERRUPT_IDX 4
/**
 * @def ENC1_INTERRUPT_IDX
 * @brief Index of the external interrupt associated with the first wheel encoder
 */
#define ENC1_INTERRUPT_IDX 5
/**
 * @def ENC2_INTERRUPT_IDX
 * @brief Index of the external interrupt associated with the second wheel encoder
 */
#define ENC2_INTERRUPT_IDX 6

#include "stm32f4xx_hal.h"
#include "Interrupt.h"
#include "LinkedList.hpp"

/**
 * @class InterruptManager
 * @brief Provides a certain control over interrupts and indexes them
 */
class InterruptManager {

private:

	/**
	 * @brief Interrupts added to the index
	 * @relates Interrupt
	 */
	friend Interrupt;

	/**
	 * @brief List of indexed interrupts
	 */
	LinkedList<Interrupt> m_interruptList;

	/**
	 * @brief Confirms if the interrupts are initialized
	 */
	bool m_isInit;

public:

	/**
	 * @brief The class' base constructor
	 */
	InterruptManager();

	/**
	 * @brief The class' base destructor
	 */
	~InterruptManager();

	/**
	 * @brief Getter for the interrupts list's pointer
	 * @return Pointer to the interrupt list
	 */
	LinkedList<Interrupt>* list(void);

	/**
	 * @brief Gets a certain indexed interruption's pointer through its associated name
	 * @param name Name of the interruption to get
	 * @return Pointer to the wanted interruption
	 */
	Interrupt* list(std::string name);

	/**
	 * @brief Initializes interruptions
	 */
	void initInterrupts(void);

	/**
	 * @brief Confirms if interruptions have been initialized
	 * @return False if they haven't been initialized, True if they have
	 */
	bool isInit(void) const;
};

#endif /* INTERRUPTMANAGER_HPP_ */
