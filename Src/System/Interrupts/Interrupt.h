/**
* @file Interrupt.h
* @brief Header file for the Interrupt struct
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 22nd, 2018
*
* The interrupt struct describes an interruption's specificities
*
*/

#ifndef INTERRUPT_H_
#define INTERRUPT_H_

#include "stm32f4xx_hal.h"
#include "SysClock.h"
#include <string>

/**
 * @enum InterruptType
 * @brief Describes the nature of the interrupt
 */
typedef enum InterruptType {
	Int_Type_Internal = 0x00,
	Int_Type_External,
	Int_Type_Timer,
	Int_Type_Undefined
} int_type_t;

/**
 * @struct Interrupt
 * @brief Describes an interruption's specificities
 *
 */
struct Interrupt
{

	/**
	 * @brief Name associated with the interrupt
	 */
	std::string name;

	/**
	 * @brief Confirmation flag of if the interruption has been triggered
	 */
	bool triggered;

	/**
	 * @brief Nature of the interrupt
	 */
	int_type_t type;

	/**
	 * @brief Digital pin of the interrupt
	 */
	uint16_t pin;

	/**
	 * @brief Digital port of the interrupt
	 */
	GPIO_TypeDef* gpioType;

	/**
	 * @brief Logic state of the associated pin
	 */
	inline bool pinState()
	{
		return HAL_GPIO_ReadPin(gpioType, pin);
	}

	/**
	 * @brief Main execution function when triggered
	 */
	inline void task(void)
	{
		if(this->name == "laser")
		{
			this->triggered = true;
		}
		else if(this->name == "rgbCode")
		{
			this->triggered = true;
		}
		else if(this->name == "lineFollowRight")
		{
			this->triggered = true;
		}
		else if(this->name == "lineFollowLeft")
		{
			this->triggered = true;
		}
		else if(this->name == "rfReceived")
		{
			this->triggered = true;
		}
		else if(this->name == "encoder1")
		{
			if(this->pinState())
			{
				this->triggered = true; //Make data available
				htim6.Instance->CR1 &= ~TIM_CR1_CEN;
			}
			else
			{
				this->triggered = false;
				htim6.Instance->CR1 |= TIM_CR1_CEN; //Fetch the new encoder speed
				__HAL_TIM_SET_COUNTER(&htim6, 0);
			}
		}
		else if(this->name ==  "encoder2")
		{
			if(this->pinState())
			{
				this->triggered = true; //Make new data available
				htim7.Instance->CR1 &= ~TIM_CR1_CEN;
			}
			else
			{
				this->triggered = false;
				htim7.Instance->CR1 |= TIM_CR1_CEN;
				__HAL_TIM_SET_COUNTER(&htim7, 0);
			}
		}
	}
};

#endif /* INTERRUPT_H_ */
