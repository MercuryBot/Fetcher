/**
* @file Interrupt_hal.cpp
* @brief Definition file for the HAL interrupt-related functions/callbacks
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 22nd, 2018
*
* These functions are the callback for the various HAL interrupts
*
*/

#include "ControlSystem.hpp"

/**
 * @brief Callback for the external interrupts
 * @param GPIO_Pin Pin that triggered the interrupt
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	for(uint8_t i = 0; i < syst->interrupts()->list()->size(); ++i)
	{
		if(syst->interrupts()->list()->at(i).pin == GPIO_Pin)
		{
			syst->interrupts()->list()->at(i).task();
			return;
		}
	}
}
