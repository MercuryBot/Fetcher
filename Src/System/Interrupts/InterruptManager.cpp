/**
* @file InterruptManager.cpp
* @brief Definition file for the InterruptManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 22nd, 2018
*
* The InterruptManager class provides a certain control over interrupts and indexes them
*
*/

#include "InterruptManager.hpp"

/**
 * @brief The class' base constructor
 */
InterruptManager::InterruptManager()
{
	m_interruptList = LinkedList<Interrupt>();
	m_isInit = false;

	//Add interrupt list
	m_interruptList.append(*(new Interrupt{.name="laser", .triggered=false, .type=Int_Type_External, .pin=LASER_INTERRUPT_Pin, .gpioType=LASER_INTERRUPT_GPIO_Port}));
	m_interruptList.append(*(new Interrupt{.name="rgbCode",  .triggered=false, .type=Int_Type_External, .pin=IR_INTERRUPT3_Pin, .gpioType=IR_INTERRUPT3_GPIO_Port}));
	m_interruptList.append(*(new Interrupt{.name="lineFollowRight", .triggered=false, .type=Int_Type_External, .pin=IR_INTERRUPT2_Pin, .gpioType=IR_INTERRUPT2_GPIO_Port}));
	m_interruptList.append(*(new Interrupt{.name="lineFollowLeft", .triggered=false, .type=Int_Type_External, .pin=IR_INTERRUPT1_Pin, .gpioType=IR_INTERRUPT1_GPIO_Port}));
	m_interruptList.append(*(new Interrupt{.name="rfReceived", .triggered=false, .type=Int_Type_External, .pin=RF_INTERRUPT_Pin, .gpioType=RF_INTERRUPT_GPIO_Port}));
	m_interruptList.append(*(new Interrupt{.name="encoder1", .triggered=false, .type=Int_Type_External, .pin=ENCODEUR1_Pin, .gpioType=ENCODEUR1_GPIO_Port}));
	m_interruptList.append(*(new Interrupt{.name="encoder2", .triggered=false, .type=Int_Type_External, .pin=ENCODEUR2_Pin, .gpioType=ENCODEUR2_GPIO_Port}));
}

/**
 * @brief The class' base destructor
 */
InterruptManager::~InterruptManager()
{

}

/**
 * @brief Getter for the interrupts list's pointer
 * @return Pointer to the interrupt list
 */
LinkedList<Interrupt>* InterruptManager::list(void)
{
	return &(m_interruptList);
}

/**
 * @brief Gets a certain indexed interruption's pointer through its associated name
 * @param name Name of the interruption to get
 * @return Pointer to the wanted interruption
 */
Interrupt* InterruptManager::list(std::string name)
{
	for(uint8_t i = 0; i < m_interruptList.size(); ++i)
	{
		if(name == m_interruptList[i].name)
		{
			return &m_interruptList[i];
		}
	}

	return (new Interrupt{.name="Unknown", .triggered=false, .type=Int_Type_Undefined, .pin=0, .gpioType=nullptr});
}

/**
 * @brief Initializes interruptions
 */
void InterruptManager::initInterrupts(void)
{
	m_isInit = true;
}

/**
 * @brief Confirms if interruptions have been initialized
 * @return False if they haven't been initialized, True if they have
 */
bool InterruptManager::isInit(void) const
{
  return m_isInit;
}
