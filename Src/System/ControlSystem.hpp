/**
* @file ControlSystem.hpp
* @brief Header file for the ControlSystem class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 20th, 2018
*
* The ControlSystem class provides control over basic hardware and registers functionalities
*
*/

#ifndef CONTROLSYSTEM_HPP_
#define CONTROLSYSTEM_HPP_

/**
 * @def MAX_PERCENT
 * @brief Maximum value of a percentage
 */
#define MAX_PERCENT 100

/**
 * @def MIN_PERCENT
 * @brief Minimum value of a percentage
 */
#define MIN_PERCENT 0

#include "stm32f4xx_hal.h"
#include "GPIO/ControlGPIO.hpp"
#include "SPI/SpiComm.hpp"
#include "Interrupts/InterruptManager.hpp"

/**
 * @class ControlSystem
 * @brief Provides control over basic hardware and registers functionalities
 */
class ControlSystem {

private:

	/**
	 * @brief The class' base constructor
	 */
	ControlSystem();

	/**
	 * @brief The class' base destructor
	 */
	~ControlSystem();

	/**
	* @brief Pointer to the single instance of the class
	*/
	static ControlSystem* inst;

	/**
	* @brief GPIO control member
	*/
	ControlGPIO m_gpio;

	/**
	* @brief SPI control member
	*/
	SpiComm m_spi;

	/**
	* @brief Interruptions control member
	*/
	InterruptManager m_interrupts;

public:

	/**
	* @brief Getter of the single instance of the class
	* @return Pointer to the instance
	*/
	static ControlSystem* getInstance(void);

	/**
	* @brief Getter of the member GPIO control class
	* @return Pointer to the instance
	*/
	ControlGPIO* GPIO(void);

	/**
	* @brief Getter of the member SPI control class
	* @return Pointer to the instance
	*/
	SpiComm* SPI(void);

	/**
	* @brief Getter of the member interrupts control class
	* @return Pointer to the instance
	*/
	InterruptManager* interrupts(void);

	/**
	 * @brief Initializes all control members
	 */
	void init(void);

	/**
	 * @brief Confirms if all the control members are initialized
	 * @return False if the control members are not all initialized, True if they all are
	 */
	bool isInit(void);
};

/**
* @brief Global instance of the ControlSystem class
*/
extern ControlSystem* syst;

#endif /* CONTROLSYSTEM_HPP_ */
