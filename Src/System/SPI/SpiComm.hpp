/**
* @file SpiComm.hpp
* @brief Header file for the SpiComm class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 20th, 2018
*
* The SpiComm class provides a certain control over the SPI communication bus
*
*/

#ifndef SPICOMM_HPP_
#define SPICOMM_HPP_

#include "stm32f4xx_hal.h"
#include <string>
#include <cstdlib>
#include <cstring>

/**
 * @class SpiComm
 * @brief Provides a certain control over the SPI communication bus
 */
class SpiComm {

private:

	/**
	 * @brief SPI handle containing the configuration
	 */
	SPI_HandleTypeDef m_hspi;

	/**
	 * @brief Confirms if the SPI bus is initialized
	 */
	bool m_init;

public:

	/**
	 * @brief The class' base constructor
	 */
	SpiComm();

	/**
	 * @brief The class' parametered constructor
	 * @param Preset SPI handle
	 */
	SpiComm(SPI_HandleTypeDef hspi);

	/**
	 * @brief The class' base destructor
	 */
	~SpiComm();

	/**
	 * @brief Initializes the SPI bus
	 */
	void init(void);

	/**
	 * @brief Initializes the SPI bus according to a preset SPI handle
	 * @param ptr_hspi Pointer to the handle to initialize
	 */
	void init(SPI_HandleTypeDef* ptr_hspi);

	/**
	* @brief Exchanges a byte over the SPI bus
	* @param c_data Byte to send
	* @return The byte returned from the exchange
	*/
	uint8_t exchData_8b(uint8_t c_data);

	/**
	* @brief Exchanges an array of bytes over the SPI bus
	* @param delay_us Delay in microseconds to wait between each byte exchange
	* @param a_data Array of bytes to send
	* @param len Size of the array to exchange
	* @param a_outData Pointer to the array in which to put the returned bytes
	*/
    void exchBuf_8b(unsigned long delay_us, uint8_t* a_data, size_t len, uint8_t* a_outData);

	/**
	* @brief Exchanges a duo of bytes over the SPI bus
	* @param c_data Bytes to send
	* @return The bytes returned from the exchange
	*/
    uint16_t exchData_16b(uint16_t c_data);

    /**
	* @brief Exchanges an array of dual bytes over the SPI bus
	* @param delay_us Delay in microseconds to wait between each byte exchange
	* @param a_data Array of dual bytes to send
	* @param len Size of the array to exchange
	* @param a_outData Pointer to the array in which to put the returned dual bytes
	*/
    void exchBuf_16b(unsigned long delay_us, uint16_t* a_data, size_t len, uint16_t* a_outData);

    /**
     * @brief Setter for the SPI handle member
     * @param hspi new handle to set
     */
    void setHandle(SPI_HandleTypeDef hspi);

    /**
     * @brief Setter for the SPI handle's instance parameter
     * @param instance Instance value to set
     */
	void setInstance(SPI_TypeDef* instance);

	/**
	 * @brief Setter for the SPI handle's mode parameter
	 * @param mode Mode value to set
	 */
	void setMode(uint32_t mode);

	/**
	 * @brief Setter for the SPI handle's direction parameter
	 * @param direction Direction value to set
	 */
	void setDirection(uint32_t direction);

	/**
	 * @brief Setter for the SPI handle's data size parameter
	 * @param dataSize Data size value to set
	 */
	void setDataSize(uint32_t dataSize);

	/**
	 * @brief Setter for the SPI handle's polarity parameter
	 * @param polarity Polarity value to set
	 */
	void setClockPolarity(uint32_t polarity);

	/**
	 * @brief Setter for the SPI handle's clock phase parameter
	 * @param phase Clock phase value to set
	 */
	void setClkPhase(uint32_t phase);

	/**
	 * @brief Setter for the SPI handle's NSS parameter
	 * @param nss NSS value to set
	 */
	void setNSS(uint32_t nss);

	/**
	 * @brief Setter for the SPI handle's prescale parameter
	 * @param prescale Prescale value to set
	 */
	void setPrescale(uint32_t prescale);

	/**
	 * @brief Setter for the SPI handle's first bit parameter
	 * @param firstBit First bit value to set
	 */
	void setFirstBit(uint32_t firstBit);

	/**
	 * @brief Setter for the SPI handle's TI mode enabling parameter
	 * @param tiMode TI mode enabling value to set
	 */
	void setTiMode(uint32_t tiMode);

	/**
	 * @brief Setter for the SPI handle's CRC calculation parameter
	 * @param calc CRC calculation value to set
	 */
	void setCrcCalculation(uint32_t calc);

	/**
	 * @brief Setter for the SPI handle's CRC polynomial parameter
	 * @param polynomial CRC polynomial value to set
	 */
	void setCrcPolynomial(uint32_t polynomial);

#ifdef SPI_CSN_Pin
	/**
	 * @brief Toggle the state of the CSN pin of the SPI bus
	 * @param data State to set the CSN pin to
	 */
	void toggleCSNPin(bool data);
#endif

#ifdef SPI_CE_Pin
	/**
	 * @brief Toggle the state of the CE pin of the SPI bus
	 * @param data State to set the CE pin to
	 */
	void toggleCEPin(bool data);
#endif

	/**
	 * @brief Getter of the SPI handle
	 * @return Value of the SPI handle
	 */
	SPI_HandleTypeDef handle(void) const;

	/**
	 * @brief Getter of the SPI handle's instance
	 * @return Value of the SPI handle's instance
	 */
	SPI_TypeDef* instance(void);

	/**
	 * @brief Getter of the SPI handle's mode
	 * @return Value of the SPI handle's mode
	 */
	uint32_t mode(void) const;

	/**
	 * @brief Getter of the SPI handle's direction
	 * @return Value of the SPI handle's direction
	 */
	uint32_t direction(void) const;

	/**
	 * @brief Getter of the SPI handle's data size
	 * @return Value of the SPI handle's data size
	 */
	uint32_t dataSize(void) const;

	/**
	 * @brief Getter of the SPI handle's clock polarity
	 * @return Value of the SPI handle's clock polarity
	 */
	uint32_t clockPolarity(void) const;

	/**
	 * @brief Getter of the SPI handle's clock phase
	 * @return Value of the SPI handle's clock phase
	 */
	uint32_t clkPhase(void) const;

	/**
	 * @brief Getter of the SPI handle's NSS
	 * @return Value of the SPI handle's NSS
	 */
	uint32_t nss(void) const;

	/**
	 * @brief Getter of the SPI handle's prescale
	 * @return Value of the SPI handle's prescale
	 */
	uint32_t prescale(void) const;

	/**
	 * @brief Getter of the SPI handle's first bit
	 * @return Value of the SPI handle's first bit
	 */
	uint32_t firstBit(void) const;

	/**
	 * @brief Getter of the SPI handle's TI mode enabling
	 * @return Value of the SPI handle's TI mode enabling
	 */
	uint32_t tiMode(void) const;

	/**
	 * @brief Getter of the SPI handle's CRC calculation
	 * @return Value of the SPI handle's CRC calculation
	 */
	uint32_t crcCalculation(void) const;

	/**
	 * @brief Getter of the SPI handle's CRC polynomial
	 * @return Value of the SPI handle's CRC polynomial
	 */
	uint32_t crcPolynomial(void) const;

	/**
	 * @brief Confirms if the SPI bus is initialized
	 * @return False if it's not initialized, True if it is
	 */
	bool isInit(void);

	/**
	 * @brief Confirms if the SPI bus is enabled
	 * @return False if it's not enabled, True if it is
	 */
	bool isEnabled(void);

	/**
	 * @brief Confirms if the SPI bus is busy sending
	 * @return False if it's not busy, True if it is
	 */
	bool isTxBusy(void);

	/**
	 * @brief Confirms if the SPI bus is busy receiving
	 * @return False if it's not busy, True if it is
	 */
	bool isRxBusy(void);
};

#endif /* SPICOMM_HPP_ */
