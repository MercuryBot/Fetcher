/**
* @file SpiComm.cpp
* @brief Definition file for the SpiComm class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 20th, 2018
*
* The SpiComm class provides a certain control over the SPI communication bus
*
*/

#include "SpiComm.hpp"

/**
 * @brief The class' base constructor
 */
SpiComm::SpiComm()
{
	m_hspi = {0};
	m_hspi.Instance = SPI2;
	m_hspi.Init.Mode = SPI_MODE_MASTER;
	m_hspi.Init.Direction = SPI_DIRECTION_2LINES;
	m_hspi.Init.DataSize = SPI_DATASIZE_8BIT;
	m_hspi.Init.CLKPolarity = SPI_POLARITY_LOW;
	m_hspi.Init.CLKPhase = SPI_PHASE_1EDGE;
	m_hspi.Init.NSS = SPI_NSS_SOFT;
	m_hspi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
	m_hspi.Init.FirstBit = SPI_FIRSTBIT_MSB;
	m_hspi.Init.TIMode = SPI_TIMODE_DISABLE;
	m_hspi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	m_hspi.Init.CRCPolynomial = 7;

	m_init = false;
}


/**
 * @brief The class' parametered constructor
 * @param Preset SPI handle
 */
SpiComm::SpiComm(SPI_HandleTypeDef hspi)
{
	m_hspi = hspi;
	m_init = false;
}


/**
 * @brief The class' base destructor
 */
SpiComm::~SpiComm()
{
	delete &m_hspi;
}

/**
 * @brief Initializes the SPI bus
 */
void SpiComm::init(void)
{
	if (HAL_SPI_Init(&m_hspi) != HAL_OK)
	{
		std::string str = __FILE__;
		char* temp = new char[str.length() + 1];
		strcpy(temp, str.c_str());
		_Error_Handler(temp, __LINE__);
	}

	m_init = true;
}

/**
 * @brief Initializes the SPI bus according to a preset SPI handle
 * @param ptr_hspi Pointer to the handle to initialize
 */
void SpiComm::init(SPI_HandleTypeDef* ptr_hspi)
{
	if (HAL_SPI_Init(ptr_hspi) != HAL_OK)
	{
		std::string str = __FILE__;
		char* temp = new char[str.length() + 1];
		strcpy(temp, str.c_str());
		_Error_Handler(temp, __LINE__);
	}

	m_init = true;
}

/**
* @brief Exchanges a byte over the SPI bus
* @param c_data Byte to send
* @return The byte returned from the exchange
*/
uint8_t SpiComm::exchData_8b(uint8_t data)
{
	uint8_t returnData;

	if(/*!isEnabled() || */!isInit() || (m_hspi.Init.DataSize != SPI_DATASIZE_8BIT))
		return 0;

	//while(isTxBusy());

	HAL_SPI_TransmitReceive(&m_hspi, &data, &returnData, 1, 1);
	while(HAL_SPI_GetState(&m_hspi) != HAL_SPI_STATE_READY);

	//*(__IO uint8_t *)&m_hspi.Instance->DR = data;

	//while(isRxBusy());

	return returnData;
}

/**
* @brief Exchanges an array of bytes over the SPI bus
* @param delay_us Delay in microseconds to wait between each byte exchange
* @param a_data Array of bytes to send
* @param len Size of the array to exchange
* @param a_outData Pointer to the array in which to put the returned bytes
*/
void SpiComm::exchBuf_8b(uint32_t delay_ms, uint8_t* a_data, size_t len, uint8_t* a_outData)
{
	/*for(uint8_t i = 0; i < len; ++i)
	{
		a_outData[i] = exchData_8b(a_data[i]);
		HAL_Delay(delay_ms);
	}*/

	//while(HAL_SPI_GetState(&m_hspi) != HAL_SPI_STATE_READY);
	HAL_SPI_TransmitReceive(&m_hspi, a_data, a_outData, len, delay_ms);
	while(HAL_SPI_GetState(&m_hspi) != HAL_SPI_STATE_READY);
}

/**
* @brief Exchanges a duo of bytes over the SPI bus
* @param c_data Bytes to send
* @return The bytes returned from the exchange
*/
uint16_t SpiComm::exchData_16b(uint16_t data)
{
	if(!isEnabled() || !isInit() || (m_hspi.Init.DataSize != SPI_DATASIZE_16BIT))
		return 0;

	while(isTxBusy());

	*(__IO uint16_t *)&m_hspi.Instance->DR = data;

	while(isRxBusy());


	return m_hspi.Instance->DR;
}

/**
* @brief Exchanges an array of dual bytes over the SPI bus
* @param delay_us Delay in microseconds to wait between each byte exchange
* @param a_data Array of dual bytes to send
* @param len Size of the array to exchange
* @param a_outData Pointer to the array in which to put the returned dual bytes
*/
void SpiComm::exchBuf_16b(uint32_t delay_ms, uint16_t* a_data, size_t len, uint16_t* a_outData)
{
	for(uint8_t i = 0; i < len; ++i)
	{
		a_outData[i] = exchData_16b(a_data[i]);
		HAL_Delay(delay_ms);
	}
}

/**
 * @brief Setter for the SPI handle member
 * @param hspi new handle to set
 */
void SpiComm::setHandle(SPI_HandleTypeDef hspi)
{
	m_hspi = hspi;
}

/**
 * @brief Setter for the SPI handle's instance parameter
 * @param instance Instance value to set
 */
void SpiComm::setInstance(SPI_TypeDef* instance)
{
	m_hspi.Instance = instance;
}

/**
 * @brief Setter for the SPI handle's mode parameter
 * @param mode Mode value to set
 */
void SpiComm::setMode(uint32_t mode)
{
	m_hspi.Init.Mode = mode;
}

/**
 * @brief Setter for the SPI handle's direction parameter
 * @param direction Direction value to set
 */
void SpiComm::setDirection(uint32_t direction)
{
	m_hspi.Init.Direction = direction;
}

/**
 * @brief Setter for the SPI handle's data size parameter
 * @param dataSize Data size value to set
 */
void SpiComm::setDataSize(uint32_t dataSize)
{
	m_hspi.Init.DataSize = dataSize;
}

/**
 * @brief Setter for the SPI handle's polarity parameter
 * @param polarity Polarity value to set
 */
void SpiComm::setClockPolarity(uint32_t polarity)
{
	m_hspi.Init.CLKPolarity = polarity;
}

/**
 * @brief Setter for the SPI handle's clock phase parameter
 * @param phase Clock phase value to set
 */
void SpiComm::setClkPhase(uint32_t phase)
{
	m_hspi.Init.CLKPhase = phase;
}

/**
 * @brief Setter for the SPI handle's NSS parameter
 * @param nss NSS value to set
 */
void SpiComm::setNSS(uint32_t nss)
{
	m_hspi.Init.NSS = nss;
}

/**
 * @brief Setter for the SPI handle's prescale parameter
 * @param prescale Prescale value to set
 */
void SpiComm::setPrescale(uint32_t prescale)
{
	m_hspi.Init.BaudRatePrescaler = prescale;
}

/**
 * @brief Setter for the SPI handle's first bit parameter
 * @param firstBit First bit value to set
 */
void SpiComm::setFirstBit(uint32_t firstBit)
{
	m_hspi.Init.FirstBit = firstBit;
}

/**
 * @brief Setter for the SPI handle's TI mode enabling parameter
 * @param tiMode TI mode enabling value to set
 */
void SpiComm::setTiMode(uint32_t tiMode)
{
	m_hspi.Init.TIMode = tiMode;
}

/**
 * @brief Setter for the SPI handle's CRC calculation parameter
 * @param calc CRC calculation value to set
 */
void SpiComm::setCrcCalculation(uint32_t calc)
{
	m_hspi.Init.CRCCalculation = calc;
}

/**
 * @brief Setter for the SPI handle's CRC polynomial parameter
 * @param polynomial CRC polynomial value to set
 */
void SpiComm::setCrcPolynomial(uint32_t polynomial)
{
	m_hspi.Init.CRCPolynomial = polynomial;
}

#ifdef SPI_CSN_Pin
/**
 * @brief Toggle the state of the CSN pin of the SPI bus
 * @param data State to set the CSN pin to
 */
void SpiComm::toggleCSNPin(bool data)
{
	HAL_GPIO_WritePin(SPI_CSN_GPIO_Port, SPI_CSN_Pin, (GPIO_PinState) data);
}
#endif

#ifdef SPI_CE_Pin
/**
 * @brief Toggle the state of the CE pin of the SPI bus
 * @param data State to set the CE pin to
 */
void SpiComm::toggleCEPin(bool data)
{
	HAL_GPIO_WritePin(SPI_CE_GPIO_Port, SPI_CE_Pin, (GPIO_PinState) data);
}
#endif

/**
 * @brief Getter of the SPI handle
 * @return Value of the SPI handle
 */
SPI_HandleTypeDef SpiComm::handle(void) const
{
	return m_hspi;
}

/**
 * @brief Getter of the SPI handle's instance
 * @return Value of the SPI handle's instance
 */
SPI_TypeDef* SpiComm::instance(void)
{
	return m_hspi.Instance;
}

/**
 * @brief Getter of the SPI handle's mode
 * @return Value of the SPI handle's mode
 */
uint32_t SpiComm::mode(void) const
{
	return m_hspi.Init.Mode;
}

/**
 * @brief Getter of the SPI handle's direction
 * @return Value of the SPI handle's direction
 */
uint32_t SpiComm::direction(void) const
{
	return m_hspi.Init.Direction;
}

/**
 * @brief Getter of the SPI handle's data size
 * @return Value of the SPI handle's data size
 */
uint32_t SpiComm::dataSize(void) const
{
	return m_hspi.Init.DataSize;
}

/**
 * @brief Getter of the SPI handle's clock polarity
 * @return Value of the SPI handle's clock polarity
 */
uint32_t SpiComm::clockPolarity(void) const
{
	return m_hspi.Init.CLKPolarity;
}

/**
 * @brief Getter of the SPI handle's clock phase
 * @return Value of the SPI handle's clock phase
 */
uint32_t SpiComm::clkPhase(void) const
{
	return m_hspi.Init.CLKPhase;
}

/**
 * @brief Getter of the SPI handle's NSS
 * @return Value of the SPI handle's NSS
 */
uint32_t SpiComm::nss(void) const
{
	return m_hspi.Init.NSS;
}

/**
 * @brief Getter of the SPI handle's prescale
 * @return Value of the SPI handle's prescale
 */
uint32_t SpiComm::prescale(void) const
{
	return m_hspi.Init.BaudRatePrescaler;
}

/**
 * @brief Getter of the SPI handle's first bit
 * @return Value of the SPI handle's first bit
 */
uint32_t SpiComm::firstBit(void) const
{
	return m_hspi.Init.FirstBit;
}

/**
 * @brief Getter of the SPI handle's TI mode enabling
 * @return Value of the SPI handle's TI mode enabling
 */
uint32_t SpiComm::tiMode(void) const
{
	return m_hspi.Init.TIMode;
}

/**
 * @brief Getter of the SPI handle's CRC calculation
 * @return Value of the SPI handle's CRC calculation
 */
uint32_t SpiComm::crcCalculation(void) const
{
	return m_hspi.Init.CRCCalculation;
}

/**
 * @brief Getter of the SPI handle's CRC polynomial
 * @return Value of the SPI handle's CRC polynomial
 */
uint32_t SpiComm::crcPolynomial(void) const
{
	return m_hspi.Init.CRCPolynomial;
}

/**
 * @brief Confirms if the SPI bus is initialized
 * @return False if it's not initialized, True if it is
 */
bool SpiComm::isInit(void)
{
	return m_init;
}

/**
 * @brief Confirms if the SPI bus is enabled
 * @return False if it's not enabled, True if it is
 */
bool SpiComm::isEnabled(void)
{
	return (m_hspi.Instance->CR1 & SPI_CR1_SPE);
}

/**
 * @brief Confirms if the SPI bus is busy sending
 * @return False if it's not busy, True if it is
 */
bool SpiComm::isTxBusy(void)
{
	return ((m_hspi.Instance->SR & SPI_FLAG_TXE) == 0 || (m_hspi.Instance->SR & SPI_FLAG_BSY));
}

/**
 * @brief Confirms if the SPI bus is busy receiving
 * @return False if it's not busy, True if it is
 */
bool SpiComm::isRxBusy(void)
{
	return ((m_hspi.Instance->SR & SPI_FLAG_RXNE) == 0 || (m_hspi.Instance->SR & SPI_FLAG_BSY));
}
