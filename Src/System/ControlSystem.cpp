/**
* @file ControlSystem.cpp
* @brief Definition file for the ControlSystem class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 20th, 2018
*
* The ControlSystem class provides control over basic hardware and registers functionalities
*
*/

#include "ControlSystem.hpp"

ControlSystem* ControlSystem::inst;

/**
* @brief The class' base constructor
*/
ControlSystem::ControlSystem() : m_gpio(), m_spi(), m_interrupts()
{
	inst = 0;
}

/**
 * @brief The class' base destructor
 */
ControlSystem::~ControlSystem()
{

}

/**
* @brief Getter of the single instance of the class
* @return Pointer to the instance
*/
ControlSystem* ControlSystem::getInstance(void)
{
	if(!ControlSystem::inst)
	{
		ControlSystem::inst = new ControlSystem();
	}

	return ControlSystem::inst;
}

/**
* @brief Getter of the member GPIO control class
* @return Pointer to the instance
*/
ControlGPIO* ControlSystem::GPIO(void)
{
	return &m_gpio;
}

/**
* @brief Getter of the member SPI control class
* @return Pointer to the instance
*/
SpiComm* ControlSystem::SPI()
{
	return &m_spi;
}

/**
* @brief Getter of the member interrupts control class
* @return Pointer to the instance
*/
InterruptManager* ControlSystem::interrupts(void)
{
	return &m_interrupts;
}

/**
 * @brief Initializes all control members
 */
void ControlSystem::init(void)
{
	m_gpio.initGPIO();
	m_spi.init();
	m_interrupts.initInterrupts();
}

/**
 * @brief Confirms if all the control members are initialized
 * @return False if the control members are not all initialized, True if they all are
 */
bool ControlSystem::isInit(void)
{
	return (m_gpio.isInit() && m_spi.isInit() && m_interrupts.isInit());
}

/**
* @brief Global instance of the ControlSystem class
*/
ControlSystem* syst = ControlSystem::getInstance();
