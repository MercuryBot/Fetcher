/**
 * @file ModelLimitSwitch.hpp
 * @brief Header file for the ModelLimitSwitch class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ModelLimitSwitch class provides easier management of limit switch
 *
 */

#ifndef SYSTEM_GPIO_MODELLIMITSWITCH_HPP_
#define SYSTEM_GPIO_MODELLIMITSWITCH_HPP_

#include "ControlSystem.hpp"
#include "DigitalSensor.hpp"
/**
 * @class ModelLimitSwitch
 * @brief The ModelLimitSwitch class provides easier management of limit switch
 */
class ModelLimitSwitch : public DigitalSensor {

private:
	/**
	 * #brief The current state of the sensor
	 */
	bool m_state;

public:
	/**
	 * @brief The class base constructor
	 */
	ModelLimitSwitch();
	/**
	 * @brief The class constructor with parameter
	 * @param port Port of the sensor
	 * @param pin Pin of the sensor
	 */
	ModelLimitSwitch(GPIO_TypeDef* port, uint16_t pin);
	/**
	 * @brief The class base destructor
	 */
	~ModelLimitSwitch();
	/**
	 * @brief Setter for the state
	 * @param state The new state
	 */
	void setState(bool state);
	/**
	 * @brief Getter for the state
	 * @return The state in boolean
	 */
	bool getState(void) const;
	/**
	 * @brief Fetch the new state if the previous state is different
	 * @return True if the state changed or false if it's the same
	 */
	virtual bool update();
};

#endif /* SYSTEM_GPIO_MODELLIMITSWITCH_HPP_ */
