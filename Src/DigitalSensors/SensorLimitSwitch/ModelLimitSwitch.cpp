/**
 * @file ModelLimitSwitch.cpp
 * @brief Definition file for the ModelLimitSwitch class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ModelLimitSwitch class provides easier management of limit switch
 *
 */

#include "ModelLimitSwitch.hpp"
/**
 * @brief The class base constructor
 */
ModelLimitSwitch::ModelLimitSwitch()
{
	setPort(nullptr);
	setPin(0);
	m_state = 0;
}
/**
 * @brief The class constructor with parameter
 * @param port Port of the sensor
 * @param pin Pin of the sensor
 */
ModelLimitSwitch::ModelLimitSwitch(GPIO_TypeDef* port, uint16_t pin)
{
	setPort(port);
	setPin(pin);
	m_state = 0;
}
/**
 * @brief The class base destructor
 */
ModelLimitSwitch::~ModelLimitSwitch()
{

}
/**
 * @brief Setter for the state
 * @param state The new state
 */
void ModelLimitSwitch::setState(bool state)
{
	m_state = state;
}
/**
 * @brief Getter for the state
 * @return The state in boolean
 */
bool ModelLimitSwitch::getState() const
{
	return m_state;
}
/**
 * @brief Fetch the new state if the previous state is different
 * @return True if the state changed or false if it's the same
 */
bool ModelLimitSwitch::update()
{
	bool prevState = m_state;

	m_state = (bool)(syst->GPIO()->readPin(getPort(), getPin()));

	return (prevState != m_state) ? true : false;
}
