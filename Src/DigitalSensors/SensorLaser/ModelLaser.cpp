/**
 * @file ModelLaser.cpp
 * @brief Definition file for the ModelLaser class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ModelLaser class provides easier management of laser sensor
 *
 */
#include "ModelLaser.hpp"
/**
 * @brief The class base constructor
 */
ModelLaser::ModelLaser()
{
	setPort(nullptr);
	setPin(0);
	setIntIndex(0);
	m_state = 0;
}
/**
 * @brief The class constructor with parameter
 * @param port Port of the sensor
 * @param pin Pin of the sensor
 */
ModelLaser::ModelLaser(GPIO_TypeDef* port, uint16_t pin, uint8_t intIndex)
{
	setPort(port);
	setPin(pin);
	setIntIndex(intIndex);
	m_state = 0;
}
/**
 * @brief The class base destructor
 */
ModelLaser::~ModelLaser()
{

}
/**
 * @brief Setter for the state
 * @param state The new state
 */
void ModelLaser::setState(bool state)
{
	m_state = state;
}
/**
 * @brief Getter for the state
 * @return The state in boolean
 */
bool ModelLaser::getState() const
{
	return m_state;
}
/**
 * @brief Fetch the new state if the previous state is different
 * @return True if the state changed or false if it's the same
 */
bool ModelLaser::update()
{
	if(syst->interrupts()->list()->at(getIntIndex()).triggered)
	{
		m_state = (bool)(syst->GPIO()->readPin(getPort(), getPin()));
		syst->interrupts()->list()->at(getIntIndex()).triggered = false;
		return true;
	}
	return false;
}
