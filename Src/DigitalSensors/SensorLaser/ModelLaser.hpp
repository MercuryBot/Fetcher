/**
 * @file ModelLaser.hpp
 * @brief Header file for the ModelLaser class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ModelLaser class provides easier management of laser sensor
 *
 */
#ifndef MODELLASER_HPP_
#define MODELLASER_HPP_

#include "ControlSystem.hpp"
#include "DigitalSensor.hpp"
/**
 * @class ModelLaser
 * @brief The ModelLaser class provides easier management of laser sensor
 */
class ModelLaser : public DigitalSensor {

private:
	/**
	 * #brief The current state of the sensor
	 */
	bool m_state;

public:
	/**
	 * @brief The class base constructor
	 */
	ModelLaser();
	/**
	 * @brief The class constructor with parameter
	 * @param port Port of the sensor
	 * @param pin Pin of the sensor
	 */
	ModelLaser(GPIO_TypeDef* port, uint16_t pin, uint8_t intIndex);
	/**
	 * @brief The class base destructor
	 */
	virtual ~ModelLaser();
	/**
	 * @brief Setter for the state
	 * @param state The new state
	 */
	void setState(bool state);
	/**
	 * @brief Getter for the state
	 * @return The state in boolean
	 */
	bool getState() const;
	/**
	 * @brief Fetch the new state if the previous state is different
	 * @return True if the state changed or false if it's the same
	 */
	virtual bool update();
};

#endif /* MODELLASER_HPP_ */
