/**
 * @file DigitalSensor.hpp
 * @brief Header file for the DigitalSensor class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The DigitalSensor class provides easier management of digital sensors
 *
 */

#ifndef DIGITALSENSOR_HPP_
#define DIGITALSENSOR_HPP_

#include "stm32f4xx_hal.h"
#include "ControlSystem.hpp"
/**
 * @class DigitalSensor
 * @brief The DigitalSensor class provides easier management of digital sensors
 */
class DigitalSensor {

private:
	GPIO_TypeDef* m_port;
	uint16_t m_pin;
	uint8_t m_intIndex;

public:
	/**
	 * @brief The class base constructor
	 */
	DigitalSensor();
	/**
	 * @brief The class constructor with parameter
	 * @param port Port of the sensor
	 * @param pin Pin of the sensor
	 * @param intIndex Index of the sensor interrupt
	 */
	DigitalSensor(GPIO_TypeDef* port, uint16_t pin, uint8_t intIndex);
	/**
	 * @brief The class base destructor
	 */
	virtual ~DigitalSensor();
	/**
	 * @brief Setter for the port attribute
	 * @param port The new port
	 */
	void setPort(GPIO_TypeDef* port);
	/**
	 * @brief Setter for the pin attribute
	 * @param pin The new pin
	 */
	void setPin(uint16_t pin);
	/**
	 * @brief Setter for the sensor interrupt
	 * @param intIndex The index of the interrupt
	 */
	void setIntIndex(uint8_t intIndex);
	/**
	 * @brief Getter for the pin attribute
	 * @return The pin in uint16_t
	 */
	GPIO_TypeDef* getPort(void);
	/**
	 * @brief Getter for the speed attribute
	 * @return The speed in 0-100%
	 */
	uint16_t getPin(void) const;
	/**
	 * @brief Getter for the sensor interrupt
	 * @return The index of the interrupt
	 */
	uint8_t getIntIndex(void) const;

	virtual bool update() = 0;
};

#endif /* DIGITALSENSOR_HPP_ */
