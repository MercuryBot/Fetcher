/**
 * @file ModelEncoder.hpp
 * @brief Header file for the ModelEncoder class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ModelEncoder class provides easier management of encoder
 *
 */
#ifndef MODELENCODER_HPP_
#define MODELENCODER_HPP_

#include <deque>

#include "DigitalSensor.hpp"
/**
 * @class ModelEncoder
 * @brief The ModelEncoder class provides easier management of encoder
 */
class ModelEncoder : public DigitalSensor{

private:
	/**
	 * #brief The current state of the sensor
	 */
	bool m_state;
	/**
	 * @brief The speed in microseconds
	 */
	uint64_t m_speedUS;
	/**
	 * @brief The handler instance of the counter
	 */
	TIM_HandleTypeDef* m_htim;
	/**
	 * @brief This is the shifting data window to remove aberrant readings
	 */
	std::deque<uint64_t> m_dataWindow;

public:
	/**
	 * @brief The class base constructor
	 */
	ModelEncoder();
	/**
	 * @brief The class constructor with parameter
	 * @param port Port of the sensor
	 * @param pin Pin of the sensor
	 */
	ModelEncoder(GPIO_TypeDef* port, uint16_t pin, uint8_t intIndex, TIM_HandleTypeDef* htim);
	/**
	 * @brief The class base destructor
	 */
	~ModelEncoder();
	/**
	 * @brief Setter for the state
	 * @param state The new state
	 */
	void setState(bool state);
	/**
	 * @brief Set the target speed in microseconds
	 * @param speed The value in microseconds
	 */
	void setSpeedUS(uint64_t speed);
	/**
	 * @brief Set the target speed in milliseconds
	 * @param speed The value in milliseconds
	 */
	void setSpeedMS(uint64_t speed);
	/**
	 * @brief Setter for the timer handler
	 * @param htim The new handler
	 */
	void setHandleTimer(TIM_HandleTypeDef* htim);
	/**
	 * @brief Setter for the shifting data window
	 * @param dataWindow The new instance of the shifting data window
	 */
	void setDataWindow(std::deque<uint64_t> dataWindow);
	/**
	 * @brief Getter for the state
	 * @return The state in boolean
	 */
	bool getState(void) const;
	/**
	 * @brief Getter for the speed target in microseconds
	 * @return The value in microseconds
	 */
	uint64_t getSpeedUS(void) const;
	/**
	 * @brief Getter for the speed target in milliseconds
	 * @return The value in milliseconds
	 */
	uint64_t getSpeedMS(void) const;
	/**
	 * @brief Getter for the timer handler
	 * @return The timer handler
	 */
	TIM_HandleTypeDef* getHandleTimer(void);
	/**
	 * @brief Adds a data to the shifting data window
	 * @param speedUs The speed in microseconds
	 */
	void addSpeedSample(uint64_t speedUS);
	/**
	 * @brief Adds a data to the shifting data window
	 * @param speedUs The speed in microseconds
	 */
	void addSpeedSampleUS(uint64_t speedUS);
	/**
	 * @brief Adds a data to the shifting data window
	 * @param speedUs The speed in milliseconds
	 */
	void addSpeedSampleMS(uint64_t speedMS);
	/**
	 * @brief Getter for the result of the shifting data window
	 * @return The speed in microseconds
	 */
	uint64_t getSampledSpeed(void) const;
	/**
	 * @brief Getter for the result of the shifting data window
	 * @return The speed in microseconds
	 */
	uint64_t getSampledSpeedUS(void) const;
	/**
	 * @brief Getter for the result of the shifting data window
	 * @return The speed in milliseconds
	 */
	uint64_t getSampledSpeedMS(void) const;
	/**
	 * @brief Fetch the new state if the previous state is different
	 * @return True if the state changed or false if it's the same
	 */
	virtual bool update(void);
};

#endif /* MODELENCODER_HPP_ */
