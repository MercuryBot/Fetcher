/**
 * @file ModelEncoder.hpp
 * @brief Header file for the ModelEncoder class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ModelEncoder class provides easier management of encoder
 *
 */
#include "ModelEncoder.hpp"
/**
 * @brief The class base constructor
 */
ModelEncoder::ModelEncoder()
{
	setPort(nullptr);
	setPin(0);
	setIntIndex(0);
	m_state = 0;
	m_speedUS = 0;
	m_htim = nullptr;
	m_dataWindow = std::deque<uint64_t>(3, 0);
}
/**
 * @brief The class constructor with parameter
 * @param port Port of the sensor
 * @param pin Pin of the sensor
 */
ModelEncoder::ModelEncoder(GPIO_TypeDef* port, uint16_t pin, uint8_t intIndex, TIM_HandleTypeDef* htim)
{
	setPort(port);
	setPin(pin);
	setIntIndex(intIndex);
	m_state = 0;
	m_speedUS = 0;
	m_htim = htim;
	m_dataWindow = std::deque<uint64_t>(3, 0);
}
/**
 * @brief The class base destructor
 */
ModelEncoder::~ModelEncoder()
{

}
/**
 * @brief Setter for the state
 * @param state The new state
 */
void ModelEncoder::setState(bool state)
{
	m_state = state;
}
/**
 * @brief Set the target speed in microseconds
 * @param speed The value in microseconds
 */
void ModelEncoder::setSpeedUS(uint64_t speed)
{
	m_speedUS = speed;
}
/**
 * @brief Set the target speed in milliseconds
 * @param speed The value in milliseconds
 */
void ModelEncoder::setSpeedMS(uint64_t speed)
{
	m_speedUS = speed * 1000;
}
/**
 * @brief Setter for the timer handler
 * @param htim The new handler
 */
void ModelEncoder::setHandleTimer(TIM_HandleTypeDef* htim)
{
	m_htim = htim;
}
/**
 * @brief Setter for the shifting data window
 * @param dataWindow The new instance of the shifting data window
 */
void ModelEncoder::setDataWindow(std::deque<uint64_t> dataWindow)
{
	m_dataWindow = dataWindow;
}
/**
 * @brief Getter for the state
 * @return The state in boolean
 */
bool ModelEncoder::getState(void) const
{
	return m_state;
}
/**
 * @brief Getter for the speed target in microseconds
 * @return The value in microseconds
 */
uint64_t ModelEncoder::getSpeedUS(void) const
{
	return m_speedUS;
}
/**
 * @brief Getter for the speed target in milliseconds
 * @return The value in milliseconds
 */
uint64_t ModelEncoder::getSpeedMS(void) const
{
	return (m_speedUS / 1000);
}
/**
 * @brief Getter for the timer handler
 * @return The timer handler
 */
TIM_HandleTypeDef* ModelEncoder::getHandleTimer(void)
{
	return m_htim;
}
/**
 * @brief Adds a data to the shifting data window
 * @param speedUs The speed in microseconds
 */
void ModelEncoder::addSpeedSample(uint64_t speedUS)
{
	m_dataWindow.push_front(speedUS);
	m_dataWindow.erase(m_dataWindow.begin() + m_dataWindow.size());
}
/**
 * @brief Adds a data to the shifting data window
 * @param speedUs The speed in microseconds
 */
void ModelEncoder::addSpeedSampleUS(uint64_t speedUS)
{
	addSpeedSample(speedUS);
}
/**
 * @brief Adds a data to the shifting data window
 * @param speedUs The speed in milliseconds
 */
void ModelEncoder::addSpeedSampleMS(uint64_t speedMS)
{
	addSpeedSample((speedMS * 1000));
}
/**
 * @brief Getter for the result of the shifting data window
 * @return The speed in microseconds
 */
uint64_t ModelEncoder::getSampledSpeed(void) const
{
	uint64_t avrg = 0;

	for(uint8_t i = 0; i < m_dataWindow.size(); ++i)
		avrg += m_dataWindow[i];

	return (avrg/m_dataWindow.size());
}
/**
 * @brief Getter for the result of the shifting data window
 * @return The speed in microseconds
 */
uint64_t ModelEncoder::getSampledSpeedUS(void) const
{
	return getSampledSpeed();
}
/**
 * @brief Getter for the result of the shifting data window
 * @return The speed in milliseconds
 */
uint64_t ModelEncoder::getSampledSpeedMS(void) const
{
	return (getSampledSpeed()/1000);
}
/**
 * @brief Fetch the new state if the previous state is different
 * @return True if the state changed or false if it's the same
 */
bool ModelEncoder::update(void)
{
	bool changed = false;

	if(syst->interrupts()->list()->at(getIntIndex()).triggered)
	{
		if(__HAL_TIM_GET_COUNTER(m_htim) > 0)
		{
			m_speedUS = __HAL_TIM_GET_COUNTER(m_htim);
			addSpeedSample(m_speedUS);
		}

		syst->interrupts()->list()->at(getIntIndex()).triggered = false;
		changed = true;
	}

	m_state = (bool)(syst->GPIO()->readPin(getPort(), getPin()));

	return changed;
}
