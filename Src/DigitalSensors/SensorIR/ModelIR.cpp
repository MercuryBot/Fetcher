/**
 * @file ModelIR.cpp
 * @brief Definition file for the ModelIR class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ModelIR class provides easier management of IR sensor
 *
 */
#include "ModelIR.hpp"
/**
 * @brief The class base constructor
 */
ModelIR::ModelIR()
{
	setPort(nullptr);
	setPin(0);
	setIntIndex(0);
	m_state = 0;
}
/**
 * @brief The class constructor with parameter
 * @param port Port of the sensor
 * @param pin Pin of the sensor
 */
ModelIR::ModelIR(GPIO_TypeDef* port, uint16_t pin, uint8_t intIndex)
{
	setPort(port);
	setPin(pin);
	setIntIndex(intIndex);
	m_state = (bool)(syst->GPIO()->readPin(getPort(), getPin()));
}
/**
 * @brief The class base destructor
 */
ModelIR::~ModelIR()
{

}
/**
 * @brief Setter for the state
 * @param state The new state
 */
void ModelIR::setState(bool state)
{
	m_state = state;
}
/**
 * @brief Getter for the state
 * @return The state in boolean
 */
bool ModelIR::getState() const
{
	return m_state;
}
/**
 * @brief Fetch the new state if the previous state is different
 * @return True if the state changed or false if it's the same
 */
bool ModelIR::update(void)
{
	if(syst->interrupts()->list()->at(getIntIndex()).triggered)
	{
		m_state = (bool)(syst->GPIO()->readPin(getPort(), getPin()));
		syst->interrupts()->list()->at(getIntIndex()).triggered = false;
		return true;
	}

	return false;
}
