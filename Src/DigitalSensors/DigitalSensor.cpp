/**
 * @file DigitalSensor.cpp
 * @brief Definition file for the DigitalSensor class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The DigitalSensor class provides easier management of digital sensors
 *
 */
#include "DigitalSensor.hpp"
/**
 * @brief The class base constructor
 */
DigitalSensor::DigitalSensor()
{
	m_port = nullptr;
	m_pin = 0;
	m_intIndex = 0;
}
/**
 * @brief The class constructor with parameter
 * @param port Port of the sensor
 * @param pin Pin of the sensor
 * @param intIndex Index of the sensor interrupt
 */
DigitalSensor::DigitalSensor(GPIO_TypeDef* port, uint16_t pin, uint8_t intIndex)
{
	m_port = port;
	m_pin = pin;
	m_intIndex = intIndex;
}
/**
 * @brief The class base destructor
 */
DigitalSensor::~DigitalSensor()
{
	//delete m_port;
}
/**
 * @brief Setter for the port attribute
 * @param port The new port
 */
void DigitalSensor::setPort(GPIO_TypeDef* port)
{
	m_port = port;
}
/**
 * @brief Setter for the pin attribute
 * @param pin The new pin
 */
void DigitalSensor::setPin(uint16_t pin)
{
	m_pin = pin;
}
/**
 * @brief Setter for the sensor interrupt
 * @param intIndex The index of the interrupt
 */
void DigitalSensor::setIntIndex(uint8_t intIndex)
{
	m_intIndex = intIndex;
}
/**
 * @brief Getter for the pin attribute
 * @return The pin in uint16_t
 */
GPIO_TypeDef* DigitalSensor::getPort(void)
{
	return m_port;
}
/**
 * @brief Getter for the speed attribute
 * @return The speed in 0-100%
 */
uint16_t DigitalSensor::getPin(void) const
{
	return m_pin;
}
/**
 * @brief Getter for the sensor interrupt
 * @return The index of the interrupt
 */
uint8_t DigitalSensor::getIntIndex(void) const
{
	return m_intIndex;
}
