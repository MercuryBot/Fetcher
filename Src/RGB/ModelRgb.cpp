/**
* @file ModelRgb.cpp
* @brief Definition file for the ModelRgb class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 15th, 2018
*
* The ModelRgb is a modelisation of the rgb sensor state
*
*/

#include "ModelRgb.hpp"

/**
 * @brief The class' base constructor
 */
ModelRgb::ModelRgb()
{
	m_GPIOx = nullptr;
	m_GPIO_Pin = 0;
	m_lastWL = 0;
}

/**
 * @brief The class' parametered constuctor
 * @param GPIOx GPIO port to set the data-in port to
 * @param GPIO_Pin GPIO pin to set the data-in pin to
 */
ModelRgb::ModelRgb(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	m_GPIOx = GPIOx;
	m_GPIO_Pin = GPIO_Pin;
	m_lastWL = 0;
}

/**
 * @brief The class' base destructor
 */
ModelRgb::~ModelRgb()
{

}

/**
 * @brief Setter for the GPIO data-in port
 * @param GPIOx New port value to set
 */
void ModelRgb::setGPIO(GPIO_TypeDef* GPIOx)
{
	m_GPIOx = GPIOx;
}

/**
 * @brief Setter for the GPIO data-in pin
 * @param GPIO_Pin New pin value to set
 */
void ModelRgb::setGPIOPin(uint16_t GPIO_Pin)
{
	m_GPIO_Pin = GPIO_Pin;
}

/**
 * @brief Manual setter for the last read wavelength
 * @param lastWL New wavelength to set
 */
void ModelRgb::setLastWL(uint64_t lastWL)
{
	m_lastWL = lastWL;
}

/**
 * @brief Getter for the GPIO data-in port
 * @return Data-in port value
 */
GPIO_TypeDef* ModelRgb::getGPIO(void) const
{
	return m_GPIOx;
}

/**
 * @brief Getter for the GPIO data-in pin
 * @return Data-in pin value
 */
uint16_t ModelRgb::getGPIOPin(void) const
{
	return m_GPIO_Pin;
}

/**
 * @brief Getter for the last wavelength read
 * @return Value of the last scanned wavelength
 */
uint64_t ModelRgb::getLastWL(void) const
{
	return m_lastWL;
}

/**
 * @brief Execute a wavelength scan
 * @return Value of the last scanned wavelength
 */
uint64_t ModelRgb::scanWL(void)
{
	uint64_t counter = 0;
	bool inScan = true;
	sstep_t step = SeekLowState;

	if(m_GPIOx == nullptr) //Not initialised
		return counter;

	do {
		switch(step)
		{
			case SeekLowState:
				if(!syst->GPIO()->readPin(m_GPIOx, m_GPIO_Pin))
					step = WaitForRisingEdge;
			break;
			case WaitForRisingEdge:
				if(syst->GPIO()->readPin(m_GPIOx, m_GPIO_Pin))
					step = WaitForFallingEdge;
			break;
			case WaitForFallingEdge:
				if(!syst->GPIO()->readPin(m_GPIOx, m_GPIO_Pin))
					step = Done;
				counter += 50;
				DWT_Delay_us(50);
			break;
			case Done:
				inScan = false;
			break;
			default:
				inScan = false;
			break;
		}
		//TODO: Add timeout value/alert/watchdog
	}while(inScan);

	return counter;
}
