/**
* @file RgbScanManager.hpp
* @brief Header file for the RgbScanManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 15th, 2018
*
* The RgbScanManager provides more advanced RGB scanning and conversion to position code capabilities
*
*/

#ifndef RGBSCANMANAGER_HPP_
#define RGBSCANMANAGER_HPP_

#include "ColorCodes.h"
#include "ColorBar.h"
#include "PosCode.h"
#include "ModelRgb.hpp"
#include "SensorIR/ModelIR.hpp"

/**
 * @def LOW
 * @brief Associates the LOW keyword to the value 0
 */
#define LOW 0
/**
 * @def HIGH
 * @brief Associates the HIGH keyword to the value 1
 */
#define HIGH 1

/**
 * @def CONFIG_S3_R
 * @brief State of the S3 pin when reading R color wavelength
 */
#define CONFIG_S3_R LOW
/**
 * @def CONFIG_S2_R
 * @brief State of the S2 pin when reading R color wavelength
 */
#define CONFIG_S2_R LOW

/**
 * @def CONFIG_S3_G
 * @brief State of the S3 pin when reading G color wavelength
 */
#define CONFIG_S3_G HIGH
/**
 * @def CONFIG_S2_G
 * @brief State of the S2 pin when reading G color wavelength
 */
#define CONFIG_S2_G HIGH

/**
 * @def CONFIG_S3_B
 * @brief State of the S3 pin when reading B color wavelength
 */
#define CONFIG_S3_B HIGH
/**
 * @def CONFIG_S2_B
 * @brief State of the S2 pin when reading B color wavelength
 */
#define CONFIG_S2_B LOW

/**
 * @def CONFIG_S3_C
 * @brief State of the S3 pin when reading C color wavelength
 */
#define CONFIG_S3_C LOW
/**
 * @def CONFIG_S2_C
 * @brief State of the S2 pin when reading C color wavelength
 */
#define CONFIG_S2_C HIGH


/**
 * @class RgbScanManager
 * @brief Provides more advanced RGB scanning and conversion to position code capabilities
 */
class RgbScanManager {

private:

	/**
	 * @brief Length of a code
	 */
	uint8_t m_codeLength;

	/**
	 * @brief Lastly read position code
	 */
	PosCode m_currentPosCode;

	/**
	 * @brief Lastly read raw color bar
	 */
	ColorBar m_currentColorBar;

	/**
	 * @brief List of active RGB sensors
	 */
	LinkedList<ModelRgb> m_sensorList;

	/**
	 * @brief Model of the IR sensor detecting the presence of a code to read
	 */
	ModelIR m_sensorIr;

public:
	/**
	 * @brief The class' base constructor
	 */
	RgbScanManager();

	/**
	 * @brief The class' parametered constructor
	 * @param codeLength Length of code to set
	 */
	RgbScanManager(uint8_t codeLength);

	/**
	 * @brief The class' base destructor
	 */
	~RgbScanManager();

	/**
	 * @brief Getter of the set code length
	 * @return Value of the code length
	 */
	uint8_t getCodeLength(void) const;

	/**
	 * @brief Getter of the last read position code
	 * @return The last read position code formatted in the PosCode struct format
	 */
	PosCode getCurrentPosCode(void) const;

	/**
	 * @brief Getter of the last read position code
	 * @return The last read position code formatted in the raw ColorBar struct format
	 */
	ColorBar getCurrentColorBar(void) const;

	/**
	 * @brief Getter of the pointer to the RGB sensors model
	 * @param bar Index of the color bar facing the RGB sensor
	 * @return Pointer to the RGB sensor model
	 */
	ModelRgb* sensorRgb(uint8_t bar);

	/**
	 * @brief Getter of the last read color wavelength in front of a bar
	 * @param bar Index of the selected color bar
	 * @param color Color to target (R, G, B, C)
	 * @return Wavelength read
	 */
	uint64_t getCurrentWL(uint8_t bar, uint8_t color) const;

	/**
	 * @brief Getter of the pointer to the IR sensor model
	 * @return Pointer to the IR sensor model
	 */
	ModelIR* sensorIr(void);

	/**
	 * @brief Manual setter for a last read wavelength
	 * @param bar Index of the selected color bar
	 * @param color Color to target (R, G, B, C)
	 * @param Wavelength to set
	 */
	void setCurrentWL(uint8_t bar, uint8_t color, uint64_t wl);

	/**
	 * @brief Manual setter for a last position code
	 * @param bar Index of the selected color bar
	 * @param code Associated code with the bar
	 */
	void setCurrentPosCode(uint8_t bar, uint8_t code);

	/**
	 * @brief Manual setter for a last position code
	 * @param code Code to set last position code to
	 */
	void setCurrentPosCode(PosCode code);

	/**
	 * @brief Manual setter for a last read ColorBar
	 * @param colorBar Copy of the color bar to the last read ColorBar to
	 */
	void setCurrentColorBar(ColorBar colorBar);

	/**
	 * @brief Setter for the code length
	 * @param length Length to set the code length to
	 */
	void setCodeLength(uint8_t length);

	/**
	 * @brief Manual setter for the IR sensor model
	 * @param sensorIr Copy of the IR sensor model to set
	 */
	void setSensorIr(ModelIR sensorIr);

	/**
	 * @brief Scans the wavelength of a certain bar for a given color focus
	 * @param bar Index of the selected color bar
	 * @param color Color to target (R, G, B, C)
	 * @return Read wavelength
	 */
	uint64_t scanWL(uint8_t bar, uint8_t color);

	/**
	 * @brief Executes a full scan of all bars of all color focus
	 * @return Read and formatted position code
	 */
	PosCode fullScan(void);

	/**
	 * @brief Converts a position formatted to PosCode struct to a ColorBar struct
	 * @param code Code to convert
	 * @return Converted position code
	 */
	ColorBar codeToBar(PosCode code);

	/**
	 * @brief Converts a position formatted to ColorBar struct to a PosCode struct
	 * @param bar Bar to convert
	 * @return Converted position code
	 */
	PosCode barToCode(ColorBar bar);

	/**
	 * @brief Toggles the S2 selection pin
	 * @param state Logic state to set the pin to
	 */
	void toggleS2Pin(bool state);

	/**
	 * @brief Toggles the S3 selection pin
	 * @param state Logic state to set the pin to
	 */
	void toggleS3Pin(bool state);

	/**
	 * @brief Toggles the S2 and S3 selection pins to focus on a specific color range
	 * @param color Color range to focus on
	 */
	void setColorSelect(int color);

	/**
	 * @brief Periodic update of the class
	 * @return False if no changes have been performed, True if changes have been performed
	 */
	bool update(void);
};

#endif /* RGBSCANMANAGER_HPP_ */
