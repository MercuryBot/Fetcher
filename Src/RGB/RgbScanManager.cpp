/**
* @file RgbScanManager.cpp
* @brief Definition file for the RgbScanManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 15th, 2018
*
* The RgbScanManager provides more advanced RGB scanning and conversion to position code capabilities
*
*/

#include "RgbScanManager.hpp"

/**
 * @brief The class' base constructor
 */
RgbScanManager::RgbScanManager()
{
	m_codeLength = COLOR_SQUR_QTY;
	m_sensorList = LinkedList<ModelRgb>();

	memset(m_currentPosCode.code, 0, m_codeLength);

	for(uint8_t i = 0; i < m_codeLength; ++i)
	{
		memset(m_currentColorBar.colorWL[i], 0, COLOR_TYPES_QTY);
		switch(i)
		{
		case 0:
			m_sensorList.append(*(new ModelRgb(GPIOC, RGB_INPUT_0_Pin)));
			break;
		case 1:
			m_sensorList.append(*(new ModelRgb(GPIOC, RGB_INPUT_1_Pin)));
			break;
		case 2:
			m_sensorList.append(*(new ModelRgb(GPIOC, RGB_INPUT_2_Pin)));
			break;
		default:
			m_sensorList.append(*(new ModelRgb()));
			break;
		}
	}

	m_sensorIr = ModelIR(IR_INTERRUPT3_GPIO_Port, IR_INTERRUPT3_Pin, RGB_INTERRUPT_IDX);
}

/**
 * @brief The class' parametered constructor
 * @param codeLength Length of code to set
 */
RgbScanManager::RgbScanManager(uint8_t codeLength)
{
	m_codeLength = codeLength;
	m_sensorList = LinkedList<ModelRgb>();

	memset(m_currentPosCode.code, 0, m_codeLength);

	for(uint8_t i = 0; i < m_codeLength; ++i)
	{
		memset(m_currentColorBar.colorWL[i], 0, COLOR_TYPES_QTY);
		switch(i)
		{
		case 0:
			m_sensorList.append(*(new ModelRgb(GPIOC, RGB_INPUT_0_Pin)));
			break;
		case 1:
			m_sensorList.append(*(new ModelRgb(GPIOC, RGB_INPUT_1_Pin)));
			break;
		case 2:
			m_sensorList.append(*(new ModelRgb(GPIOC, RGB_INPUT_2_Pin)));
			break;
		default:
			m_sensorList.append(*(new ModelRgb()));
			break;
		}
	}

	m_sensorIr = ModelIR(IR_INTERRUPT3_GPIO_Port, IR_INTERRUPT3_Pin, RGB_INTERRUPT_IDX);
}

/**
 * @brief The class' base destructor
 */
RgbScanManager::~RgbScanManager()
{

}

/**
 * @brief Getter of the set code length
 * @return Value of the code length
 */
uint8_t RgbScanManager::getCodeLength(void) const
{
	return m_codeLength;
}

/**
 * @brief Getter of the last read position code
 * @return The last read position code formatted in the PosCode struct format
 */
PosCode RgbScanManager::getCurrentPosCode(void) const
{
	return m_currentPosCode;
}

/**
 * @brief Getter of the last read position code
 * @return The last read position code formatted in the raw ColorBar struct format
 */
ColorBar RgbScanManager::getCurrentColorBar(void) const
{
	return m_currentColorBar;
}

/**
 * @brief Getter of the pointer to the RGB sensors model
 * @param bar Index of the color bar facing the RGB sensor
 * @return Pointer to the RGB sensor model
 */
ModelRgb* RgbScanManager::sensorRgb(uint8_t bar)
{
	if(bar >= m_sensorList.size() )
		return nullptr;
	else
		return &m_sensorList[bar];
}

/**
 * @brief Getter of the last read color wavelength in front of a bar
 * @param bar Index of the selected color bar
 * @param color Color to target (R, G, B, C)
 * @return Wavelength read
 */
uint64_t RgbScanManager::getCurrentWL(uint8_t bar, uint8_t color) const
{
	return m_currentColorBar.colorWL[bar][color];
}

/**
 * @brief Getter of the pointer to the IR sensor model
 * @return Pointer to the IR sensor model
 */
ModelIR* RgbScanManager::sensorIr(void)
{
	return &m_sensorIr;
}

/**
 * @brief Manual setter for a last read wavelength
 * @param bar Index of the selected color bar
 * @param color Color to target (R, G, B, C)
 * @param Wavelength to set
 */
void RgbScanManager::setCurrentWL(uint8_t bar, uint8_t color, uint64_t wl)
{
	m_currentColorBar.colorWL[bar][color] = wl;
}

/**
 * @brief Manual setter for a last position code
 * @param bar Index of the selected color bar
 * @param code Associated code with the bar
 */
void RgbScanManager::setCurrentPosCode(uint8_t bar, uint8_t code)
{
	m_currentPosCode.code[bar] = code;
}

/**
 * @brief Manual setter for a last position code
 * @param code Code to set last position code to
 */
void RgbScanManager::setCurrentPosCode(PosCode code)
{
	m_currentPosCode = code;
}

/**
 * @brief Manual setter for a last read ColorBar
 * @param colorBar Copy of the color bar to the last read ColorBar to
 */
void RgbScanManager::setCurrentColorBar(ColorBar colorBar)
{
	m_currentColorBar = colorBar;
}

/**
 * @brief Setter for the code length
 * @param length Length to set the code length to
 */
void RgbScanManager::setCodeLength(uint8_t length)
{
	m_codeLength = length;
}

/**
 * @brief Manual setter for the IR sensor model
 * @param sensorIr Copy of the IR sensor model to set
 */
void RgbScanManager::setSensorIr(ModelIR sensorIr)
{
	m_sensorIr = sensorIr;
}

/**
 * @brief Scans the wavelength of a certain bar for a given color focus
 * @param bar Index of the selected color bar
 * @param color Color to target (R, G, B, C)
 * @return Read wavelength
 */
uint64_t RgbScanManager::scanWL(uint8_t bar, uint8_t color)
{
	setColorSelect(color);

	m_currentColorBar.colorWL[bar][color] = m_sensorList[bar].scanWL();
	m_currentPosCode = barToCode(m_currentColorBar);

	return m_currentColorBar.colorWL[bar][color];
}

/**
 * @brief Executes a full scan of all bars of all color focus
 * @return Read and formatted position code
 */
PosCode RgbScanManager::fullScan(void)
{
	for(uint8_t i = 0; i < COLOR_TYPES_QTY; ++i)
	{
		setColorSelect(i);

		for(uint8_t j = 0; j < COLOR_SQUR_QTY; ++j)
		{
			m_currentColorBar.colorWL[j][i] = m_sensorList[j].scanWL();
			HAL_IWDG_Refresh(&hiwdg);
		}
	}

	m_currentPosCode = barToCode(m_currentColorBar);

	return m_currentPosCode;
}

/**
 * @brief Converts a position formatted to PosCode struct to a ColorBar struct
 * @param code Code to convert
 * @return Converted position code
 */
ColorBar RgbScanManager::codeToBar(PosCode code)
{
	ColorBar returnBar = {0};

	for(uint8_t i = 0; i < COLOR_SQUR_QTY; ++i)
	{
		if(code.code[i] == RED_CODE)
		{
			returnBar.colorWL[i][R] = (RED_CODE_MAX_WL_R + RED_CODE_MIN_WL_R)/2;
			returnBar.colorWL[i][G] = (RED_CODE_MAX_WL_G + RED_CODE_MIN_WL_G)/2;
			returnBar.colorWL[i][B] = (RED_CODE_MAX_WL_B + RED_CODE_MIN_WL_B)/2;
		}
		else if(code.code[i] == GREEN_CODE)
		{
			returnBar.colorWL[i][R] = (GREEN_CODE_MAX_WL_R + GREEN_CODE_MIN_WL_R)/2;
			returnBar.colorWL[i][G] = (GREEN_CODE_MAX_WL_G + GREEN_CODE_MIN_WL_G)/2;
			returnBar.colorWL[i][B] = (GREEN_CODE_MAX_WL_B + GREEN_CODE_MIN_WL_B)/2;
		}
		else if(code.code[i] == 	BLUE_CODE)
		{
			returnBar.colorWL[i][R] = (BLUE_CODE_MAX_WL_R + BLUE_CODE_MIN_WL_R)/2;
			returnBar.colorWL[i][G] = (BLUE_CODE_MAX_WL_G + BLUE_CODE_MIN_WL_G)/2;
			returnBar.colorWL[i][B] = (BLUE_CODE_MAX_WL_B + BLUE_CODE_MIN_WL_B)/2;
		}
		else if(code.code[i] == PURPLE_CODE)
		{
			returnBar.colorWL[i][R] = (PURPLE_CODE_MAX_WL_R + PURPLE_CODE_MIN_WL_R)/2;
			returnBar.colorWL[i][G] = (PURPLE_CODE_MAX_WL_G + PURPLE_CODE_MIN_WL_G)/2;
			returnBar.colorWL[i][B] = (PURPLE_CODE_MAX_WL_B + PURPLE_CODE_MIN_WL_B)/2;
		}
		else if(code.code[i] == UNKNOWN_CODE)
		{
			returnBar.colorWL[i][R] = 0;
			returnBar.colorWL[i][G] = 0;
			returnBar.colorWL[i][B] = 0;
		}
	}

	return returnBar;
}


/**
 * @brief Converts a position formatted to ColorBar struct to a PosCode struct
 * @param bar Bar to convert
 * @return Converted position code
 */
PosCode RgbScanManager::barToCode(ColorBar bar)
{
	PosCode returnCode = {0};

	for(uint8_t i = 0; i < COLOR_SQUR_QTY; ++i)
	{
		if((bar.colorWL[i][R] <= RED_CODE_MAX_WL_R && bar.colorWL[i][R] >= RED_CODE_MIN_WL_R) &&
				(bar.colorWL[i][G] <= RED_CODE_MAX_WL_G && bar.colorWL[i][G] >= RED_CODE_MIN_WL_G) &&
				(bar.colorWL[i][B] <= RED_CODE_MAX_WL_B && bar.colorWL[i][B] >= RED_CODE_MIN_WL_B))
		{
			returnCode.code[i] = RED_CODE;
		}
		else if((bar.colorWL[i][R] <= GREEN_CODE_MAX_WL_R && bar.colorWL[i][R] >= GREEN_CODE_MIN_WL_R) &&
				(bar.colorWL[i][G] <= GREEN_CODE_MAX_WL_G && bar.colorWL[i][G] >= GREEN_CODE_MIN_WL_G) &&
				(bar.colorWL[i][B] <= GREEN_CODE_MAX_WL_B && bar.colorWL[i][B] >= GREEN_CODE_MIN_WL_B))
		{
			returnCode.code[i] = GREEN_CODE;
		}
		else if((bar.colorWL[i][R] <= BLUE_CODE_MAX_WL_R && bar.colorWL[i][R] >= BLUE_CODE_MIN_WL_R) &&
				(bar.colorWL[i][G] <= BLUE_CODE_MAX_WL_G && bar.colorWL[i][G] >= BLUE_CODE_MIN_WL_G) &&
				(bar.colorWL[i][B] <= BLUE_CODE_MAX_WL_B && bar.colorWL[i][B] >= BLUE_CODE_MIN_WL_B))
		{
			returnCode.code[i] = BLUE_CODE;
		}
		else if((bar.colorWL[i][R] <= PURPLE_CODE_MAX_WL_R && bar.colorWL[i][R] >= PURPLE_CODE_MIN_WL_R) &&
				(bar.colorWL[i][G] <= PURPLE_CODE_MAX_WL_G && bar.colorWL[i][G] >= PURPLE_CODE_MIN_WL_G) &&
				(bar.colorWL[i][B] <= PURPLE_CODE_MAX_WL_B && bar.colorWL[i][B] >= PURPLE_CODE_MIN_WL_B))
		{
			returnCode.code[i] = PURPLE_CODE;
		}
		else
		{
			returnCode.code[i] = UNKNOWN_CODE;
		}
	}

	return returnCode;
}

/**
 * @brief Toggles the S2 selection pin
 * @param state Logic state to set the pin to
 */
void RgbScanManager::toggleS2Pin(bool state)
{
	if(state)
		syst->GPIO()->setPin(GPIOA, RGB_OUTPUT_S2_Pin);
	else
		syst->GPIO()->resetPin(GPIOA, RGB_OUTPUT_S2_Pin);
}

/**
 * @brief Toggles the S3 selection pin
 * @param state Logic state to set the pin to
 */
void RgbScanManager::toggleS3Pin(bool state)
{
	if(state)
		syst->GPIO()->setPin(GPIOA, RGB_OUTPUT_S3_Pin);
	else
		syst->GPIO()->resetPin(GPIOA, RGB_OUTPUT_S3_Pin);
}

/**
 * @brief Toggles the S2 and S3 selection pins to focus on a specific color range
 * @param color Color range to focus on
 */
void RgbScanManager::setColorSelect(int color)
{
	switch(color)
	{
	case R:
		toggleS2Pin(CONFIG_S2_R);
		toggleS3Pin(CONFIG_S3_R);
		break;
	case G:
		toggleS2Pin(CONFIG_S2_G);
		toggleS3Pin(CONFIG_S3_G);
		break;
	case B:
		toggleS2Pin(CONFIG_S2_B);
		toggleS3Pin(CONFIG_S3_B);
		break;
	default:
		toggleS2Pin(CONFIG_S2_C);
		toggleS3Pin(CONFIG_S3_C);
		break;
	}
}

/**
 * @brief Periodic update of the class
 * @return False if no changes have been performed, True if changes have been performed
 */
bool RgbScanManager::update(void)
{
	m_sensorIr.update();

	if(syst->GPIO()->readPin(m_sensorIr.getPort(), m_sensorIr.getPin()))
		return false;

	fullScan();

	return true;
}
