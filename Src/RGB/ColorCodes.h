/**
* @file ColorCodes.h
* @brief Defines color wavelength ranges for color to code conversion
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date May 2nd, 2018
*
* This file defines color wavelength ranges for color to code conversion
*
*/

#ifndef COLORCODES_H_
#define COLORCODES_H_

/**
 * @def RED_CODE
 * @brief Code value associated with a 'red' color
 */
#define RED_CODE 0
/**
 * @def GREEN_CODE
 * @brief Code value associated with a 'green' color
 */
#define GREEN_CODE 1
/**
 * @def BLUE_CODE
 * @brief Code value associated with a 'blue' color
 */
#define BLUE_CODE 2
/**
 * @def PURPLE_CODE
 * @brief Code value associated with a 'purple' color
 */
#define PURPLE_CODE 3
/**
 * @def UNKNOWN_CODE
 * @brief Code value associated with an unknown color
 */
#define UNKNOWN_CODE 9
/**
 * @def RED_CODE_MAX_WL_R
 * @brief Maximum wavelength of red in a 'red' color
 */
#define RED_CODE_MAX_WL_R 7650
/**
 * @def RED_CODE_MIN_WL_R
 * @brief Minimum wavelength of red in a 'red' color
 */
#define RED_CODE_MIN_WL_R 2200
/**
 * @def RED_CODE_MAX_WL_G
 * @brief Maximum wavelength of green in a 'red' color
 */
#define RED_CODE_MAX_WL_G 14000
/**
 * @def RED_CODE_MIN_WL_G
 * @brief  Minimum wavelength of green in a 'red' color
 */
#define RED_CODE_MIN_WL_G 8400
/**
 * @def RED_CODE_MAX_WL_B
 * @brief  Maximum wavelength of blue in a 'red' color
 */
#define RED_CODE_MAX_WL_B 11500
/**
 * @def RED_CODE_MIN_WL_B
 * @brief  Minimum wavelength of blue in a 'red' color
 */
#define RED_CODE_MIN_WL_B 7000
/**
 * @def GREEN_CODE_MAX_WL_R
 * @brief Maximum wavelength of red in a 'green' color
 */
#define GREEN_CODE_MAX_WL_R 7700
/**
 * @def GREEN_CODE_MIN_WL_R
 * @brief Minimum wavelength of red in a 'green' color
 */
#define GREEN_CODE_MIN_WL_R 5000
/**
 * @def GREEN_CODE_MAX_WL_G
 * @brief Maximum wavelength of green in a 'green' color
 */
#define GREEN_CODE_MAX_WL_G 7250
/**
 * @def GREEN_CODE_MIN_WL_G
 * @brief Minimum wavelength of green in a 'green' color
 */
#define GREEN_CODE_MIN_WL_G 5000
/**
 * @def GREEN_CODE_MAX_WL_B
 * @brief Maximum wavelength of blue in a 'green' color
 */
#define GREEN_CODE_MAX_WL_B 8000
/**
 * @def GREEN_CODE_MIN_WL_B
 * @brief Minimum wavelength of blue in a 'green' color
 */
#define GREEN_CODE_MIN_WL_B 5000
/**
 * @def BLUE_CODE_MAX_WL_R
 * @brief Maximum wavelength of red in a 'blue' color
 */
#define BLUE_CODE_MAX_WL_R 7300
/**
 * @def BLUE_CODE_MIN_WL_R
 * @brief Minimum wavelength of red in a 'blue' color
 */
#define BLUE_CODE_MIN_WL_R 4800
/**
 * @def BLUE_CODE_MAX_WL_G
 * @brief Maximum wavelength of green in a 'blue' color
 */
#define BLUE_CODE_MAX_WL_G 6500
/**
 * @def BLUE_CODE_MIN_WL_G
 * @brief Minimum wavelength of green in a 'blue' color
 */
#define BLUE_CODE_MIN_WL_G 4850
/**
 * @def BLUE_CODE_MAX_WL_B
 * @brief Maximum wavelength of blue in a 'blue' color
 */
#define BLUE_CODE_MAX_WL_B 4700
/**
 * @def BLUE_CODE_MIN_WL_B
 * @brief Minimum wavelength of blue in a 'blue' color
 */
#define BLUE_CODE_MIN_WL_B 3300
/**
 * @def PURPLE_CODE_MAX_WL_R
 * @brief Maximum wavelength of red in a 'purple' color
 */
#define PURPLE_CODE_MAX_WL_R 10500
/**
 * @def PURPLE_CODE_MIN_WL_R
 * @brief Minimum wavelength of red in a 'purple' color
 */
#define PURPLE_CODE_MIN_WL_R 6000
/**
 * @def PURPLE_CODE_MAX_WL_G
 * @brief Maximum wavelength of green in a 'purple' color
 */
#define PURPLE_CODE_MAX_WL_G 11000
/**
 * @def PURPLE_CODE_MIN_WL_G
 * @brief Minimum wavelength of green in a 'purple' color
 */
#define PURPLE_CODE_MIN_WL_G 800
/**
 * @def PURPLE_CODE_MAX_WL_B
 * @brief Maximum wavelength of blue in a 'purple' color
 */
#define PURPLE_CODE_MAX_WL_B 11000
/**
 * @def PURPLE_CODE_MIN_WL_B
 * @brief Minimum wavelength of blue in a 'purple' color
 */
#define PURPLE_CODE_MIN_WL_B 5200


#endif /* COLORCODES_H_ */
