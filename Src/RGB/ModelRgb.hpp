/**
* @file ModelRgb.hpp
* @brief Header file for the ModelRgb class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 15th, 2018
*
* The ModelRgb is a modelisation of the rgb sensor state
*
*/

#ifndef MODELRGB_HPP_
#define MODELRGB_HPP_

#include "ColorBar.h"
#include "ControlSystem.hpp"
#include "dwt_stm32_delay.h"

/**
 * @enum ScanSteps
 * @brief Qualifies the different scanning steps on a pin level
 */
typedef enum ScanSteps{
	SeekLowState = 0x00,//!< SeekLowState
	WaitForRisingEdge,  //!< WaitForRisingEdge
	WaitForFallingEdge, //!< WaitForFallingEdge
	Done                //!< Done
}sstep_t;

/**
 * @class ModelRgb
 * @brief Modelisation of the rgb sensor state
 */
class ModelRgb {

private:

	/**
	 * @brief GPIO data-in port
	 */
	GPIO_TypeDef* m_GPIOx;

	/**
	 * @brief GPIO data-in pin
	 */
	uint16_t m_GPIO_Pin;

	/**
	 * @brief Last scanned wavelength
	 */
	uint64_t m_lastWL;

public:

	/**
	 * @brief The class' base constructor
	 */
	ModelRgb();

	/**
	 * @brief The class' parametered constuctor
	 * @param GPIOx GPIO port to set the data-in port to
	 * @param GPIO_Pin GPIO pin to set the data-in pin to
	 */
	ModelRgb(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

	/**
	 * @brief The class' base destructor
	 */
	~ModelRgb();

	/**
	 * @brief Setter for the GPIO data-in port
	 * @param GPIOx New port value to set
	 */
	void setGPIO(GPIO_TypeDef* GPIOx);

	/**
	 * @brief Setter for the GPIO data-in pin
	 * @param GPIO_Pin New pin value to set
	 */
	void setGPIOPin(uint16_t GPIO_Pin);

	/**
	 * @brief Manual setter for the last read wavelength
	 * @param lastWL New wavelength to set
	 */
	void setLastWL(uint64_t lastWL);

	/**
	 * @brief Getter for the GPIO data-in port
	 * @return Data-in port value
	 */
	GPIO_TypeDef* getGPIO(void) const;

	/**
	 * @brief Getter for the GPIO data-in pin
	 * @return Data-in pin value
	 */
	uint16_t getGPIOPin(void) const;

	/**
	 * @brief Getter for the last wavelength read
	 * @return Value of the last scanned wavelength
	 */
	uint64_t getLastWL(void) const;

	/**
	 * @brief Execute a wavelength scan
	 * @return Value of the last scanned wavelength
	 */
	uint64_t scanWL(void);
};

#endif /* MODELRGB_HPP_ */
