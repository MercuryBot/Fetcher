/**
* @file ColorBar.h
* @brief Header file for the ColorBar struct
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 15th, 2018
*
* The ColorBar structure represents the raw color read from a position code
*
*/

#ifndef COLORBAR_H_
#define COLORBAR_H_

/**
 * @def COLOR_SQUR_QTY
 * @brief Color bars quantity
 */
#define COLOR_SQUR_QTY 3
/**
 * @def COLOR_TYPES_QTY
 * @brief Color wavelengths to scan
 */
#define COLOR_TYPES_QTY 3
/**
 * @def R
 * @brief Red scanning wavelength
 */
#define R 0
/**
 * @def G
 * @brief Green scanning wavelength
 */
#define G 1
/**
 * @def B
 * @brief Blue scanning wavelength
 */
#define B 2

#include <stdint.h>

/**
 * @struct ColorBar
 * @brief Represents the raw color read from a position code
 */
struct ColorBar {
	/**
	 * @brief Array representing the color wavelengths for every color bar available
	 */
	uint64_t colorWL[COLOR_SQUR_QTY][COLOR_TYPES_QTY];
};


#endif /* COLORBAR_H_ */
