/**
* @file PosCode.h
* @brief Header file for the PosCode struct
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 15th, 2018
*
* The PosCode structure illustrates the structure of a position code
*
*/

#ifndef POSCODE_H_
#define POSCODE_H_

#include "ColorBar.h"

/**
 * @struct PosCode
 * @brief Illustrates the structure of a position code
 */
struct PosCode {
	/**
	 * @brief Array representing the position code
	 */
	uint8_t code[COLOR_SQUR_QTY];
};

#endif /* POSCODE_H_ */
