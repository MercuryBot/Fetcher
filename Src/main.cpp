/**
* @file main.cpp
* @brief Main routine file
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 2nd, 2018
*
* This file is the main routine file. It calls the execution of system tasks.
*
*/

#include <iostream>
#include <stdio.h>
#include <string.h>

#include "main.h"
#include "stm32f4xx_hal.h"
#include "dwt_stm32_delay.h"
#include "LinkedList.hpp"
#include "SysClock.h"
#include "RgbScanManager.hpp"
#include "RfManager.hpp"
#include "AutomationManager.hpp"

/**
* @brief Main instance of the RfManager
*/
RfManager telecom;

/**
* @brief Main instance of the AutomationManager
*/
AutomationManager automation;

/**
* @brief Main routine function
*/
int main(void)
{
	HAL_Init();
	SystemClock_Config();

	MX_IWDG_Init();
	initAllTimers();

	syst->init();
	syst->GPIO()->startADC(0);

	startAllTimers();

	telecom.init();

	for(;;)
	{
		telecom.intTask();
		automation.mainTask();
		telecom.mainTask(&automation);
		HAL_IWDG_Refresh(&hiwdg);
	}
}

/**
* @brief Main timer interrupt for every millisecond
* @param htim Pointer to the timer handled called
*/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim->Instance==TIM5)
		telecom.timerTask();
}


/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line)
{

}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{ 

}
#endif
