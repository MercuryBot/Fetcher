/**
 * @file BatteryMonitoring.hpp
 * @brief Header file for the BatteryMonitoring class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The BatteryMonitoring class provides easier management of the battery
 *
 */
#ifndef BATTERYMONITORING_HPP_
#define BATTERYMONITORING_HPP_

#include "ControlSystem.hpp"
/**
 * @class BatteryMonitoring
 * @brief The BatteryMonitoring class provides easier management of the battery
 */
class BatteryMonitoring {
	/**
	 * @brief Handler for the analog to digital converter
	 */
	ADC_HandleTypeDef* m_adc;
	/**
	 * @brief The battery level in %
	 */
	float m_batteryLevel;
	/**
	 * @brief The battery tension in volts
	 */
	float m_batteryTension;
	/**
	 * @brief The battery level threshold for the low battery warning in 0-100%
	 */
	float m_batteryLevelThreshold;
	/**
	 * @brief The battery level threshold for the low battery warning in volts
	 */
	float m_batteryTensionThreshold;
	/**
	 * @brief The battery maximum tension in volts
	 */
	float m_maxTension;
	/**
	 * @brief The battery minimum tension in volts
	 */
	float m_minTension;
	/**
	 * @brief The value needed for conversion from ADc value to real tension (0~15V)
	 */
	float m_divider;
	/**
	 * @brief Conversion from tension to percentage
	 * @param value The value to convert
	 */
	float tensionToPercent(float value);
	/**
	 * @brief Conversion from percentage to tension
	 * @param value The value to convert
	 */
	float percentToTension(float value);
public:
	/**
	 * @brief The class base constructor
	 */
	BatteryMonitoring();
	/**
	 * @brief The class constructor with parameter
	 * @param adc The handler for the ADC
	 * @param minTension Value at witch the battery is considered at 100%
	 * @param maxTension Value at witch the battery is considered at 0%
	 */
	BatteryMonitoring(ADC_HandleTypeDef* adc, float minTension , float maxTension);
	/**
	 * @brief The class base destructor
	 */
	~BatteryMonitoring();
	/**
	 * @brief Setter for the maximum tension
	 * @param value The new value
	 */
	void setMaxTension(float value);
	/**
	 * @brief Setter for the minimum tension
	 * @param value The new value
	 */
	void setMinTension(float value);
	/**
	 * @brief Setter for the threshold in volts
	 * @param value The new value
	 */
	void setTensionThreshold(float value);
	/**
	 * @brief Setter for the threshold in percentage
	 * @param value The new value
	 */
	void setLevelThreshold(float value);
	/**
	 * @brief The getter for the level in percentage
	 * @return The value in float
	 */
	float getLevel() const;
	/**
	 * @brief The getter for the level in volts
	 * @return The value in float
	 */
	float getTension() const;
	/**
	 * @brief Check if the battery is below the threshold level or tension
	 * @return True is the value is below the threshold, false if it is not
	 */
	bool batteryNeedCharging();
	/**
	 * @brief Updates the battery value in the class
	 */
	void readBattery();
};

#endif /* BATTERYMONITORING_HPP_ */
