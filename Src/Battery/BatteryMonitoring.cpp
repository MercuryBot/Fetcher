/**
 * @file BatteryMonitoring.cpp
 * @brief Definition file for the BatteryMonitoring class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The BatteryMonitoring class provides easier management of the battery
 *
 */
#include "BatteryMonitoring.hpp"
/**
 * @brief The class base constructor
 */
BatteryMonitoring::BatteryMonitoring()
{
	m_batteryLevel = 0;
	m_batteryTension = 0;
	m_batteryLevelThreshold = 0;
	m_batteryTensionThreshold = 0;
	m_maxTension = 0;
	m_minTension = 0;
	m_divider = 1;
	m_adc = nullptr;
}
/**
 * @brief The class constructor with parameter
 * @param adc The handler for the ADC
 * @param minTension Value at witch the battery is considered at 100%
 * @param maxTension Value at witch the battery is considered at 0%
 */
BatteryMonitoring::BatteryMonitoring(ADC_HandleTypeDef* adc, float minTension, float maxTension)
{
	m_batteryLevel = 0;
	m_batteryTension = 0;
	m_batteryLevelThreshold = 0;
	m_batteryTensionThreshold = 0;
	m_maxTension = maxTension;
	m_minTension = minTension;
	m_divider = 1;
	m_adc = adc;
}
/**
 * @brief The class base destructor
 */
BatteryMonitoring::~BatteryMonitoring()
{

}
/**
 * @brief Setter for the maximum tension
 * @param value The new value
 */
void BatteryMonitoring::setMaxTension(float value)
{
	m_maxTension = value;
	readBattery();
}
/**
 * @brief Setter for the minimum tension
 * @param value The new value
 */
void BatteryMonitoring::setMinTension(float value)
{
	m_minTension = value;
	readBattery();
}
/**
 * @brief Setter for the threshold in volts
 * @param value The new value
 */
void BatteryMonitoring::setTensionThreshold(float value)
{
	m_batteryTensionThreshold = value;
	m_batteryLevelThreshold = tensionToPercent(value);
}
/**
 * @brief Setter for the threshold in percentage
 * @param value The new value
 */
void BatteryMonitoring::setLevelThreshold(float value)
{
	m_batteryLevelThreshold = value;
	m_batteryTensionThreshold = percentToTension(value);
}
/**
 * @brief The getter for the level in percentage
 * @return The value in float
 */
float BatteryMonitoring::getLevel() const
{
	return m_batteryLevel;
}
/**
 * @brief The getter for the level in volts
 * @return The value in float
 */
float BatteryMonitoring::getTension() const
{
	return m_batteryTension;
}
/**
 * @brief Check if the battery is below the threshold level or tension
 * @return True is the value is below the threshold, false if it is not
 */
bool BatteryMonitoring::batteryNeedCharging()
{
	return ((m_batteryLevelThreshold < m_batteryLevel) || (m_batteryTension < m_batteryTensionThreshold));
}
/**
 * @brief Updates the battery value in the class
 */
void BatteryMonitoring::readBattery()
{
	int64_t tmp = syst->GPIO()->readADCHandle(m_adc);

	if(tmp >= 0)
	{
		m_batteryTension = tmp/m_divider;
		m_batteryLevel = tensionToPercent(m_batteryTension);
	}
}
/**
 * @brief Conversion from tension to percentage
 * @param value The value to convert
 */
float BatteryMonitoring::tensionToPercent(float value)
{
	return (100 * (value - m_minTension)) / (m_maxTension - m_minTension);
}
/**
 * @brief Conversion from percentage to tension
 * @param value The value to convert
 */
float BatteryMonitoring::percentToTension(float value)
{
	return ((value * (m_maxTension - m_minTension)) / 100) + m_minTension;
}
