/**
 * @file ControlClutch.hpp
 * @brief Header file for the ControlClutch class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ControlClutch class provides easier management of clutches
 *
 */
#ifndef CONTROLCLUTCH_HPP_
#define CONTROLCLUTCH_HPP_

#include "ControlSystem.hpp"
/**
 * @class ControlClutch
 * @brief The ControlClutch class provides easier management of clutches
 */
class ControlClutch
{
private:
	/**
	 * @brief Port of the clutch
	 */
	GPIO_TypeDef *m_port;
	/**
	 * @brief Pin of the clutch
	 */
	uint16_t m_pin;
	bool m_isLocked;

public:
	/**
	 * @brief The class base constructor
	 */
	ControlClutch();
	/**
	 * @brief The class constructor with parameter
	 * @param port Port of the clutch
	 * @param pin Pin of the clutch
	 */
	ControlClutch(GPIO_TypeDef* port, uint16_t pin);
	/**
	 * @brief The class base destructor
	 */
	~ControlClutch();
	/**
	 * @brief Setter for the port attribute
	 * @param port The new port
	 */
	void setPort(GPIO_TypeDef* port);
	/**
	 * @brief Setter for the pin attribute
	 * @param pin The new pin
	 */
	void setPin(uint16_t pin);
	/**
		 * @brief Setter for the state of the clutch
		 * @param state The new state of the clutch, true for locked and false for unlocked
		 */
	void setIsLocked(bool state);
	/**
	 * @brief Getter for the port attribute
	 * @return A pointer to the port
	 */
	GPIO_TypeDef* getPort(void);
	/**
	 * @brief Getter for the pin attribute
	 * @return The pin in uint16_t
	 */
	uint16_t getPin(void) const;
	/**
	 * @brief Getter for the state of the clutch
	 * @return The current state of the clutch, true for locked and false for unlocked
	 */
	bool isLocked(void) const;
	/**
	 * @brief Function to toggle the state of the pin saved in the attribute
	 * @param state The state to change the pin to.
	 */
	void togglePin(bool state);
	/**
	 * @brief Function to release the motor (and set speed to 0)
	 */
	void release(void);
	/**
	 * @brief Lock the clutch to allow to motor to turn the wheels
	 */
	void lock(void);
};

#endif /* CONTROLCLUTCH_HPP_ */
