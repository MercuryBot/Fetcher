/**
 * @file ControlClutch.hpp
 * @brief Header file for the ControlClutch class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ControlClutch class provides easier management of clutches
 *
 */
#include "ControlClutch.hpp"
/**
 * @brief The class base constructor
 */
ControlClutch::ControlClutch()
{
	m_port = nullptr;
	m_pin = 0;
	m_isLocked = false;
}
/**
 * @brief The class constructor with parameter
 * @param port Port of the clutch
 * @param pin Pin of the clutch
 */
ControlClutch::ControlClutch(GPIO_TypeDef* port, uint16_t pin)
{
	m_port = port;
	m_pin = pin;
	m_isLocked = false;
}
/**
 * @brief The class base destructor
 */
ControlClutch::~ControlClutch()
{
	delete m_port;
}
/**
 * @brief Setter for the port attribute
 * @param port The new port
 */
void ControlClutch::setPort(GPIO_TypeDef* port)
{
	m_port = port;
}
/**
 * @brief Setter for the pin attribute
 * @param pin The new pin
 */
void ControlClutch::setPin(uint16_t pin)
{
	m_pin = pin;
}
/**
 * @brief Setter for the state of the clutch
 * @param state The new state of the clutch, true for locked and false for unlocked
 */
void ControlClutch::setIsLocked(bool state)
{
	m_isLocked = state;
}
/**
 * @brief Getter for the port attribute
 * @return A pointer to the port
 */
GPIO_TypeDef* ControlClutch::getPort(void)
{
	return m_port;
}
/**
 * @brief Getter for the pin attribute
 * @return The pin in uint16_t
 */
uint16_t ControlClutch::getPin(void) const
{
	return m_pin;
}
/**
 * @brief Getter for the state of the clutch
 * @return The current state of the clutch, true for locked and false for unlocked
 */
bool ControlClutch::isLocked(void) const
{
	return m_isLocked;
}
/**
 * @brief Function to toggle the state of the pin saved in the attribute
 * @param state The state to change the pin to.
 */
void ControlClutch::togglePin(bool state)
{
	if(state)
		syst->GPIO()->setPin(m_port, m_pin);
	else
		syst->GPIO()->resetPin(m_port, m_pin);
}
/**
 * @brief Function to release the motor (and set speed to 0)
 */
void ControlClutch::release(void)
{
	togglePin(0);
	m_isLocked = false;
}
/**
 * @brief Lock the clutch to allow to motor to turn the wheels
 */
void ControlClutch::lock(void)
{
	togglePin(1);
	m_isLocked = true;
}
