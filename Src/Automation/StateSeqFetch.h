/**
* @file StateSeqFetch.h
* @brief Header file for the StateSeqFetch
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 28th, 2018
*
* The StateSeqFetch enum allows a more user-friendly selection for a unit state in a sequence
*
*/
#ifndef STATESEQFETCH_H_
#define STATESEQFETCH_H_
/**
 * @enum StateSeqFetch
 * @brief Allows a more user-friendly selection for a unit state in a sequence
 */
typedef enum StateSeqFetch {
	InitPos = 0,
	MoveElevation,
	ExtendMat,
	GetItem,
	RetractMat,
	ReturnToInitPos
} stateSeqFetch_t;

#endif /* STATESEQFETCH_H_ */
