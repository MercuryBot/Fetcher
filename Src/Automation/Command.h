/**
* @file Command.h
* @brief Header file for the Command
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 28th, 2018
*
* The StateSeqFetch enum allows a more user-friendly selection for a unit command
*
*/#ifndef COMMAND_H_
#define COMMAND_H_
/**
 * @enum Command
 * @brief Allows a more user-friendly selection for a unit command
 */
typedef enum Command {
	Wait = 0,
	Continue,
	TurnLeft,
	TurnRight,
	Move,
	FetchObject,
	Unload,
	Recharge,
	EmergencyStop
} command_t;

#endif /* COMMAND_H_ */
