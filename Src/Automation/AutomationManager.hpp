/**
 * @file AutomationManager.hpp
 * @brief Header file for the AutomationManager class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The AutomationManager class is the main class that controls everything else
 *
 */
#ifndef AUTOMATIONMANAGER_HPP_
#define AUTOMATIONMANAGER_HPP_

#include "Mode.h"
#include "Command.h"
#include "StateSeqFetch.h"
#include "Instruction.h"

#include "MovementManager.hpp"
#include "BatteryMonitoring.hpp"
#include "RgbScanManager.hpp"
#include "SensorIR/ModelIR.hpp"
#include "SensorLaser/ModelLaser.hpp"

typedef void task;
/**
 * @class AutomationManager
 * @brief The AutomationManager class is the main class that controls everything else
 */
class AutomationManager
{
	/**
	 * @brief Movement manager member
	 */
	MovementManager m_movement;
	/**
	 * @brief Bettery monitoring member
	 */
	BatteryMonitoring m_battery;
	/**
	 * @brief Code scanner member
	 */
	RgbScanManager m_codeScanner;
	/**
	 * @brief Left IR sensor member
	 */
	ModelIR m_lineLeft;
	/**
	 * @brief Right IR sensor member
	 */
	ModelIR m_lineRight;
	/**
	 * @brief Laser sensor member
	 */
	ModelLaser m_laserSensor;
	/**
	 * @brief List of instruction received from the server
	 */
	LinkedList<Instruction> m_instructionList;
	/**
	 * @brief The mode of the operator
	 */
	op_mode_t m_op_mode;
	/**
	 * @brief The current instruction
	 */
	Instruction m_instruction;
	/**
	 * @brief The current state in the fetch sequence
	 */
	stateSeqFetch_t m_stateSeqFetch;
	/**
	 * @brief Used for the debouncing of the laser
	 */
	int m_debouncing;
	/**
	 * @brief True if the robot is going to next state or false if idle
	 */
	bool m_going;
	/**
	 * @brief True when a new code needs to be scan
	 */
	bool m_newPosition;
	/**
	 * @brief Contains the sequence to follow the line
	 */
	void sequenceFollowLine();
	/**
	 * @brief Contains the sequence to fetch an object
	 * @param level The level of the object, true for high level and false for low level
	 */
	void sequenceFetchObject(uint8_t level);

public:
	/**
	 * @brief The class' base constructor
	 */
	AutomationManager();
	/**
	 * @brief The class' base destructor
	 */
	~AutomationManager();
	/**
	 * @brief The class main task
	 * @return Nothing, not implemented yet
	 */
	task mainTask(void);
	/**
	 * @brief The movement manager member
	 */
	MovementManager* movement(void);
	/**
	 * @brief The battery monitoring member
	 */
	BatteryMonitoring* battery(void);
	/**
	 * @brief The RGB scan manager member
	 */
	RgbScanManager* codeScanner(void);
	/**
	 * @brief The getter for the new position attribute
	 * @return The value in boolean
	 */
	bool isNewPosition(void) const;
	/**
	 * @brief The getter for the operator Mode
	 * @return The current value
	 */
	op_mode_t getOpMode(void) const;
	/**
	 * @brief The getter for the instruction
	 * @return The current value
	 */
	Instruction getInstruction(void) const;
	/**
	 * @brief The getter for the state in the fetch sequence
	 * @return The current value
	 */
	stateSeqFetch_t getStateSeqFetch(void) const;
	/**
	 * @brief The setter for the operator mode
	 * @param mode The new value
	 */
	void setOpMode(op_mode_t mode);
	/**
	 * @brief The setter for the instruction
	 * @param instruct The new value
	 */
	void setInstruction(Instruction instruct);
	/**
	 * @brief The setter for the state in the fetch sequence
	 * @param stateSeqFetch The new value
	 */
	void setStateSeqFetch(stateSeqFetch_t stateSeqFetch);
	/**
	 * @brief The setter for the isNewPosition
	 * @param state The new value
	 */
	void setIsNewPosition(bool state);
	/**
	 * @brief Sets manually all the motors speeds
	 * @param motor_1 The speed of the first motor
	 * @param motor_2 The speed of the second motor
	 * @param motor_3 The speed of the third motor
	 * @param motor_4 The speed of the fourth motor
	 */
	void setMotorSpeeds(int8_t motor_1, int8_t motor_2, int8_t motor_3, int8_t motor_4);
	/**
	 * @brief Adds an instruction to the list
	 * @param instruct The instruction to add
	 */
	void addInstruction(Instruction instruct);
	/**
	 * @brief Removes the instruction at desired position
	 * @param code The code to witch the instructions must be removed
	 */
	void removeInstruction(PosCode code);
};

#endif /* AUTOMATIONMANAGER_HPP_ */
