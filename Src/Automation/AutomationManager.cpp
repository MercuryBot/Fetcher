/**
 * @file AutomationManager.cpp
 * @brief Definition file for the AutomationManager class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The AutomationManager class is the main class that controls everything else
 *
 */
#include "AutomationManager.hpp"
/**
 * @brief The class' base constructor
 */
AutomationManager::AutomationManager()
{
	m_movement = MovementManager();
	m_codeScanner = RgbScanManager();
	m_battery = BatteryMonitoring(syst->GPIO()->getADCHandle_ptr(0), 15, 11);

	m_lineLeft = ModelIR(IR_INTERRUPT1_GPIO_Port, IR_INTERRUPT1_Pin, 3);
	m_lineRight = ModelIR(IR_INTERRUPT2_GPIO_Port, IR_INTERRUPT2_Pin, 2);
	m_laserSensor = ModelLaser(LASER_INTERRUPT_GPIO_Port, LASER_INTERRUPT_Pin,0);

	m_instructionList = LinkedList<Instruction> ();

	m_going = false;
	m_newPosition = false;
	m_op_mode = Auto;
	m_instruction = {Continue, {{'X', 'X', 'X'}}, 0};
	m_stateSeqFetch = InitPos;
	m_debouncing = 0;
}
/**
 * @brief The class' base destructor
 */
AutomationManager::~AutomationManager()
{

}
/**
 * @brief Contains the sequence to follow the line
 */
void AutomationManager::sequenceFollowLine()
{
	if(m_lineLeft.update() || m_lineRight.update())
	{
		if (!m_lineLeft.getState() && !m_lineRight.getState())
			m_movement.engageFreeRun();
		else if (!m_lineLeft.getState())
			m_movement.steerLeft((uint16_t)100, 50);
		else if (!m_lineRight.getState())
			m_movement.steerRight((uint16_t)100, 50);
		else if (m_lineLeft.getState() && m_lineRight.getState())
			m_movement.goForward((uint16_t)100);
		else
			m_movement.engageFreeRun();
	}
}
/**
 * @brief Contains the sequence to fetch an object
 * @param level The level of the object, true for high level and false for low level
 */
void AutomationManager::sequenceFetchObject(uint8_t level) {

	//TODO: Eventually, with better mechanical capacities, implement more fetching levels in the sequence
	switch(m_stateSeqFetch) {
	case InitPos :
		m_stateSeqFetch = (level) ? MoveElevation : ExtendMat;
		break;
	case MoveElevation :
		if(m_movement.getLiftState() == Deployed)
			m_stateSeqFetch = ExtendMat;
		else if (m_movement.getLiftState() != Deploying)
			m_movement.setTargetLiftState(Deployed);
		break;
	case ExtendMat :
		if(m_movement.getMatState() == Deployed)
			m_stateSeqFetch = GetItem;
		else if (m_movement.getMatState() != Deploying)
			m_movement.setTargetMatState(Deployed);
		break;
	case GetItem :
		if((m_debouncing == 0) && (m_laserSensor.getState())) {
			m_movement.spinMat(100);
			m_debouncing = 1;
		}
		else if((m_debouncing == 1) && (!m_laserSensor.getState()))
		{
			m_debouncing = 2;
		}
		else if((m_debouncing == 2) && (m_laserSensor.getState()))
		{
			m_debouncing = 0;
			m_movement.stopSpinMat();
			m_stateSeqFetch = RetractMat;
		}
		break;
	case RetractMat :
		if(m_movement.getMatState() == Retracted)
			m_stateSeqFetch = ReturnToInitPos;
		else if (m_movement.getMatState() != Retracting)
			m_movement.setTargetMatState(Retracted);
		break;
	case ReturnToInitPos :
		if(m_movement.getLiftState() == Retracted || level == 0)
		{
			m_instruction.command = Continue;
			m_stateSeqFetch = InitPos;
		}
		else if (m_movement.getLiftState() != Retracting)
			m_movement.setTargetLiftState(Retracted);
		break;
	default :
		m_stateSeqFetch = InitPos;
		break;
	}
}
/**
 * @brief The getter for the new position attribute
 * @return The value in boolean
 */
bool AutomationManager::isNewPosition(void) const
{
	return m_newPosition;
}
/**
 * @brief The getter for the operator Mode
 * @return The current value
 */
op_mode_t AutomationManager::getOpMode(void) const
{
	return m_op_mode;
}
/**
 * @brief The getter for the instruction
 * @return The current value
 */
Instruction AutomationManager::getInstruction(void) const
{
	return m_instruction;
}
/**
 * @brief The getter for the state in the fetch sequence
 * @return The current value
 */
stateSeqFetch_t AutomationManager::getStateSeqFetch(void) const
{
	return m_stateSeqFetch;
}
/**
 * @brief The setter for the operator mode
 * @param mode The new value
 */
void AutomationManager::setOpMode(op_mode_t mode)
{
	m_op_mode = ud_std::clamp(mode, Manual, Auto);
}
/**
 * @brief The setter for the instruction
 * @param instruct The new value
 */
void AutomationManager::setInstruction(Instruction instruct)
{
	m_instruction = instruct;
}
/**
 * @brief The setter for the state in the fetch sequence
 * @param stateSeqFetch The new value
 */
void AutomationManager::setStateSeqFetch(stateSeqFetch_t stateSeqFetch)
{
	m_stateSeqFetch = ud_std::clamp(stateSeqFetch, InitPos, ReturnToInitPos);
}
/**
 * @brief The setter for the isNewPosition
 * @param state The new value
 */
void AutomationManager::setIsNewPosition(bool state)
{
	m_newPosition = state;
}
/**
 * @brief The movement manager member
 */
MovementManager* AutomationManager::movement(void)
{
	return &m_movement;
}
/**
 * @brief The battery monitoring member
 */
BatteryMonitoring* AutomationManager::battery(void)
{
	return &m_battery;
}
/**
 * @brief The RGB scan manager member
 */
RgbScanManager* AutomationManager::codeScanner(void)
{
	return &m_codeScanner;
}
/**
 * @brief The class main task
 * @return Nothing, not implemented yet
 */
task AutomationManager::mainTask(void)
{
	m_movement.update();
	m_laserSensor.update();

	if(m_op_mode == Auto)
	{
		if(m_codeScanner.update())
		{
			m_newPosition = true;

			for(uint8_t i = 0; i < m_instructionList.size(); ++i)
			{
				if(memcmp(m_codeScanner.getCurrentPosCode().code, m_instructionList.at(i).posCode.code, sizeof(m_codeScanner.getCurrentPosCode().code)) == 0)
				{
					m_instruction = m_instructionList.at(i);
					m_instructionList.dropAt(i);
					break;
				}
			}
		}
	}

	if(m_op_mode == Auto || m_op_mode == SemiAuto)
	{
		switch(m_instruction.command)
		{
		case Wait:
			m_movement.engageFreeRun();
			break;
		case Continue:
			sequenceFollowLine();
			break;
		case FetchObject:
			sequenceFetchObject(m_instruction.commandSpec);
			break;
		default:
			sequenceFollowLine();
			break;
		}
	}
	else if (m_op_mode == Manual)
	{

	}
}
/**
 * @brief Sets manually all the motors speeds
 * @param motor_1 The speed of the first motor
 * @param motor_2 The speed of the second motor
 * @param motor_3 The speed of the third motor
 * @param motor_4 The speed of the fourth motor
 */
void AutomationManager::setMotorSpeeds(int8_t motor_1, int8_t motor_2, int8_t motor_3, int8_t motor_4)
{
	m_movement.ctrlMotorLeft(motor_1);
	m_movement.ctrlMotorRight(motor_2);
	m_movement.ctrlMotorLift(motor_3);
	m_movement.ctrlMotorSlide(motor_4);
}
/**
 * @brief Adds an instruction to the list
 * @param instruct The instruction to add
 */
void AutomationManager::addInstruction(Instruction instruct)
{
	for(uint8_t i = 0; i < m_instructionList.size(); ++i)
	{
		if(memcmp(instruct.posCode.code, m_instructionList.at(i).posCode.code, sizeof(instruct.posCode.code)) == 0)
		{
			m_instructionList.at(i) = instruct;
			return;
		}
	}

	m_instructionList.append(instruct);
}
/**
 * @brief Removes the instruction at desired position
 * @param code The code to witch the instructions must be removed
 */
void AutomationManager::removeInstruction(PosCode code)
{
	for(uint8_t i = 0; i < m_instructionList.size(); ++i)
	{
		if(memcmp(code.code, m_instructionList.at(i).posCode.code, sizeof(code)) == 0)
			m_instructionList.dropAt(i);

	}
}
