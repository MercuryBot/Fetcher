/**
* @file Instruction.h
* @brief Header file for the Instruction
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 28th, 2018
*
* The Instruction enum allows a more user-friendly use for instructions
*
*/
#ifndef INSTRUCTION_H_
#define INSTRUCTION_H_

#include "PosCode.h"
#include "Command.h"
/**
 * @enum Instruction
 * @brief Allows a more user-friendly use for instructions
 */
struct Instruction {
	command_t command;
	PosCode posCode;
	uint8_t commandSpec;
};

#endif /* INSTRUCTION_H_ */
