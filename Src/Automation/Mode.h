/**
* @file Mode.h
* @brief Header file for the Mode
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 28th, 2018
*
* The Mode enum allows a more user-friendly selection for a unit mode
*
*/#ifndef MODE_H_
#define MODE_H_
/**
 * @enum Mode
 * @brief Allows a more user-friendly selection for a unit mode
 */
typedef enum Mode {
	Manual = 0,
	SemiAuto,
	Auto
} op_mode_t;

#endif /* MODE_H_ */
