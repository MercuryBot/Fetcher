/**
* @file Side
* @brief Header file for the Side enum
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 28th, 2018
*
* The Side enum allows a more user-friendly selection for a unit's side
*
*/

#ifndef SIDE_H_
#define SIDE_H_

/**
 * @enum Side
 * @brief Allows a more user-friendly selection for a unit's side
 */
typedef enum Side {
  Left = 0,//!< Left
  Right    //!< Right
} side_t;

#endif /* SIDE_H_ */
