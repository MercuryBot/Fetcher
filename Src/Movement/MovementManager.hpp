/**
* @file MovementManager.hpp
* @brief Header file for the MovementManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 28th, 2018
*
* The MovementManager class provides easier control over the robot's movements
*
*/

#ifndef MOVEMENTMANAGER_HPP_
#define MOVEMENTMANAGER_HPP_

/**
 * @def PC_RATE_LEFT
 * @brief Proportional rate control of the left wheel
 */
#define PC_RATE_LEFT 5

/**
 * @def PC_RATE_RIGHT
 * @brief Proportional rate control of the right wheel
 */
#define PC_RATE_RIGHT 5

/**
 * @def MAX_STEER_RATIO
 * @brief Maximum steering ratio when steering
 */
#define MAX_STEER_RATIO 100

/**
 * @def MOTORS_QTY
 * @brief Quantity of controlled motors
 */
#define MOTORS_QTY 5

/**
 * @def LIMIT_SWITCH_QTY
 * @brief Quantity of controlled limit switches
 */
#define LIMIT_SWITCH_QTY 4

#include "ControlSystem.hpp"
#include "ControlClutch.hpp"
#include "SensorEncoder/ModelEncoder.hpp"
#include "SensorLimitSwitch/ModelLimitSwitch.hpp"
#include "ControlLight.hpp"
#include "ControlHeavy.hpp"
#include "Side.h"
#include "MechanismState.h"

/**
 * @class MovementManager
 * @brief Provides easier control over the robot's movements
 */
class MovementManager {

private:

	/**
	 * @brief Controller of the lift's motor
	 */
	ControlHeavy m_liftMotor;

	/**
	 * @brief Controller of mat's motor
	 */
	ControlLight m_matMotor;

	/**
	 * @brief Controller of the slide's motor
	 */
	ControlLight m_slideMotor;

	/**
	 * @brief Controller of the left wheel's motor
	 */
	ControlLight m_leftMotor;

	/**
	 * @brief Controller of the right wheel's motor
	 */
	ControlLight m_rightMotor;

	/**
	 * @brief Controller of the left clutch
	 */
	ControlClutch m_leftClutch;

	/**
	 * @brief Controller of the right clutch
	 */
	ControlClutch m_rightClutch;

	/**
	 * @brief Model of the left encoder
	 */
	ModelEncoder m_leftEncoder;

	/**
	 * @brief Model of the right encoder
	 */
	ModelEncoder m_rightEncoder;

	/**
	 * @brief Model of the low lift's limit switch
	 */
	ModelLimitSwitch m_liftLowLs;

	/**
	 * @brief Model of the high lift's limit switch
	 */
	ModelLimitSwitch m_liftHighLs;

	/**
	 * @brief Model of the retracted mat's limit switch
	 */
	ModelLimitSwitch m_matInLs;

	/**
	 * @brief Model of the deployed mat's limit switch
	 */
	ModelLimitSwitch m_matOutLs;

	/**
	 * @brief Current known state of the lift
	 */
	mech_state m_liftState;

	/**
	 * @brief Targeted state of the lift
	 */
	mech_state m_targetLiftState;

	/**
	 * @brief Current known state of the mat
	 */
	mech_state m_matState;


	/**
	 * @brief Target state of the mat
	 */
	mech_state m_targetMatState;

	/**
	 * @brief Index of the clutche's pwm
	 */
	uint8_t m_pwmClutchesIndex;

	/**
	 * @brief Proportional control ratio for the left motor
	 */
	float m_kpLeftMotor;

	/**
	 * @brief Proportional control ratio for the right motor
	 */
	float m_kpRightMotor;

	/**
	 * @brief Encoder's step speed to target for the right wheel
	 */
	uint64_t m_targetEncoderStepUS_r;

	/**
	 * @brief Encoder's step speed to target for the left wheel
	 */
	uint64_t m_targetEncoderStepUS_l;

	/**
	 * @brief Currently set steering ratio
	 */
	int16_t m_turnRatio;

	/**
	 * @brief Confirms if the wheel's proportional control is enabled
	 */
	bool m_isPCEnabled;

	/**
	 * @brief Confirms if the proportional control is pwm based (rather than speed based)
	 */
	bool m_pwmBasedPC;

	/**
	 * @brief Confirms if more complex movement managing is enabled
	 */
	bool m_isMovementManagingEnabled;

public:

	/**
	 * @brief The class' base constructor
	 */
	MovementManager();

	/**
	 * @brief The class base destructor
	 */
	~MovementManager();

	/**
	 * @brief Setter for the lift's motor
	 * @param motor New motor to set
	 */
	void setLiftMotor(ControlHeavy motor);

	/**
	 * @brief Setter for the mat's motor
	 * @param motor New motor to set
	 */
	void setMatMotor(ControlLight motor);

	/**
	 * @brief Setter for the slide's motor
	 * @param motor New motor to set
	 */
	void setSlideMotor(ControlLight motor);

	/**
	 * @brief Setter for the left wheel's motor
	 * @param motor New motor to set
	 */
	void setLeftMotor(ControlLight motor);

	/**
	 * @brief Setter for the right wheel's motor
	 * @param motor New motor to set
	 */
	void setRightMotor(ControlLight motor);

	/**
	 * @brief Setter for the left clutch controller
	 * @param clutch New clutch to set
	 */
	void setLeftClutch(ControlClutch clutch);

	/**
	 * @brief Setter for the right clutch controller
	 * @param clutch New clutch to set
	 */
	void setRightClutch(ControlClutch clutch);

	/**
	 * @brief Setter for left encoder model
	 * @param encoder New clutch to set
	 */
	void setLeftEncoder(ModelEncoder encoder);

	/**
	 * @brief Setter for right encoder model
	 * @param encoder New clutch to set
	 */
	void setRightEncoder(ModelEncoder encoder);

	/**
	 * @brief Setter for the lift low limit switch model
	 * @param ls New model to set
	 */
	void setLiftLowLs(ModelLimitSwitch ls);

	/**
	 * @brief Setter for the lift high limit switch model
	 * @param ls New model to set
	 */
	void setLiftHighLs(ModelLimitSwitch ls);

	/**
	 * @brief Setter for the retracted mat limit switch model
	 * @param ls New model to set
	 */
	void setMatInLs(ModelLimitSwitch ls);

	/**
	 * @brief Setter for the deployed mat limit switch model
	 * @param ls New model to set
	 */
	void setMatOutLs(ModelLimitSwitch ls);

	/**
	 * @brief Setter for the lift's current state
	 * @param state Mechanical state to set
	 */
	void setLiftState(mech_state state);

	/**
	 * @brief Setter for the targeted lift's state
	 * @param state Mechanical state to set
	 */
	void setTargetLiftState(mech_state state);

	/**
	 * @brief Setter for the mat's current state
	 * @param state Mechanical state to set
	 */
	void setMatState(mech_state state);

	/**
	 * @brief Setter for the targeted mat's state
	 * @param state Mechanical state to set
	 */
	void setTargetMatState(mech_state state);

	/**
	 * @brief Setter for a proportional ratio of a wheel's proportional control
	 * @param side Side to set
	 * @param kp New value to set to
	 */
	void setKp(side_t side, float kp);

	/**
	 * @brief Setter for the targeted speed of a wheel in encoder steps, in US
	 * @param stepUS Target step speed
	 * @param side Side to set
	 */
	void setTargetEncoderStepUS(uint64_t stepUS, side_t side);

	/**
	 * @brief Setter for the targeted speed of a wheel in encoder steps, in MS
	 * @param stepMS Target step speed
	 * @param side Side to set
	 */
	void setTargetEncoderStepMS(uint64_t stepMS, side_t side);

	/**
	 * @brief Setter for the steering ratio
	 * @param ratio New value of the ratio
	 */
	void setTurnRatio(int16_t ratio);

	/**
	 * @brief Setter for enabling proportional control in routine
	 * @param enabled Value to set
	 */
	void setIsPCEnabled(bool enabled);

	/**
	 * @brief Setter for enabling the movement managing
	 * @param enabled Value to set
	 */
	void setIsMovementManagingEnabled(bool enabled);

	/**
	 * @brief Setter for enabling PWM based proportional control
	 * @param value Value to set
	 */
	void setpwmBasedPC(bool value);

	/**
	 * @brief Setter for the PWM index of the clutches
	 * @param index Value to set the index to
	 */
	void setPwmClutchesIndex(uint8_t index);

	/**
	 * @brief Getter for the lift motor's controller
	 * @return Pointer to the lift motor's controller
	 */
	ControlMotor* liftMotor(void);

	/**
	 * @brief Getter for the mat motor's controller
	 * @return Pointer to the mat motor's controller
	 */
	ControlMotor* matMotor(void);

	/**
	 * @brief Getter for the rod motor's controller
	 * @return Pointer to the rod motor's controller
	 */
	ControlMotor* slideMotor(void);

	/**
	 * @brief Getter for the left wheel's motor's controller
	 * @return Pointer to the left wheel's motor's controller
	 */
	ControlMotor* leftMotor(void);

	/**
	 * @brief Getter for the right wheel's motor's controller
	 * @return Pointer to the right wheel's motor's controller
	 */
	ControlMotor* rightMotor(void);

	/**
	 * @brief Getter for the left clutche's controller
	 * @return Pointer to the left clutche's controller
	 */
	ControlClutch* leftClutch(void);

	/**
	 * @brief Getter for the right clutche's controller
	 * @return Pointer to the right clutche's controller
	 */
	ControlClutch* rightClutch(void);

	/**
	 * @brief Getter for the left encoder's model
	 * @return Pointer to the left encoder's model
	 */
	ModelEncoder* leftEncoder(void);

	/**
	 * @brief Getter for the right encoder's model
	 * @return Pointer to the right encoder's model
	 */
	ModelEncoder* rightEncoder(void);

	/**
	 * @brief Getter for the low limit switch model
	 * @return Pointer to the low limit switch model
	 */
	ModelLimitSwitch* liftLowLs(void);

	/**
	 * @brief Getter for the high limit switch model
	 * @return Pointer to the high limit switch model
	 */
	ModelLimitSwitch* liftHighLs(void);

	/**
	 * @brief Getter for the mat's retracted limit switch model
	 * @return Pointer to the mat's retracted limit switch model
	 */
	ModelLimitSwitch* matInLs(void);

	/**
	 * @brief Getter for the mat's deployed limit switch model
	 * @return Pointer to the mat's deployed limit switch model
	 */
	ModelLimitSwitch* matOutLs(void);

	/**
	 * @brief Getter for the target lift state
	 * @return Mechanical state
	 */
	mech_state getTargetLiftState(void) const;

	/**
	 * @brief Getter for the current lift state
	 * @return Mechanical state
	 */
	mech_state getLiftState(void) const;

	/**
	 * @brief Getter for the target mat state
	 * @return Mechanical state
	 */
	mech_state getTargetMatState(void) const;

	/**
	 * @brief Getter for the current mat state
	 * @return Mechanical state
	 */
	mech_state getMatState(void) const;

	/**
	 * @brief Getter for the set proportional control coefficent of a wheel
	 * @param side Side to get the coefficient from
	 * @return Currently set coefficient
	 */
	float getKp(side_t side) const;

	/**
	 * @brief Getter for the target encoder step speed, in US
	 * @param side Side to get the target step speed from
	 * @return Target step speed
	 */
	uint64_t getTargetEncoderStepUS(side_t side) const;

	/**
	 * @brief Getter for the target encoder step speed, in MS
	 * @param side Side to get the target step speed from
	 * @return Target step speed
	 */
	uint64_t getTargetEncoderStepMS(side_t side) const;

	/**
	 * @brief Getter for the currently set turning ratio
	 * @return Turning ratio
	 */
	int16_t getTurnRatio(void) const;

	/**
	 * @brief Getter for the enabling of the proportional control
	 * @return False if it's disabled, True if it's enabled
	 */
	bool isPCEnabled(void) const;

	/**
	 * @brief Getter for the enabling of the movement managing
	 * @return False if it's disabled, True if it's enabled
	 */
	bool isMovementManagingEnabled(void) const;

	/**
	 * @brief Getter for the enabling of PWM based proportional
	 * @return False if it's disabled, True if it's enabled
	 */
	bool isPwmBasedPC(void) const;

	/**
	 * @brief Getter for currently set index of the PWM of the clutches
	 * @return Index of the PWM
	 */
	uint8_t getPwmClutchesIndex(void) const;

	/**
	 * @brief Confirms if the lift is currently moving
	 * @return False if it's not, True if it is
	 */
	bool isLiftMoving(void) const;

	/**
	 * @brief Confirms if the mat is currently moving
	 * @return False if it's not, True if it is
	 */
	bool isMatMoving(void) const;

	/**
	 * @brief Confirms if the rod is currently moving
	 * @return False if it's not, True if it is
	 */
	bool isSlideMoving(void) const;

	/**
	 * @brief Confirms if the wheels are currently moving
	 * @return False if they aren't, True if they are
	 */
	bool areWheelsMoving(void) const;

	/**
	 * @brief Confirms if the clutch is locked
	 * @param side Side to check the clutch from
	 * @return False if it's not, True if it is
	 */
	bool isClutchLocked(side_t side) const;

	/**
	 * @brief Update the sensor models
	 * @return False if there hasn't been any change, True if there's been
	 */
	bool updateModels(void);

	/**
	 * @brief Update the currently set state of the mechanical parts
	 */
	void updateState(void);

	/**
	 * @brief Update outputs of the wheels according to the proportional control
	 */
	void updatePC(void);

	/**
	 * @brief Update digital outputs
	 */
	void updateOutputs(void);

	/**
	 * @brief Calls every update routine
	 */
	void update(void);

	/**
	 * @brief Set the outputs to make the unit go forward
	 * @param speed Target encoder speed of the wheels
	 */
	void goForward(uint64_t speed);

	/**
	 * @brief Set the outputs to make the unit go forward
	 * @param percent PWM percentage to set the master wheel to
	 */
	void goForward(uint16_t percent);

	/**
	 * @brief Set the outputs to make the unit go backwards
	 * @param speed Target encoder speed of the wheels
	 */
	void goBackwards(uint64_t speed);

	/**
	 * @brief Set the outputs to make the unit go backwards
	 * @param percent PWM percentage to set the master wheel to
	 */
	void goBackwards(uint16_t percent);

	/**
	 * @brief Set the outputs to make the unit turn right
	 * @param speed Target encoder speed of the master wheel
	 * @param ratio Turning ratio to turn to
	 */
	void steerRight(uint64_t speed, uint8_t ratio);

	/**
	 * @brief Set the outputs to make the unit turn right
	 * @param percent PWM percentage to set the master wheel to
	 * @param ratio Turning ratio to turn to
	 */
	void steerRight(uint16_t percent, uint8_t ratio);

	/**
	 * @brief Set the outputs to make the unit turn left
	 * @param speed Target encoder speed of the master wheel
	 * @param ratio Turning ratio to turn to
	 */
	void steerLeft(uint64_t speed, uint8_t ratio);

	/**
	 * @brief Set the outputs to make the unit turn left
	 * @param percent PWM percentage to set the master wheel to
	 * @param ratio Turning ratio to turn to
	 */
	void steerLeft(uint16_t percent, uint8_t ratio);

	/**
	 * @brief Set the outputs to fit a breaking configuration
	 */
	void engageBreaking(void);

	/**
	 * @brief Set the outputs to disengage the breaking configuration
	 */
	void disengageBreaking(void);

	/**
	 * @brief Set the outputs to fit a free run configuration
	 */
	void engageFreeRun(void);

	/**
	 * @brief Set the outputs to disengage the free run configuration
	 */
	void disengageFreeRun(void);

	/**
	 * @brief Sets the spinning speed of the mat
	 * @param speed Speed to set the mat's motor to
	 */
	void spinMat(uint64_t speed);

	/**
	 * @brief Set the outputs to deplay the mat
	 */
	void deployMat(void);

	/**
	 * @brief Set the outputs to retract the mat
	 */
	void retractMat(void);

	/**
	 * @brief Set the outputs to immobilize the mat's rod
	 */
	void stopSlideMat(void);

	/**
	 * @brief Set the outputs to immobilize the mat's spinning
	 */
	void stopSpinMat(void);

	/**
	 * @brief Set the outputs to deploy the lift
	 */
	void deployLift(void);

	/**
	 * @brief Set the outputs to retract the lift
	 */
	void retractLift(void);

	/**
	 * @brief Set the outputs to immobilize the lift
	 */
	void stopLift(void);

	/**
	 * @brief Enables the wheels proportional control
	 */
	void enablePC(void);

	/**
	 * @brief Disables the wheels proportional control
	 */
	void disablePC(void);

	/**
	 * @brief Locks the clutch of  a certain side
	 * @param side Side to lock clutch to
	 */
	void lockClutch(side_t side);

	/**
	 * @brief Releases the clutch of  a certain side
	 * @param side Side to lock clutch to
	 */
	void releaseClutch(side_t side);

	/**
	 * @brief Set the PWM duty cycle of the clutches
	 * @param percent Value of to set the PWMs to
	 */
	void setPwmClutches(uint8_t percent);

	/**
	 * @brief Manualy set the speed of the lift's motor
	 * @param speed Speed to set
	 */
	void ctrlMotorLift(int16_t speed);

	/**
	 * @brief Manualy set the speed of the left wheel's motor
	 * @param speed Speed to set
	 */
	void ctrlMotorLeft(int16_t speed);

	/**
	 * @brief Manualy set the speed of the right wheel's motor
	 * @param speed Speed to set
	 */
	void ctrlMotorRight(int16_t speed);

	/**
	 * @brief Manualy set the speed of the mat motor
	 * @param speed Speed to set
	 */
	void ctrlMotorMat(int16_t speed);

	/**
	 * @brief Manualy set the speed of the rod'S motor
	 * @param speed Speed to set
	 */
	void ctrlMotorSlide(int16_t speed);
};


#endif /* MOVEMENTMANAGER_HPP_ */
