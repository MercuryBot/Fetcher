/**
* @file MovementManager.cpp
* @brief Definition file for the MovementManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 28th, 2018
*
* The MovementManager class provides easier control over the robot's movements
*
*/

#include "MovementManager.hpp"

MovementManager::MovementManager()
{
	m_leftEncoder = ModelEncoder(ENCODEUR1_GPIO_Port, ENCODEUR1_Pin, ENC1_INTERRUPT_IDX, &htim6);
	m_rightEncoder = ModelEncoder(ENCODEUR2_GPIO_Port, ENCODEUR2_Pin, ENC2_INTERRUPT_IDX, &htim7);

	m_matInLs = ModelLimitSwitch(LIMIT_SWITCH4_GPIO_Port, LIMIT_SWITCH4_Pin);
	m_liftHighLs = ModelLimitSwitch(LIMIT_SWITCH3_GPIO_Port, LIMIT_SWITCH3_Pin);
	m_liftLowLs = ModelLimitSwitch(LIMIT_SWITCH2_GPIO_Port, LIMIT_SWITCH2_Pin);
	m_matOutLs = ModelLimitSwitch(LIMIT_SWITCH1_GPIO_Port, LIMIT_SWITCH1_Pin);

	m_liftState = Idle;
	m_targetLiftState = Idle;
	m_matState = Idle;
	m_targetMatState = Idle;

	m_kpLeftMotor = PC_RATE_LEFT;
	m_kpRightMotor = PC_RATE_RIGHT;

	m_pwmClutchesIndex = 1;

	m_targetEncoderStepUS_r = 0;
	m_targetEncoderStepUS_l = 0;

	//Both automated controls are updated by default
	m_turnRatio = MAX_STEER_RATIO;
	m_isPCEnabled = true;
	m_isMovementManagingEnabled = true;
	m_pwmBasedPC = false;

	m_liftMotor = ControlHeavy(U3_3A_GPIO_Port, U3_3A_Pin, 3);
	m_matMotor = ControlLight(U2_3A_GPIO_Port, U2_3A_Pin, U2_4A_GPIO_Port, U2_4A_Pin, 4);
	m_rightMotor = ControlLight(U2_1A_GPIO_Port, U2_1A_Pin, U2_2A_GPIO_Port, U2_2A_Pin, 2);
	m_leftMotor = ControlLight(U1_1A_GPIO_Port, U1_1A_Pin, U1_2A_GPIO_Port, U1_2A_Pin, 0);
	m_slideMotor = ControlLight(U1_3A_GPIO_Port, U1_3A_Pin, U1_4A_GPIO_Port, U1_4A_Pin, 5);
	m_leftClutch = ControlClutch(U3_1A_GPIO_Port, U3_1A_Pin);
	m_rightClutch = ControlClutch(U3_2A_GPIO_Port, U3_2A_Pin);
}

MovementManager::~MovementManager()
{

}

void MovementManager::setLiftMotor(ControlHeavy motor)
{
	m_liftMotor = motor;
}

void MovementManager::setMatMotor(ControlLight motor)
{
	m_matMotor = motor;
}

void MovementManager::setSlideMotor(ControlLight motor)
{
	m_slideMotor = motor;
}

void MovementManager::setLeftMotor(ControlLight motor)
{
	m_leftMotor = motor;
}

void MovementManager::setRightMotor(ControlLight motor)
{
	m_rightMotor = motor;
}

void MovementManager::setLeftClutch(ControlClutch clutch)
{
	m_leftClutch = clutch;
}

void MovementManager::setRightClutch(ControlClutch clutch)
{
	m_rightClutch = clutch;
}

void MovementManager::setLeftEncoder(ModelEncoder encoder)
{
	m_leftEncoder = encoder;
}

void MovementManager::setRightEncoder(ModelEncoder encoder)
{
	m_rightEncoder = encoder;
}

void MovementManager::setLiftLowLs(ModelLimitSwitch ls)
{
	m_liftLowLs = ls;
}

void MovementManager::setLiftHighLs(ModelLimitSwitch ls)
{
	m_liftHighLs = ls;
}

void MovementManager::setMatInLs(ModelLimitSwitch ls)
{
	m_matInLs = ls;
}

void MovementManager::setMatOutLs(ModelLimitSwitch ls)
{
	m_matOutLs = ls;
}

void MovementManager::setLiftState(mech_state state)
{
	m_liftState = state;
}

void MovementManager::setTargetLiftState(mech_state state)
{
	if(state == Retracted || state == Deployed || state == Idle) //Cannot set wanted state to something other than those two
		m_targetLiftState = state;
}

void MovementManager::setMatState(mech_state state)
{
	m_matState = state;
}

void MovementManager::setTargetMatState(mech_state state)
{
	if(state == Retracted || state == Deployed || state == Idle) //Cannot set wanted state to something other than those two
		m_targetMatState = state;
}

void MovementManager::setKp(side_t side, float kp)
{
	if(side == Left)
		m_kpLeftMotor = kp;
	else
		m_kpRightMotor = kp;
}

void MovementManager::setTargetEncoderStepUS(uint64_t stepUS, side_t side)
{
	if(side == Left)
		m_targetEncoderStepUS_l = stepUS;
	else
		m_targetEncoderStepUS_r = stepUS;
}

void MovementManager::setTargetEncoderStepMS(uint64_t stepMS, side_t side)
{
	if(side == Left)
		m_targetEncoderStepUS_l = stepMS * 1000;
	else
		m_targetEncoderStepUS_r = stepMS * 1000;
}

void MovementManager::setTurnRatio(int16_t ratio)
{
	m_turnRatio = ud_std::clamp(ratio, (0 - MAX_STEER_RATIO), MAX_STEER_RATIO);
}

void MovementManager::setIsPCEnabled(bool enabled)
{
	m_isPCEnabled = enabled;
}

void MovementManager::setIsMovementManagingEnabled(bool enabled)
{
	m_isMovementManagingEnabled = enabled;
}

void MovementManager::setpwmBasedPC(bool value)
{
	m_pwmBasedPC = value;
}

void MovementManager::setPwmClutchesIndex(uint8_t index)
{
	m_pwmClutchesIndex = index;
}

ControlMotor* MovementManager::liftMotor(void)
{
	return &m_liftMotor;
}

ControlMotor* MovementManager::matMotor(void)
{
	return &m_matMotor;
}

ControlMotor* MovementManager::slideMotor(void)
{
	return &m_slideMotor;
}

ControlMotor* MovementManager::leftMotor(void)
{
	return &m_leftMotor;
}

ControlMotor* MovementManager::rightMotor(void)
{
	return &m_rightMotor;
}

ControlClutch* MovementManager::leftClutch(void)
{
	return &m_leftClutch;
}

ControlClutch* MovementManager::rightClutch(void)
{
	return &m_rightClutch;
}

ModelEncoder* MovementManager::leftEncoder(void)
{
	return &m_leftEncoder;
}

ModelEncoder* MovementManager::rightEncoder(void)
{
	return &m_rightEncoder;
}

ModelLimitSwitch* MovementManager::liftLowLs(void)
{
	return &m_liftLowLs;
}

ModelLimitSwitch* MovementManager::liftHighLs(void)
{
	return &m_liftHighLs;
}

ModelLimitSwitch* MovementManager::matInLs(void)
{
	return &m_matInLs;
}

ModelLimitSwitch* MovementManager::matOutLs(void)
{
	return &m_matOutLs;
}

mech_state MovementManager::getTargetLiftState(void) const
{
	return m_targetLiftState;
}

mech_state MovementManager::getLiftState(void) const
{
	return m_liftState;
}

mech_state MovementManager::getTargetMatState(void) const
{
	return m_targetMatState;
}

mech_state MovementManager::getMatState(void) const
{
	return m_matState;
}

float MovementManager::getKp(side_t side) const
{
	return ((side == Left) ? (m_kpLeftMotor) : (m_kpRightMotor));
}

uint64_t MovementManager::getTargetEncoderStepUS(side_t side) const
{
	return ((side == Left) ? (m_targetEncoderStepUS_l) : (m_targetEncoderStepUS_r));
}

uint64_t MovementManager::getTargetEncoderStepMS(side_t side) const
{
	return ((side == Left) ? (m_targetEncoderStepUS_l / 1000) : (m_targetEncoderStepUS_r / 1000));
}

int16_t MovementManager::getTurnRatio(void) const
{
	return m_turnRatio;
}

bool MovementManager::isPCEnabled(void) const
{
	return m_isPCEnabled;
}

bool MovementManager::isMovementManagingEnabled(void) const
{
	return m_isMovementManagingEnabled;
}

bool MovementManager::isPwmBasedPC(void) const
{
	return m_pwmBasedPC;
}

uint8_t MovementManager::getPwmClutchesIndex(void) const
{
	return m_pwmClutchesIndex;
}

void MovementManager::setPwmClutches(uint8_t percent)
{
	syst->GPIO()->updatePWM(m_pwmClutchesIndex, percent);
}

bool MovementManager::isLiftMoving(void) const
{
	return (m_liftMotor.getSpeed() != 0);
}

bool MovementManager::isMatMoving(void) const
{
	return (m_matMotor.getSpeed() != 0);
}

bool MovementManager::isSlideMoving(void) const
{
	return (m_slideMotor.getSpeed() != 0);
}

bool MovementManager::areWheelsMoving(void) const
{
	return (m_rightMotor.getSpeed() != 0 || m_leftMotor.getSpeed() != 0);
}

bool MovementManager::isClutchLocked(side_t side) const
{
	return ((side == Left) ? (m_leftClutch.isLocked()) : (m_rightClutch.isLocked()));
}

bool MovementManager::updateModels(void)
{
	return (m_leftEncoder.update() || m_rightEncoder.update() || m_liftLowLs.update() || m_liftHighLs.update() ||  m_matInLs.update() || m_matOutLs.update());
}

void MovementManager::updateState(void)
{
	if(m_liftLowLs.getState())
		m_liftState = Retracted;
	else if(m_liftHighLs.getState())
		m_liftState = Deployed;

	if(m_matInLs.getState())
		m_matState = Retracted;
	else if(m_matOutLs.getState())
		m_matState = Deployed;
}

void MovementManager::updatePC(void)
{
	int64_t leftError = 0, rightError = 0;

	if(!m_isPCEnabled)
		return;

	if(m_pwmBasedPC)
	{
		if(m_turnRatio >= 0)
			leftError = abs(m_leftEncoder.getSampledSpeed() - ((float)m_rightEncoder.getSampledSpeed() * ((float)m_turnRatio / (float)MAX_STEER_RATIO)));
		else
			rightError = abs(m_rightEncoder.getSampledSpeed() - ((float)m_leftEncoder.getSampledSpeed() * (abs((float)m_turnRatio) / (float)MAX_STEER_RATIO)));
	}
	else
	{
		rightError  = abs(getTargetEncoderStepUS(Right) - m_rightEncoder.getSampledSpeed());
		leftError = abs(getTargetEncoderStepUS(Left) - m_leftEncoder.getSampledSpeed());
	}

	m_leftMotor.setPWM((m_leftMotor.getPWM() + ((float)leftError / m_kpLeftMotor)));
	m_rightMotor.setPWM((m_rightMotor.getPWM() + ((float)rightError / m_kpRightMotor)));
}

void MovementManager::updateOutputs(void)
{
	//TODO: This motor control is pretty stiff. Do something smoother if necessary
	if(!m_isMovementManagingEnabled)
		return;

	if((m_liftState == Retracted || m_liftState == Retracting || m_liftState == Idle) && m_targetLiftState == Deployed)
	{
		m_liftMotor.setSpeed(100);
		m_liftMotor.toggleDirection(1);
		m_liftState = Deploying;
	}
	else if((m_liftState == Deployed || m_liftState == Deploying) && m_targetLiftState == Retracted)
	{
		m_liftMotor.setSpeed(-100);
		m_liftMotor.toggleDirection(0);
		m_liftState = Retracting;
	}
	else if(m_liftState == m_targetLiftState || m_targetLiftState == Idle)
	{
		m_liftMotor.setSpeed(0); //Stop moving if reached wanted position
	}

	if((m_matState == Retracted || m_matState == Retracting || m_matState == Idle) && m_targetMatState == Deployed)
	{
		m_slideMotor.setSpeed(100);
		m_slideMotor.toggleDirection(1);
		m_matState = Deploying;
	}
	else if((m_matState == Deployed || m_matState == Deploying) && m_targetMatState == Retracted)
	{
		m_slideMotor.setSpeed(100);
		m_slideMotor.toggleDirection(0);
		m_matState = Retracting;
	}
	else if(m_matState == m_targetMatState  || m_targetMatState == Idle)
	{
		m_slideMotor.setSpeed(0); //Stop moving if reached wanted position
	}
}

void MovementManager::update(void)
{
	updateModels();
	updateState();
	updatePC();
	updateOutputs();
}

void MovementManager::goForward(uint64_t speed)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = false;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);

	m_leftMotor.toggleDirection(1);
	m_rightMotor.toggleDirection(1);

	setTargetEncoderStepUS(speed, Left);
	setTargetEncoderStepUS(speed, Right);

	updateModels();
	updatePC();
}

void MovementManager::goForward(uint16_t percent)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = true;
	m_turnRatio = MAX_STEER_RATIO;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);

	m_rightMotor.setSpeed(percent);

	m_leftMotor.toggleDirection(1);
	m_rightMotor.toggleDirection(1);

	setTargetEncoderStepUS(m_rightEncoder.getSampledSpeed(), Left);

	updateModels();
	updatePC();
}

void MovementManager::goBackwards(uint64_t speed)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = false;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);

	m_leftMotor.toggleDirection(0);
	m_rightMotor.toggleDirection(0);

	setTargetEncoderStepUS((0 - (int64_t)speed), Left);
	setTargetEncoderStepUS((0 - (int64_t)speed), Right);

	updateModels();
	updatePC();
}

void MovementManager::goBackwards(uint16_t percent)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = true;
	m_turnRatio = MAX_STEER_RATIO;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);

	m_rightMotor.setSpeed(percent);

	m_leftMotor.toggleDirection(0);
	m_rightMotor.toggleDirection(0);

	setTargetEncoderStepUS(m_rightEncoder.getSampledSpeed(), Left);

	updateModels();
	updatePC();
}

void MovementManager::steerRight(uint64_t speed, uint8_t ratio)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = false;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);

	ratio = ud_std::clamp(ratio, 0, MAX_STEER_RATIO);

	m_leftMotor.toggleDirection(0);
	m_rightMotor.toggleDirection(1);

	setTargetEncoderStepUS(speed, Left);
	setTargetEncoderStepUS((speed * ((MAX_STEER_RATIO - ratio) / MAX_STEER_RATIO)), Right);

	updateModels();
	updatePC();
}

void MovementManager::steerLeft(uint16_t percent, uint8_t ratio)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = true;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);

	m_turnRatio = ud_std::clamp(ratio, 0, MAX_STEER_RATIO);

	m_rightMotor.setSpeed(percent);

	m_leftMotor.toggleDirection(0);
	m_rightMotor.toggleDirection(1);

	setTargetEncoderStepUS((percent * (m_turnRatio / MAX_STEER_RATIO)), Left);

	updateModels();
	updatePC();
}

void MovementManager::steerLeft(uint64_t speed, uint8_t ratio)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = false;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);

	ratio = ud_std::clamp(ratio, 0, MAX_STEER_RATIO);

	m_leftMotor.toggleDirection(1);
	m_rightMotor.toggleDirection(0);

	setTargetEncoderStepUS(speed, Right);
	setTargetEncoderStepUS((speed * ((MAX_STEER_RATIO - ratio) / MAX_STEER_RATIO)), Left);

	updateModels();
	updatePC();
}

void MovementManager::steerRight(uint16_t percent, uint8_t ratio)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = true;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);

	m_turnRatio = (0 - ud_std::clamp(ratio, 0, MAX_STEER_RATIO));

	m_leftMotor.setSpeed(percent);

	m_leftMotor.toggleDirection(1);
	m_rightMotor.toggleDirection(0);

	setTargetEncoderStepUS((percent * abs(m_turnRatio / MAX_STEER_RATIO)), Right);

	updateModels();
	updatePC();
}

void MovementManager::engageBreaking(void)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = false;
	m_turnRatio = MAX_STEER_RATIO;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);

	m_leftMotor.setPWM(0);
	m_rightMotor.setPWM(0);

	setTargetEncoderStepUS(0, Left);
	setTargetEncoderStepUS(0, Right);

	updateModels();
	updatePC();
}

void MovementManager::engageFreeRun(void)
{
	setPwmClutches(0);

	m_pwmBasedPC = true;
	m_turnRatio = 0;

	if(isClutchLocked(Left))
		releaseClutch(Left);

	if(isClutchLocked(Right))
		releaseClutch(Right);

	m_leftMotor.setPWM(0);
	m_rightMotor.setPWM(0);

	setTargetEncoderStepUS(2000, Left);
	setTargetEncoderStepUS(2000, Right);

	updateModels();
	updatePC();
}

void MovementManager::disengageFreeRun(void)
{
	setPwmClutches(MAX_PERCENT);

	m_pwmBasedPC = false;
	m_turnRatio = MAX_STEER_RATIO;

	if(!isClutchLocked(Left))
		lockClutch(Left);

	if(!isClutchLocked(Right))
		lockClutch(Right);
}

void MovementManager::spinMat(uint64_t speed)
{
	m_matMotor.setPWM((uint16_t)speed);
}

void MovementManager::deployMat(void)
{
	m_targetMatState = Deployed;

	updateModels();
	updateState();
	updateOutputs();
}

void MovementManager::retractMat(void)
{
	m_targetMatState = Retracted;

	updateModels();
	updateState();
	updateOutputs();
}

void MovementManager::stopSlideMat(void)
{
	m_targetMatState = Idle;
}

void MovementManager::stopSpinMat(void)
{
	m_matMotor.setPWM(0);
}

void MovementManager::deployLift(void)
{
	m_targetLiftState = Deployed;

	updateModels();
	updateState();
	updateOutputs();
}

void MovementManager::retractLift(void)
{
	m_targetLiftState = Retracted;

	updateModels();
	updateState();
	updateOutputs();
}

void MovementManager::stopLift(void)
{
	m_targetLiftState = Idle;

	updateModels();
	updateState();
	updateOutputs();
}

void MovementManager::enablePC(void)
{
	m_isPCEnabled = true;
}

void MovementManager::disablePC(void)
{
	m_isPCEnabled = false;
}

void MovementManager::lockClutch(side_t side)
{
	if(side == Left)
		m_leftClutch.lock();
	else
		m_rightClutch.lock();
}

void MovementManager::releaseClutch(side_t side)
{
	if(side == Left)
		m_leftClutch.release();
	else
		m_rightClutch.release();
}

void MovementManager::ctrlMotorLift(int16_t speed)
{
	m_liftMotor.setSpeed(speed);
}

void MovementManager::ctrlMotorLeft(int16_t speed)
{
	m_leftMotor.setSpeed(speed);
}

void MovementManager::ctrlMotorRight(int16_t speed)
{
	m_rightMotor.setSpeed(speed);
}

void MovementManager::ctrlMotorMat(int16_t speed)
{
	m_matMotor.setSpeed(speed);
}

void MovementManager::ctrlMotorSlide(int16_t speed)
{
	m_slideMotor.setSpeed(speed);
}
