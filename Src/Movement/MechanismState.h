/**
* @file	MechanismState.h
* @brief Header file for the MechanismState enum
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 28th, 2018
*
* The MechanismState enum allows a more user-friendly selection for a unit's movement state
*
*/


#ifndef MECHANISMSTATE_H_
#define MECHANISMSTATE_H_

typedef enum MechanismState {
  Idle = 0x00,
  Deployed,
  Retracted,
  Deploying,
  Retracting,
  Unknown
} mech_state;

#endif /* MECHANISMSTATE_H_ */
