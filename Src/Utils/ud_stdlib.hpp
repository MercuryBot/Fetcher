/**
* @file ud_std.hpp
* @brief Header file containing the ud_std namespace with various functions
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date February 12th, 2018
*
* This file declares and defines the ud_std(user defined standard) namespace with its contents.
*
*/

#ifndef UD_STDLIB_HPP_
#define UD_STDLIB_HPP_

/**
* @namespace ud_std
* @brief User-defined standard functions and other tools
*/
namespace ud_std {

	template<class T, class U, class V>
	/**
	* @brief Alternative to the C++17 implementation of the function of the same name. Sets a maximum and a minimum for a given variable.
	* @param v Variable to set maximum and minimum to
	* @param lo Minimum to set the variable to
	* @param hi Maximum to set the variable to
	* @return The value of lo if v was lower than lo, the value of hi if v was higher than hi, and v unaltered otherwise
	*/
	extern const T clamp( const T& v, const U& lo, const V& hi)
	{
		if(v < (T)lo)
			return (T)lo;
		else if (v > (T)hi)
			return (T)hi;
		else
			return (T)v;
	}
}

#endif /* UD_STDLIB_HPP_ */
