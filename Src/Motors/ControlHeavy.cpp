/**
 * @file ControlHeavy.cpp
 * @brief Definition file for the ControlHeavy class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ControlHeavy class provides easier control over motor that uses the relays
 *
 */
#include "ControlHeavy.hpp"
/**
 * @brief The class base constructor
 */
ControlHeavy::ControlHeavy() : ControlMotor()
{

}
/**
 * @brief The class constructor with parameter
 * @param port Port of the motor
 * @param pin Pin of the motor
 * @param pwmIndex Index of the pwm handler and channel
 */
ControlHeavy::ControlHeavy(GPIO_TypeDef* port, uint16_t pin, uint8_t pwmIndex) : ControlMotor(port, pin, pwmIndex)
{

}
/**
 * @brief The class base destructor
 */
ControlHeavy::~ControlHeavy()
{

}
/**
 * @brief Setter for the speed
 * @param speed The speed
 */
void ControlHeavy::setSpeed(int16_t speed)
{
	ControlMotor::setSpeed(speed);

	if(getSpeed() > 0)
		ControlMotor::toggleDirection(1);
	else
		ControlMotor::toggleDirection(0);
}
/**
 * @brief Function to toggle the direction of the motor
 * @param dir The direction to set to, true for forward and false for backward
 */
void ControlHeavy::toggleDirection(bool dir)
{
	ControlMotor::togglePosPin(dir);

	if(dir)
		setSpeed((int16_t)abs(getSpeed()));
	else
		setSpeed(0 - (int16_t)abs(getSpeed()));
}
