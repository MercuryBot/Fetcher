/**
 * @file ControlLight.hpp
 * @brief Header file for the ControlLight class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ControlLight class provides easier control over motor that uses the SN744510NE drive
 *
 */
#ifndef CONTROLLIGHT_HPP_
#define CONTROLLIGHT_HPP_

#include "ControlMotor.hpp"
/**
 * @class ControlLight.hpp
 * @brief Provides easier control over motor that uses the SN744510NE drive
 */
class ControlLight: public ControlMotor
{

private:
	/**
	 * @brief The port of the negative motor pin
	 */
	GPIO_TypeDef* m_portNeg;
	/**
	 * @brief The pin of the negative motor pin
	 */
	uint16_t m_pinNeg;

public:
	/**
	 * @brief The class base constructor
	 */
	ControlLight();
	/**
	 * @brief The class constructor with parameter
	 * @param port Positive port of the motor
	 * @param pin Positive pin of the motor
	 * @param portNeg Negative port of the motor
	 * @param pinNeg Negative pin of the motor
	 * @param pwmIndex Index of the pwm handler and channel
	 */
	ControlLight(GPIO_TypeDef* port, uint16_t pin, GPIO_TypeDef* portNeg, uint16_t pinNeg, uint8_t pwmIndex);
	/**
	 * @brief The class base destructor
	 */
	virtual ~ControlLight();
	/**
	 * @brief Getter for the negative port attribute
	 * @return A pointer to the port
	 */
	GPIO_TypeDef* getPortNeg(void);
	/**
	 * @brief Getter for the negative pin attribute
	 * @return The pin in uint16_t
	 */
	uint16_t getPinNeg(void) const;

	/**
	 * @brief Setter for the negative port attribute
	 * @param port The new port
	 */
	void setPortNeg(GPIO_TypeDef* port);
	/**
	 * @brief Setter for the negative pin attribute
	 * @param pin The new pin
	 */
	void setPinNeg(uint16_t pin);
	/**
	 * @brief Function to toggle the state of the negative pin saved in the attribute
	 * @param state The state to change the pin to.
	 */
	void toggleNegPin(bool state);
	/**
	 * @brief Setter for the speed
	 * @param speed The speed
	 */
	void setSpeed(int16_t speed);
	/**
	 * @brief Function to toggle the direction of the motor
	 * @param dir The direction to set to, true for forward and false for backward
	 */
	void toggleDirection(bool dir);
	/**
	 * @brief Function to release the motor (and set speed to 0)
	 */
	void release();
	/**
	 * @brief Function to release the motor
	 */
	void releasePins();
};

#endif /* CONTROLLIGHT_HPP_ */
