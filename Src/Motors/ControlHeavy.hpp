/**
 * @file ControlHeavy.hpp
 * @brief Header file for the ControlHeavy class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ControlHeavy class provides easier control over motor that uses the relays
 *
 */
#ifndef CONTROLHEAVY_HPP_
#define CONTROLHEAVY_HPP_

#include "ControlMotor.hpp"
/**
 * @class ControlHeavy.hpp
 * @brief Provides easier control over motor that uses the relays
 */
class ControlHeavy : public ControlMotor
{
private:

public:
	/**
	 * @brief The class base constructor
	 */
	ControlHeavy();
	/**
	 * @brief The class constructor with parameter
	 * @param port Port of the motor
	 * @param pin Pin of the motor
	 * @param pwmIndex Index of the pwm handler and channel
	 */
	ControlHeavy(GPIO_TypeDef* port, uint16_t pin, uint8_t pwmIndex);
	/**
	 * @brief The class base destructor
	 */
	virtual ~ControlHeavy();
	/**
	 * @brief Setter for the speed
	 * @param speed The speed
	 */
	void setSpeed(int16_t speed);
	/**
	 * @brief Function to toggle the direction of the motor
	 * @param dir The direction to set to, true for forward and false for backward
	 */
	void toggleDirection(bool dir);
	//void release(void);

};

#endif /* CONTROLHEAVY_HPP_ */
