/**
 * @file ControlMotor.cpp
 * @brief Definition file for the ControlMotor class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ControlMotor class provides easier control over individual motor
 *
 */

#include "ControlMotor.hpp"
/**
 * @brief The class base constructor
 */
ControlMotor::ControlMotor()
{
	m_port = nullptr;
	m_pin = 0;
	m_pwmIndex = 0;
	m_speed = 0;
}
/**
 * @brief The class constructor with parameter
 * @param port Port of the motor
 * @param pin Pin of the motor
 * @param pwmIndex Index of the pwm handler and channel
 */
ControlMotor::ControlMotor(GPIO_TypeDef* port, uint16_t pin, uint8_t pwmIndex)
{
	m_port = port;
	m_pin = pin;
	m_pwmIndex = pwmIndex;
	m_speed = 0;
}
/**
 * @brief The class base destructor
 */
ControlMotor::~ControlMotor()
{

}
/**
 * @brief Getter for the port attribute
 * @return A pointer to the port
 */
GPIO_TypeDef* ControlMotor::getPort(void) const
{
	return m_port;
}
/**
 * @brief Getter for the pin attribute
 * @return The pin in uint16_t
 */
uint16_t ControlMotor:: getPin(void) const
{
	return m_pin;
}
/**
 * @brief Getter for the speed attribute
 * @return The speed in 0-100%
 */
int16_t ControlMotor::getSpeed(void) const
{
	return m_speed;
}
/**
 * @brief Getter for the pwm index attribute
 * @return The index of the pwm
 */
uint8_t ControlMotor::getPMWIndex(void) const
{
	return m_pwmIndex;
}
/**
 * @brief Getter for the pwm value
 * @return The pwm value in 0-100%
 */
uint16_t ControlMotor::getPWM(void) const
{
	return (abs(m_speed));
}
/**
 * @brief Getter for the direction attribute
 * @return The direction in boolean, true for forward or false for backward
 */
bool ControlMotor::getDirection(void) const
{
	return (m_speed > 0);
}
/**
 * @brief Setter for the port attribute
 * @param port The new port
 */
void ControlMotor::setPort(GPIO_TypeDef* port)
{
	m_port = port;
}
/**
 * @brief Setter for the pin attribute
 * @param pin The new pin
 */
void ControlMotor::setPin(uint16_t pin)
{
	m_pin = pin;
}
/**
 * @brief Setter for the pwm index attribute
 * @param index The index of the new handler
 */
void ControlMotor::setPWMIndex(uint8_t index)
{
	m_pwmIndex = index;
}
/**
 * @brief Setter for the pwm value (the target)
 * @param pwm The target pwm to reach in 0-100%
 */
void ControlMotor::setSpeed(int16_t speed)
{
	m_speed = ud_std::clamp(speed, -100, 100);

	setPWM(abs(m_speed));
}
/**
 * @brief Function to toggle the state of the pin passed in parameter
 * @param state The state to change the pin to.
 */
void ControlMotor::setPWM(uint16_t pwm)
{
	pwm = ud_std::clamp(pwm, 0, 100);

	m_speed = (m_speed >= 0) ? pwm : (int16_t)(pwm * -1);

	syst->GPIO()->updatePWM(m_pwmIndex, pwm);
}
/**
 * @brief Setter for the speed
 * @param speed The speed
 */
void ControlMotor::togglePosPin(bool state)
{
	if(state)
		syst->GPIO()->setPin(m_port, m_pin);
	else
		syst->GPIO()->resetPin(m_port, m_pin);
}
/**
 * @brief Function to release the motor (and set speed to 0)
 */
void ControlMotor::release()
{
	setSpeed(0);
}
/**
 * @brief Virtual function to toggle the direction of the motor
 * @param dir The direction to set to, true for forward and false for backward
 */
void ControlMotor::toggleDirection(bool dir)
{
	m_speed = (dir) ? (abs(m_speed)) : (0 - abs(m_speed));
}
