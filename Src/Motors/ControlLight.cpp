/**
 * @file ControlLight.cpp
 * @brief Definition file for the ControlLight class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ControlLight class provides easier control over motor that uses the SN744510NE drive
 *
 */
#include "ControlLight.hpp"
/**
 * @brief The class base constructor
 */
ControlLight::ControlLight() : ControlMotor()
{
	m_portNeg = nullptr;
	m_pinNeg = 0;
}
/**
 * @brief The class constructor with parameter
 * @param port Positive port of the motor
 * @param pin Positive pin of the motor
 * @param portNeg Negative port of the motor
 * @param pinNeg Negative pin of the motor
 * @param pwmIndex Index of the pwm handler and channel
 */
ControlLight::ControlLight(GPIO_TypeDef* port, uint16_t pin, GPIO_TypeDef* portNeg, uint16_t pinNeg, uint8_t pwmIndex) : ControlMotor(port, pin, pwmIndex)
{
	m_portNeg = portNeg;
	m_pinNeg = pinNeg;
}
/**
 * @brief The class base destructor
 */
ControlLight::~ControlLight()
{
	//delete m_portNeg;
}
/**
 * @brief Getter for the negative port attribute
 * @return A pointer to the port
 */
GPIO_TypeDef* ControlLight::getPortNeg(void)
{
	return m_portNeg;
}
/**
 * @brief Getter for the negative pin attribute
 * @return The pin in uint16_t
 */
uint16_t ControlLight::getPinNeg(void) const
{
	return m_pinNeg;
}
/**
 * @brief Setter for the negative port attribute
 * @param port The new port
 */
void ControlLight::setPortNeg(GPIO_TypeDef* port)
{
	m_portNeg = port;
}
/**
 * @brief Setter for the negative pin attribute
 * @param pin The new pin
 */
void ControlLight::setPinNeg(uint16_t pin)
{
	m_pinNeg = pin;
}
/**
 * @brief Function to toggle the state of the negative pin saved in the attribute
 * @param state The state to change the pin to.
 */
void ControlLight::toggleNegPin(bool state)
{
	if(state)
		syst->GPIO()->setPin(m_portNeg, m_pinNeg);
	else
		syst->GPIO()->resetPin(m_portNeg, m_pinNeg);
}
/**
 * @brief Setter for the speed
 * @param speed The speed
 */
void ControlLight::setSpeed(int16_t speed)
{
	ControlMotor::setSpeed(speed);

	if(ControlMotor::getSpeed() > 0)
		toggleDirection(1);
	else if(ControlMotor::getSpeed() < 0)
		toggleDirection(0);
	else
		releasePins();
}
/**
 * @brief Virtual function to toggle the direction of the motor
 * @param dir The direction to set to, true for forward and false for backward
 */
void ControlLight::toggleDirection(bool dir)
{
	ControlMotor::toggleDirection(dir);

	if(dir)
	{
		ControlMotor::togglePosPin(1);
		toggleNegPin(0);
	}
	else
	{
		ControlMotor::togglePosPin(0);
		toggleNegPin(1);
	}
}
/**
 * @brief Function to release the motor (and set speed to 0)
 */
void ControlLight::release()
{
	ControlMotor::setSpeed(0);
	ControlMotor::togglePosPin(0);
	toggleNegPin(0);
}
/**
 * @brief Function to release the motor
 */
void ControlLight::releasePins()
{
	ControlMotor::togglePosPin(0);
	toggleNegPin(0);
}
