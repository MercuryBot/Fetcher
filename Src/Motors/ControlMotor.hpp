/**
 * @file ControlMotor.hpp
 * @brief Header file for the ControlMotor class
 * @author William Marcoux (mailto:william.marcoux.98@gmail.com)
 * @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
 * @version 1.0.0
 * @date March 28th, 2018
 *
 * The ControlMotor class provides easier control over individual motor
 *
 */

#ifndef CONTROLMOTOR_HPP_
#define CONTROLMOTOR_HPP_

#include "stm32f4xx_hal.h"
#include "ControlSystem.hpp"
#include "ud_stdlib.hpp"
#include "timers.h"

/**
 * @class ControlMotor.hpp
 * @brief Provides easier control over individual motor
 */
class ControlMotor {

private:
	/**
	 * @brief Port of the motor
	 */
	GPIO_TypeDef* m_port;
	/**
	 * @brief Pin of the motor
	 */
	uint16_t m_pin;
	/**
	 * @brief The target speed of the motor in 0-100%
	 */
	uint16_t m_speed;
	/**
	 * The index of the PWM handler and channel
	 */
	uint8_t m_pwmIndex;

public:
	/**
	 * @brief The class base constructor
	 */
	ControlMotor();
	/**
	 * @brief The class constructor with parameter
	 * @param port Port of the motor
	 * @param pin Pin of the motor
	 * @param pwmIndex Index of the pwm handler and channel
	 */
	ControlMotor(GPIO_TypeDef* port, uint16_t pin, uint8_t pwmIndex);
	/**
	 * @brief The class base destructor
	 */
	virtual ~ControlMotor();

	/**
	 * @brief Getter for the port attribute
	 * @return A pointer to the port
	 */
	GPIO_TypeDef* getPort(void) const;
	/**
	 * @brief Getter for the pin attribute
	 * @return The pin in uint16_t
	 */
	uint16_t getPin(void) const;
	/**
	 * @brief Getter for the speed attribute
	 * @return The speed in 0-100%
	 */
	int16_t getSpeed(void) const;
	/**
	 * @brief Getter for the pwm index attribute
	 * @return The index of the pwm
	 */
	uint8_t getPMWIndex(void) const;
	/**
	 * @brief Getter for the pwm value
	 * @return The pwm value in 0-100%
	 */
	uint16_t getPWM(void) const;
	/**
	 * @brief Getter for the direction attribute
	 * @return The direction in boolean, true for forward or false for backward
	 */
	bool getDirection(void) const;

	/**
	 * @brief Setter for the port attribute
	 * @param port The new port
	 */
	void setPort(GPIO_TypeDef* port);
	/**
	 * @brief Setter for the pin attribute
	 * @param pin The new pin
	 */
	void setPin(uint16_t pin);
	/**
	 * @brief Setter for the pwm index attribute
	 * @param index The index of the new handler
	 */
	void setPWMIndex(uint8_t index);
	/**
	 * @brief Setter for the pwm value (the target)
	 * @param pwm The target pwm to reach in 0-100%
	 */
	void setPWM(uint16_t pwm);
	/**
	 * @brief Function to toggle the state of the pin saved in the attribute
	 * @param state The state to change the pin to.
	 */
	void togglePosPin(bool state);
	/**
	 * @brief Setter for the speed
	 * @param speed The speed
	 */
	virtual void setSpeed(int16_t speed);
	/**
	 * @brief Function to release the motor (and set speed to 0)
	 */
	virtual void release(void);
	/**
	 * @brief Virtual function to toggle the direction of the motor
	 * @param dir The direction to set to, true for forward and false for backward
	 */
	virtual void toggleDirection(bool dir);
};

#endif /* CONTROLMOTOR_HPP_ */
