/**
* @file RfManager.hpp
* @brief Header file for the RfManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 3rd, 2018
*
* The RfManager class provides control over the nRF24L01+ module
* and the MercuryBot communication protocol.
*
*/

#ifndef RFMANAGER_HPP_
#define RFMANAGER_HPP_

/**
* @def TX_CONFIG
* @brief Layout of the CONFIG register to put the module in transmission mode
*/
#define TX_CONFIG ((1<<PWR_UP) | (1<<CRCO) | (1<<EN_CRC) | (1<<MASK_TX_DS) | (1<<MASK_RX_DR) | (1<<MASK_MAX_RT))
/**
* @def RX_CONFIG
* @brief Layout of the CONFIG register to put the module in reception mode
*/
#define RX_CONFIG ((1<<PWR_UP) | (1<<CRCO) | (1<<EN_CRC) | (1<<MASK_TX_DS) | (1<<MASK_MAX_RT) | (1<<PRIM_RX))

/**
* @def FINDING_PORT_IDX
* @brief Index of the data pipe allocated to sweep for addresses
*/
#define FINDING_PORT_IDX 0
/**
* @def MAX_UNITS
* @brief Maximum units on a network (limited to 35 temporarily for debugging purposes)
*/
#define MAX_UNITS 35
/**
* @def MAX_UNITS_PER_PIPE
* @brief Maximum units on a single data pipe
*/
#define MAX_UNITS_PER_PIPE (MAX_UNITS/(MAX_DATA_PIPES - 1))
/**
* @def OPERATING_FREQUENCY_MHZ
* @brief Operating frequency for the protocol
*/
#define OPERATING_FREQUENCY_MHZ 2489

/**
* @def PACKET_LEN
* @brief Default packet length
*/
#define PACKET_LEN 32
/**
* @def PACKET_SENDER_ADDR_IDX
* @brief In-packet index of the sender address
*/
#define PACKET_SENDER_ADDR_IDX 1
/**
* @def PACKET_TARGET_ADDR_IDX
* @brief In-packet index of the target address
*/
#define PACKET_TARGET_ADDR_IDX (PACKET_SENDER_ADDR_IDX + MAX_ADDR_LEN)
/**
* @def PACKET_HEAD_IDX
* @brief In-packet index of the packet head
*/
#define PACKET_HEAD_IDX (PACKET_TARGET_ADDR_IDX + MAX_ADDR_LEN)
/**
* @def PACKET_BODY_IDX
* @brief In-packet index of the packet body
*/
#define PACKET_BODY_IDX (PACKET_HEAD_IDX + HEAD_LEN)
/**
* @def COMM_ID_NOT_INIT
* @brief Default communication ID if uninitialized
*/
#define COMM_ID_NOT_INIT '*'

/**
* @def ADDR_PIPE_IDX
* @brief Index of the address allocated for the pipe's index
*/
#define ADDR_PIPE_IDX 0
/**
* @def ADDR_FUNC_IDX
* @brief Index of the address allocated for the pipe's function
*/
#define ADDR_FUNC_IDX 1
/**
* @def ADDR_COMM_ID_IDX
* @brief Index of the address allocated for the pipe's communication ID
*/
#define ADDR_COMM_ID_IDX 2
/**
* @def ADDR_DEVICE_TYPE_IDX
* @brief Index of the address allocated for the pipe's device type
*/
#define ADDR_DEVICE_TYPE_IDX 3
/**
* @def ADDR_STATUS_IDX
* @brief Index of the address allocated for the pipe's current status
*/
#define ADDR_STATUS_IDX 4

/**
* @def DONT_CARE_CHAR
* @brief Character in telecommunication payloads for characters to ignore
*/
#define DONT_CARE_CHAR '#'

/**
* @def FUNC_NOTINIT
* @brief Function not yet defined for pipe
*/
#define FUNC_NOTINIT '0'
/**
* @def FUNC_UNIT
* @brief Function is communication between Receptor and units for the pipe
*/
#define FUNC_UNIT '1'
/**
* @def FUNC_SWEEP
@brief Function is sweeping for finding a Receptor to connect to
*/
#define FUNC_SWEEP '2'
/**
* @def FUNC_ADDR
* @brief Function for the pipe is to intercept and reply if an address is already taken for a unit
*/
#define FUNC_ADDR '3'
/**
* @def FUNC_OTHER
* @brief Function for the pipe is something else
*/
#define FUNC_OTHER '4'
/**
* @def FUNC_NOTINIT
* @brief The pipe is blocked, has a denied access
*/
#define FUNC_BLOCKED '5'

/**
* @def DEVICE_TYPE_NOT_INIT
* @brief The device type is undefined
*/
#define DEVICE_TYPE_NOT_INIT '0'
/**
* @def DEVICE_TYPE_UNIT
* @brief The device type is a unit
*/
#define DEVICE_TYPE_UNIT 'U'
/**
* @def DEVICE_TYPE_RECEPT
* @brief The device type is a Receptor
*/
#define DEVICE_TYPE_RECEPT 'R'

/**
* @def STATUS_OK
* @brief The status of the pipe is normal
*/
#define STATUS_OK '0'
/**
* @def STATUS_ERROR
* @brief The status of the pipe is not normal/in error
*/
#define STATUS_ERROR '1'

/**
* @def MAX_PONG_DELAY_MS
* @brief Maximum delay (in milliseconds) allowed for a pong back before time out
*/
#define MAX_PONG_DELAY_MS 5*1000
/**
* @def MAX_INTER_DELAY_MS
* @brief Maximum delay (in milliseconds) allowed without activity with a certain unit
*/
#define MAX_INTER_DELAY_MS 60*1000
/**
* @def MAX_SEQU_DELAY_MS
* @brief Maximum delay (in milliseconds) allowed without activity with a certain unit since the beginning of a sequence
*/
#define MAX_SEQU_DELAY_MS 45*1000
/**
* @def NEW_CONN_REQUEST_DELAY
* @brief Maximum delay waiting before connecting to a Receptor
*/
#define NEW_CONN_REQUEST_DELAY 250
/**
* @def ADDR_TAKING_REQUEST_DELAY
* @brief Maximum delay waiting before auto-assigning and address
*/
#define ADDR_TAKING_REQUEST_DELAY 250
/**
* @def MAX_ALRT_DELAY_MS
* @brief Maximum delay waiting before re-sending an alert
*/
#define MAX_ALRT_DELAY_MS 5*1000

#include "RfPacket.h"
#include "Receptor.h"
#include "Alert.h"

#include "AutomationManager.hpp"

typedef void task;

class RfManager: public ControlRf {

private:
	std::string m_buffer;

	bool m_init;
	bool m_foundAddress;
	bool m_foundReceptor;
	bool m_connectedReceptor;
	bool m_receivedPacket;
	bool m_sentInvalidPacket;

	uint32_t m_timeSinceAddrTakingRequest;
	uint32_t m_timeSinceConnRequest;

	char m_commId;
	char m_commDeviceType;
	char m_mainAddr[MAX_ADDR_LEN + 1];
	char m_addressingAddr[MAX_ADDR_LEN + 1];
	char m_sweepingAddr[MAX_ADDR_LEN + 1];

	Receptor m_receptor;

	LinkedList<Alert> m_alertList;

public:

	RfManager();
	~RfManager();

	void sendData(char* a_addr, char* a_data);
	void readData(void);

	void dropPipe(uint8_t idx);
	void setPipe(uint8_t idx, char* a_addr);

	void init(void);

	task mainTask(AutomationManager* automation);
	task intTask(void);
	task timerTask(void);

	RfPacket extractPacket(void);
	void sendPacket(RfPacket packet);

	void addAlert(alert_t code);
	void clearAlert(alert_t code);
	void clearAlertList(void);

	void setCommDeviceType(char deviceType);
	void setCommID(char ID);
	void setInit(bool data);
	void setFoundAddress(bool data);
	void setFoundReceptor(bool data);
	void setConnectedReceptor(bool data);
	void setSentInvalidPacket(bool data);
	void setMainAddress(char* addr);
	void setAddressingAddress(char* addr);

	Receptor *receptor(void);
	char getCommDeviceType(void) const;
	char getCommID(void) const;
	void getMainAddress(char* outData);
	void getAddressingAddress(char* outData);

	bool isInit(void) const;
	bool foundAddress(void) const;
	bool foundReceptor(void) const;
	bool connectedReceptor(void) const;
	bool receivedPacket(void) const;
	bool sentInvalidPacket(void) const;

	void receptPING(RfPacket packet);
	void receptPONG(RfPacket packet);
	void receptNCON(RfPacket packet);
	void receptSCON(RfPacket packet);
	void receptBCON(RfPacket packet);
	void receptTPOS(RfPacket packet, AutomationManager* automation);
	void receptACKA(RfPacket packet);
	void receptRSTT(RfPacket packet, AutomationManager* automation);
	void receptDEPL(RfPacket packet, AutomationManager* automation);
	void receptMODE(RfPacket packet, AutomationManager* automation);
	void receptMOVE(RfPacket packet, AutomationManager* automation);
	void receptSEQU(RfPacket packet, AutomationManager* automation);
	void receptDROP(RfPacket packet);
	void receptEADR(RfPacket packet);
	void receptTADR(RfPacket packet);
	void receptNCOM(RfPacket packet);

    void sendPING(char* dest);
    void sendPONG(char* dest);
    void sendNCON(char* dest);
	void sendCPOS(char* dest, PosCode code);
	void sendABAT(char* dest);
	void sendLBAT(char* dest, float level);
	void sendALRT(char* dest, uint8_t code);
	void sendCSTT(char* dest, char* stateArray);
	void sendSDON(char* dest, uint8_t code, uint8_t specs);
	void sendDROP(char* dest);
	void sendEADR(char* dest, char* addr);
	void sendTADR(char* dest, char* addr);
	void sendNCOM(char* dest);
};


#endif /* RFMANAGER_HPP_ */
