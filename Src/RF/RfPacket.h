/**
* @file RfPacket.h
* @brief Header file for the RfPacket class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 3rd, 2018
*
* The RfPacket struct describes the basic structure of a packet sent
* over RF module.
*
*/

#ifndef RFPACKET_H_
#define RFPACKET_H_

/**
 * @def MAX_ADDR_LEN
 * @brief Maximum address pipe length
 */
#define MAX_ADDR_LEN 5

/**
* @def HEAD_LEN
* @brief Length of a packet's head
*/
#define HEAD_LEN 4

/**
* @def BODY_LEN
* @brief Length of a packet's body
*/
#define BODY_LEN 16

/**
* @def PACKET_LEN
* @brief Total length of a packet
*/
#define PACKET_LEN 32

/**
* @def PACKET_SENDER_ADDR_IDX
* @brief Index of the packet sender's address in a packet
*/
#define PACKET_SENDER_ADDR_IDX 1

/**
* @def PACKET_TARGET_ADDR_IDX
* @brief Index of the packet's target address in a packet
*/
#define PACKET_TARGET_ADDR_IDX (PACKET_SENDER_ADDR_IDX + MAX_ADDR_LEN)

/**
* @def PACKET_HEAD_IDX
* @brief Index of the packet's head in a packet
*/
#define PACKET_HEAD_IDX (PACKET_TARGET_ADDR_IDX + MAX_ADDR_LEN)

/**
* @def PACKET_BODY_IDX
* @brief Index of the packet's body in a packet
*/
#define PACKET_BODY_IDX (PACKET_HEAD_IDX + HEAD_LEN)

/**
* @struct RfPacket
* @brief Describes the basic structure of a packet sent over RF module
*/
struct RfPacket {

	/**
	* @brief Packet sender's address
	*/
	char senderAddr[MAX_ADDR_LEN + 1];

	/**
	* @brief Packet's target address
	*/
	char targetAddr[MAX_ADDR_LEN + 1];

	/**
	* @brief Packet's head
	*/
	char instructHead[HEAD_LEN + 1];

	/**
	* @brief Packet's body
	*/
	char instructBody[BODY_LEN + 1];
};

#endif /* RFPACKET_H_ */
