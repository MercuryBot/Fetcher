/**
* @file Alerts.h
* @brief Header file for the Alerts enum
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 3rd, 2018
*
* The enum facilitates the indexing of alert codes
*/

#ifndef ALERTS_H_
#define ALERTS_H_

/**
 * @enum Alerts
 * @brief Facilitates the indexing of alert codes
 */
typedef enum Alerts {
	LowBattery = '0',//!< LowBattery
	Collision        //!< Collision
} alert_t;

#endif /* ALERTS_H_ */
