/**
* @file RfManager.cpp
* @brief Definition file for the RfManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 3rd, 2018
*
* The RfManager class provides control over the nRF24L01+ module
* and the MercuryBot communication protocol.
*
*/

#include "RfManager.hpp"

RfManager::RfManager()
{
	m_buffer = "";

	m_init = false;
	m_foundAddress = false;
	m_foundReceptor = false;
	m_connectedReceptor = false;
	m_receivedPacket = false;
	m_sentInvalidPacket = false;

	m_commId = COMM_ID_NOT_INIT;
    m_commDeviceType = DEVICE_TYPE_NOT_INIT;

    m_timeSinceConnRequest = 0;
    m_timeSinceAddrTakingRequest = 0;

    sprintf(m_mainAddr, "110U0");
    sprintf(m_addressingAddr, "030U0");
    sprintf(m_sweepingAddr, "%c%c%c%c%c", '0', FUNC_SWEEP, '0', DEVICE_TYPE_RECEPT, STATUS_OK);

	Receptor m_receptor  = {0, 0, 0, 0};
	sprintf(m_receptor.address, "00000");

	m_alertList = LinkedList<Alert>();
}

RfManager::~RfManager()
{

}

void RfManager::sendData(char* a_addr, char* a_data)
{
	setSendingPipe(a_addr);
	setOpMode(0); //TX mode
	transmitMsg((uint8_t*)a_data, getPayloadLen());
	setSendingPipe((char*)"00000");
	setOpMode(1);
}

void RfManager::addAlert(alert_t code)
{
	for(uint8_t i = 0; i < m_alertList.size(); ++i)
	{
		if(m_alertList[i].alertCode == code)
		{
			if(m_alertList[i].acknowledged)
				m_alertList[i].acknowledged = false;

			return;
		}
	}

	m_alertList.append(*(new Alert{.alertCode = (alert_t)code, .acknowledged = false, .timeSinceLastNotif = 0}));
}

void RfManager::clearAlert(alert_t code)
{
	for(uint8_t i = 0; i < m_alertList.size(); ++i)
	{
		if(m_alertList[i].alertCode == code)
		{
			m_alertList.dropAt(i);
			return;
		}
	}
}

void RfManager::clearAlertList(void)
{
	m_alertList = LinkedList<Alert>();
}

void RfManager::readData(void)
{
	char tmpBuf[getPayloadLen()];

	syst->SPI()->toggleCEPin(0);

	receiveMsg((uint8_t*)tmpBuf);
	m_buffer.append(tmpBuf);
	resetRF();

	m_receivedPacket = false;

	syst->SPI()->toggleCEPin(1);
}

void RfManager::dropPipe(uint8_t idx)
{
	if(!isPipeActive(idx)) //Port already dropped
		return;

	setDataPipsNum(getDataPipsNum() - 1);
	setListeningPipe(idx, (char*)"00000");
	disableDataPipe(idx);
}

void RfManager::setPipe(uint8_t idx, char* a_addr)
{
	if(!isPipeActive(idx))
		setDataPipsNum(getDataPipsNum() + 1);

	setListeningPipe(idx, a_addr);
	enableDataPipe(idx);
}

void RfManager::init(void)
{
	uint8_t tmp[32];

	if(!syst->SPI()->isInit())
		return;

	HAL_Delay(100);	//Leave time if in shutdown process

	setAutoAck(true);
	setDataPipsNum(MAX_DATA_PIPES);
	enableDataPipe(0);
	enableDataPipe(1);
	setAddrLen(MAX_ADDR_LEN);
	setChannelFreqMHZ(OPERATING_FREQUENCY_MHZ);
	setDataRate(DATARATE_1MBPS);
	setPALevel(PALEVEL4);

	setListeningPipe(0, m_addressingAddr);
	setListeningPipe(1, m_mainAddr);
	setSendingPipe((char*)"00000");

	setPayloadLen(MAX_PL_LEN);

	setRetryCount(MAX_RETRY_CNT);
	setRetryDelay(MAX_RETRY_DELAY_US);

	setConfig(RX_CONFIG); //Register is set directly to save writing time (also logically simpler)

	readReg(R_RX_PAYLOAD, 32, tmp); //Line to avoid undetected rx interrupt bug.
	resetRF();

	m_init = true;
    m_commDeviceType = DEVICE_TYPE_UNIT;

    HAL_Delay(100);

    syst->SPI()->toggleCEPin(0); //Start listening
}

task RfManager::mainTask(AutomationManager* automation)
{
	RfPacket inPacket;

	if(!m_foundAddress && m_timeSinceAddrTakingRequest >= ADDR_TAKING_REQUEST_DELAY)
	{
		setListeningPipe(1, m_mainAddr);

		m_foundAddress = true;
		m_timeSinceAddrTakingRequest = 0;
	}

	if(m_foundAddress && !m_connectedReceptor && m_timeSinceConnRequest >= NEW_CONN_REQUEST_DELAY)
	{
		sendNCON(m_sweepingAddr);

		switch(m_sweepingAddr[ADDR_COMM_ID_IDX])
		{
			case '9':
				m_sweepingAddr[ADDR_COMM_ID_IDX] = 'A';
			break;
			case 'Z':
				m_sweepingAddr[ADDR_COMM_ID_IDX] = 'a';
			break;
			case 'z':
				m_sweepingAddr[ADDR_COMM_ID_IDX] = '0';
			break;
			default:
				++m_sweepingAddr[ADDR_COMM_ID_IDX];
			break;
		}

		m_timeSinceConnRequest = 0;
	}

	//Drop device, timeout connection
	if(m_receptor.awaitingPong && m_receptor.timeSincePing > MAX_PONG_DELAY_MS) //Device is dead
	{
		sendDROP(m_receptor.address);
		m_foundReceptor = false;
	}

	//Ping device
	if(!m_receptor.awaitingPong && m_receptor.timeSinceLastMsg >= MAX_INTER_DELAY_MS)
	{
		sendPING(m_receptor.address);
		m_receptor.awaitingPong = true;
	}

	//Re-send alerts if unacknowledged
	if(m_foundReceptor)
	{
		for(uint8_t i = 0; i < m_alertList.size(); ++i)
		{
			if(!m_alertList.at(i).acknowledged && m_alertList.at(i).timeSinceLastNotif >= MAX_ALRT_DELAY_MS)
			{
				sendALRT(m_receptor.address, m_alertList.at(i).alertCode);
				m_alertList.at(i).timeSinceLastNotif = 0;
			}
		}
	}

	if(!m_receivedPacket)
		return;

	inPacket = extractPacket();

	if(strncmp(inPacket.senderAddr, m_receptor.address, getAddrLen()) == 0)
		m_receptor.timeSinceLastMsg = 0;

	if(strncmp(inPacket.instructHead, "PING", HEAD_LEN) == 0)
		receptPING(inPacket);
	else if(strncmp(inPacket.instructHead, "PONG", HEAD_LEN) == 0)
		receptPONG(inPacket);
	else if(strncmp(inPacket.instructHead, "SCON", HEAD_LEN) == 0)
			receptSCON(inPacket);
	else if(strncmp(inPacket.instructHead, "BCON", HEAD_LEN) == 0)
			receptBCON(inPacket);
	else if(strncmp(inPacket.instructHead, "TPOS", HEAD_LEN) == 0)
			receptTPOS(inPacket, automation);
	else if(strncmp(inPacket.instructHead, "ACKA", HEAD_LEN) == 0)
			receptACKA(inPacket);
	else if(strncmp(inPacket.instructHead, "RSTT", HEAD_LEN) == 0)
			receptRSTT(inPacket, automation);
	else if(strncmp(inPacket.instructHead, "DEPL", HEAD_LEN) == 0)
			receptDEPL(inPacket, automation);
	else if(strncmp(inPacket.instructHead, "MODE", HEAD_LEN) == 0)
			receptMODE(inPacket, automation);
	else if(strncmp(inPacket.instructHead, "MOVE", HEAD_LEN) == 0)
			receptMOVE(inPacket, automation);
	else if(strncmp(inPacket.instructHead, "SEQU", HEAD_LEN) == 0)
			receptSEQU(inPacket, automation);
	else if(strncmp(inPacket.instructHead, "DROP", HEAD_LEN) == 0)
			receptDROP(inPacket);
	else if(strncmp(inPacket.instructHead, "EADR", HEAD_LEN) == 0)
			receptEADR(inPacket);
	else if(strncmp(inPacket.instructHead, "TADR", HEAD_LEN) == 0)
			receptTADR(inPacket);
	else if(strncmp(inPacket.instructHead, "NCOM", HEAD_LEN) == 0)
			receptNCOM(inPacket);

	if(automation->isNewPosition() && m_foundReceptor)
	{
		sendCPOS(m_receptor.address, automation->codeScanner()->getCurrentPosCode());
		automation->setIsNewPosition(false);
	}

	if(automation->battery()->batteryNeedCharging())
		addAlert(LowBattery);
}

task RfManager::intTask(void)
{
	if(syst->interrupts()->list("rfReceived")->triggered)
	{
		m_receivedPacket = true;
		syst->interrupts()->list("rfReceived")->triggered = false;
	}
}

task RfManager::timerTask(void)
{
	if(!m_foundAddress)
	{
		++m_timeSinceAddrTakingRequest;
		return;
	}

	if(!m_connectedReceptor)
		++m_timeSinceConnRequest;
	else
	{
		if(m_receptor.awaitingPong)
			++m_receptor.timeSincePing;

		++m_receptor.timeSinceLastMsg;
	}

	for(uint8_t i = 0; i < m_alertList.size(); ++i)
	{
		if(!m_alertList.at(i).acknowledged)
			++m_alertList.at(i).timeSinceLastNotif;
	}
}

RfPacket RfManager::extractPacket(void)
{
	std::string packetBuffer;
	RfPacket returnPacket;

	packetBuffer = m_buffer.substr(m_buffer.find("<"), (m_buffer.find(">") - m_buffer.find("<")));
	m_buffer.erase(m_buffer.find("<"), (m_buffer.find(">") - m_buffer.find("<")));

	returnPacket.senderAddr[0] = 0; //Adding null-termination
	returnPacket.targetAddr[0] = 0; //Adding null-termination
	returnPacket.instructHead[0] = 0; //Adding null-termination
	returnPacket.instructBody[0] = 0; //Adding null-termination

	strncpy(returnPacket.senderAddr, packetBuffer.substr(PACKET_SENDER_ADDR_IDX, MAX_ADDR_LEN).c_str(), MAX_ADDR_LEN);
	strncpy(returnPacket.targetAddr, packetBuffer.substr(PACKET_TARGET_ADDR_IDX, MAX_ADDR_LEN).c_str(), MAX_ADDR_LEN);
	strncpy(returnPacket.instructHead, packetBuffer.substr(PACKET_HEAD_IDX, HEAD_LEN).c_str(), HEAD_LEN);
	strncpy(returnPacket.instructBody, packetBuffer.substr(PACKET_BODY_IDX, BODY_LEN).c_str(), MAX_ADDR_LEN);

	return returnPacket;
}

void RfManager::sendPacket(RfPacket packet)
{
	char packetStr[PACKET_LEN];

	memset(packetStr, 0, PACKET_LEN);

	packetStr[0] = '<';
	memcpy(packetStr + PACKET_SENDER_ADDR_IDX, packet.senderAddr, MAX_ADDR_LEN);
	memcpy(packetStr + PACKET_TARGET_ADDR_IDX, packet.targetAddr, MAX_ADDR_LEN);
	memcpy(packetStr + PACKET_HEAD_IDX, packet.instructHead, HEAD_LEN);
	memcpy(packetStr + PACKET_BODY_IDX, packet.instructBody, BODY_LEN);
	packetStr[PACKET_LEN - 1] = '>';

	sendData(packet.targetAddr, packetStr);
}

void RfManager::setCommDeviceType(char deviceType)
{
	if(deviceType == DEVICE_TYPE_NOT_INIT || deviceType == DEVICE_TYPE_RECEPT || deviceType == DEVICE_TYPE_UNIT)
		m_commDeviceType = deviceType;
}

void RfManager::setCommID(char ID)
{
	m_commId = ID;
}

void RfManager::setInit(bool data)
{
	m_init = data;
}

void RfManager::setFoundAddress(bool data)
{
	m_foundAddress = data;
}

void RfManager::setFoundReceptor(bool data)
{
	m_foundReceptor = data;
}

void RfManager::setConnectedReceptor(bool data)
{
	m_connectedReceptor = data;
}

void RfManager::setSentInvalidPacket(bool data)
{
	m_sentInvalidPacket = data;
}

void RfManager::setMainAddress(char* addr)
{
	strcpy(m_mainAddr, addr);
}

void RfManager::setAddressingAddress(char* addr)
{
	strcpy(m_addressingAddr, addr);
}

Receptor* RfManager::receptor(void)
{
	return &m_receptor;
}

char RfManager::getCommDeviceType(void) const
{
	return m_commDeviceType;
}

char RfManager::getCommID(void) const
{
	return m_commId;
}

void RfManager::getMainAddress(char* outData)
{
	memcpy(outData, m_mainAddr, sizeof m_mainAddr);
}

void RfManager::getAddressingAddress(char* outData)
{
	memcpy(outData, m_addressingAddr, sizeof m_addressingAddr);
}

bool RfManager::isInit(void) const
{
	return m_init;
}

bool RfManager::foundAddress(void) const
{
	return m_foundAddress;
}

bool RfManager::foundReceptor(void) const
{
	return m_foundReceptor;
}

bool RfManager::connectedReceptor(void) const
{
	return m_connectedReceptor;
}

bool RfManager::receivedPacket(void) const
{
	return m_receivedPacket;
}

bool RfManager::sentInvalidPacket(void) const
{
	return m_sentInvalidPacket;
}

void RfManager::receptPING(RfPacket packet)
{
	char replyAddress[getAddrLen() + 1] = "";

	memcpy(replyAddress, packet.instructBody, getAddrLen());

	sendPONG(replyAddress);
}

void RfManager::receptPONG(RfPacket packet)
{
	if(strncmp(m_receptor.address, packet.instructBody, getAddrLen()) == 0)
		m_receptor.awaitingPong = false;
}

void RfManager::receptSCON(RfPacket packet)
{
	if(!m_foundAddress || m_connectedReceptor)
		return;

	snprintf(m_receptor.address, getAddrLen(), packet.instructBody);
	m_receptor.timeSinceLastMsg = 0;

	m_foundReceptor = true;
	m_connectedReceptor = true;
}

void RfManager::receptBCON(RfPacket packet)
{
	if(!(strncmp(m_sweepingAddr, packet.senderAddr, getAddrLen()) == 0))
			return;

	switch(m_sweepingAddr[ADDR_COMM_ID_IDX])
	{
		case '9':
			m_sweepingAddr[ADDR_COMM_ID_IDX] = 'A';
		break;
		case 'Z':
			m_sweepingAddr[ADDR_COMM_ID_IDX] = 'a';
		break;
		case 'z':
			m_sweepingAddr[ADDR_COMM_ID_IDX] = '0';
		break;
		default:
			++m_sweepingAddr[ADDR_COMM_ID_IDX];
		break;
	}

	sendNCON(m_sweepingAddr);

	m_timeSinceConnRequest = 0;
}

void RfManager::receptTPOS(RfPacket packet, AutomationManager* automation)
{
	Instruction instruct;

	if(strncmp(packet.senderAddr, m_receptor.address, getAddrLen()) != 0)
		return;

	instruct.posCode.code[0]  = packet.instructBody[0] - '0';
	instruct.posCode.code[1]  = packet.instructBody[1] - '0';
	instruct.posCode.code[2]  = packet.instructBody[2] - '0';
	instruct.command = (command_t)packet.instructBody[3];
	instruct.commandSpec = packet.instructBody[4];

	automation->addInstruction(instruct);
}

void RfManager::receptACKA(RfPacket packet)
{
	for(uint8_t i = 0; i < m_alertList.size(); ++i)
	{
		if(m_alertList[i].alertCode == packet.instructBody[0])
		{
			m_alertList[i].acknowledged = true;
			return;
		}
	}
}

void RfManager::receptRSTT(RfPacket packet, AutomationManager* automation)
{
	char stateArray[BODY_LEN + 1] = {0};

	stateArray[0] = (char)automation->getOpMode();
	stateArray[1] = (char)automation->getStateSeqFetch();
	stateArray[2] = (char)automation->getInstruction().command;
	stateArray[3] = (char)automation->getInstruction().commandSpec;
	memcpy(stateArray + 4, automation->getInstruction().posCode.code, sizeof(automation->getInstruction().posCode.code));

	sendCSTT(packet.senderAddr, stateArray);
}

void RfManager::receptDEPL(RfPacket packet, AutomationManager* automation)
{
	if(automation->getOpMode() == Auto)
		return;

	automation->setMotorSpeeds(packet.instructBody[0], packet.instructBody[1], packet.instructBody[2], packet.instructBody[3]);
}

void RfManager::receptMODE(RfPacket packet, AutomationManager* automation)
{
	automation->setOpMode((op_mode_t)packet.instructBody[0]);
}

void RfManager::receptMOVE(RfPacket packet, AutomationManager* automation)
{
	//TODO: Define the moveset array better eventually
}

void RfManager::receptSEQU(RfPacket packet, AutomationManager* automation)
{
	if(automation->getOpMode() != SemiAuto)
		return;

	Instruction instruct = {(command_t)packet.instructBody[0], {{'X', 'X', 'X'}}, packet.instructBody[1]};

	automation->setInstruction(instruct);
}

void RfManager::receptDROP(RfPacket packet)
{
	if(strncmp(packet.senderAddr, m_receptor.address, MAX_ADDR_LEN) == 0)
	{
		m_foundReceptor = false;
		m_connectedReceptor = false;
		strcpy(m_receptor.address, "00000");
	}
}

void RfManager::receptEADR(RfPacket packet)
{
	char wantedAddr[MAX_ADDR_LEN + 1] = "";

	if(!m_foundAddress)
		return;

	strncpy(wantedAddr, packet.instructBody, MAX_ADDR_LEN);

	if(strncmp(m_mainAddr, wantedAddr, MAX_ADDR_LEN) == 0)
	{
		sendTADR(packet.senderAddr, wantedAddr); //Address is already taken
	}
}

void RfManager::receptTADR(RfPacket packet)
{
	if(m_foundAddress || !(strncmp(packet.instructBody, m_mainAddr, getAddrLen()) == 0))
		return;

	switch(m_mainAddr[ADDR_COMM_ID_IDX])
	{
		case '9':
			m_mainAddr[ADDR_COMM_ID_IDX] = 'A';
		break;
		case 'Z':
			m_mainAddr[ADDR_COMM_ID_IDX] = 'a';
		break;
		case 'z':
			m_mainAddr[ADDR_COMM_ID_IDX] = '0';
		break;
		default:
			++m_mainAddr[ADDR_COMM_ID_IDX];
		break;
	}

	sendEADR(m_addressingAddr, m_mainAddr);

	m_timeSinceAddrTakingRequest = 0;
}

void RfManager::receptNCOM(RfPacket packet)
{
	if(strncmp(packet.targetAddr, m_mainAddr, MAX_ADDR_LEN) == 0)
		m_sentInvalidPacket = true;
}

void RfManager::sendPING(char* dest)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "PING", HEAD_LEN);
	strncpy(packetToSend.instructBody, packetToSend.targetAddr, getAddrLen());

	if(strncmp(packetToSend.targetAddr, m_receptor.address, getAddrLen()) == 0)
		m_receptor.awaitingPong = true;

	sendPacket(packetToSend);
}

void RfManager::sendPONG(char* dest)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "PONG", HEAD_LEN);
	strncpy(packetToSend.instructBody, dest, getAddrLen());

	sendPacket(packetToSend);
}

void RfManager::sendNCON(char* dest)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "NCON", HEAD_LEN);

	sendPacket(packetToSend);
}

void RfManager::sendCPOS(char* dest, PosCode code)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "CPOS", HEAD_LEN);
	sprintf(packetToSend.instructBody, "%c%c%c", (char) code.code[0] + '0', (char) code.code[1] + '0', (char) code.code[2] + '0');

	sendPacket(packetToSend);
}

void RfManager::sendABAT(char* dest)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "ABAT", HEAD_LEN);

	sendPacket(packetToSend);
}

void RfManager::sendLBAT(char* dest, float level)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "LBAT", HEAD_LEN);
	sprintf(packetToSend.instructBody, "%6.2f", level); //send percentage with 2 decimals precision

	sendPacket(packetToSend);
}

void RfManager::sendALRT(char* dest, uint8_t code)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "ALRT", HEAD_LEN);
	sprintf(packetToSend.instructBody, "%c",(char) code); //send percentage with 2 decimals precision

	sendPacket(packetToSend);
}

void RfManager::sendCSTT(char* dest, char* stateArray)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "ABAT", HEAD_LEN);
	sprintf(packetToSend.instructBody, "%s", stateArray);

	sendPacket(packetToSend);
}

void RfManager::sendSDON(char* dest, uint8_t code, uint8_t specs)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "ABAT", HEAD_LEN);
	sprintf(packetToSend.instructBody, "%c%c",(char) code, (char) specs);

	sendPacket(packetToSend);
}

void RfManager::sendDROP(char* dest)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "ABAT", HEAD_LEN);

	sendPacket(packetToSend);

	if(strncmp(dest, m_receptor.address, MAX_ADDR_LEN) == 0)
	{
		m_foundReceptor = false;
		m_connectedReceptor = false;
		strcpy(m_receptor.address, "00000");
	}
}

void RfManager::sendEADR(char* dest, char* addr)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "EADR", HEAD_LEN);
	strncpy(packetToSend.instructBody, addr, getAddrLen());

	sendPacket(packetToSend);
}

void RfManager::sendTADR(char* dest, char* addr)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_addressingAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "TADR", HEAD_LEN);
	strncpy(packetToSend.instructBody, addr, getAddrLen());

	sendPacket(packetToSend);
}

void RfManager::sendNCOM(char* dest)
{
	RfPacket packetToSend = {"", "", "", ""};

	strncpy(packetToSend.targetAddr, dest, getAddrLen());
	strncpy(packetToSend.senderAddr, m_mainAddr, getAddrLen());
	strncpy(packetToSend.instructHead, "NCOM", HEAD_LEN);

	sendPacket(packetToSend);
}
