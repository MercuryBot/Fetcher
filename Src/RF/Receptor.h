/**
* @file Receptor.h
* @brief Header file for the Receptor structure
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 3rd, 2018
*
* The Receptor struct describes the basic structure of a connected Receptor
*
*/

#ifndef RECEPTOR_H_
#define RECEPTOR_H_

#include "ControlRf.hpp"

/**
* @struct Receptor
* @brief Structure representing a basic Receptor attribute to a network
*/
struct Receptor {

	/**
	* @brief Address of the Receptor
	*/
	char address[MAX_ADDR_LEN + 1];


	/**
	* @brief Time since the last message
	*/
	uint32_t timeSinceLastMsg;

	/**
	* @brief Confirms if the Receptor is waiting for a pong response from the Receptor
	*/
	bool awaitingPong;

	/**
	* @brief Time since the last ping message sent
	*/
	uint32_t timeSincePing;
};

#endif /* RECEPTOR_H_ */
