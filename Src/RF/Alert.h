/**
* @file Alert.h
* @brief Header file for the Alert structure
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 3rd, 2018
*
* The alert struct lists the characteristics of an alert
*/

#ifndef ALERT_H_
#define ALERT_H_

#include "Alerts.h"

/**
 * @struct Alert
 * @brief Lists the characteristics of an alert
 */
struct Alert {

	/**
	 * @brief Corresponding code of the alert
	 */
	alert_t alertCode;

	/**
	 * @brief Confirmation if the alert has been acknowledged
	 */
	bool acknowledged;

	/**
	 * @brief Time since the last notification to the receptor
	 */
	uint32_t timeSinceLastNotif;
};

#endif /* ALERT_H_ */
