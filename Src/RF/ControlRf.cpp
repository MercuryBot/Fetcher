/**
* @file ControlRf.cpp
* @brief Header file for the ControlRf class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @author Jean-Christophe Lepage (mailto:jean.christophe.lepage03@gmail.com)
* @version 1.0.0
* @date March 3rd, 2018
*
* The ControlRf class provides control over the nRF24L01+ module's registers
*
*/

#include "ControlRf.hpp"

ControlRf::ControlRf()
{
	m_paLevel = 0;
	m_dataPipsNum = 0;
	m_addrLen = 0;
	m_dataRate = 0;
	m_payloadLen = 0;
	m_retryCount = 0;
	m_retryDelay = 0;
	m_opMode = 0;
	m_currentConfigMsk = 0;
	
	m_channelFreq = MIN_CHANNEL_FREQ_MHZ;
		
	m_autoAck = 0;
	
	memset(m_txAddr, 0, MAX_ADDR_LEN);
	
	for(uint8_t i = 0; i < MAX_DATA_PIPES; ++i)
	{
		memset(m_rxAddrPipe[i], 0, MAX_ADDR_LEN + 1);
		m_isPipeActive[i] = false;
	}
}

ControlRf::~ControlRf() {}

void ControlRf::readReg(uint8_t addr, size_t len, uint8_t* a_outData)
{	
	DWT_Delay_us(10);
	syst->SPI()->toggleCSNPin(0);
	DWT_Delay_us(10);
	
	syst->SPI()->exchData_8b(R_REGISTER + addr);
	
	DWT_Delay_us(10);
	
	for(uint8_t i = 0; i < len; ++i)
	{
		a_outData[i] = syst->SPI()->exchData_8b(RF24_NOP);
		DWT_Delay_us(10);
	}
	
	syst->SPI()->toggleCSNPin(1);
}

void ControlRf::writeReg(uint8_t addr, uint8_t* a_data, size_t len)
{
	DWT_Delay_us(10);
	syst->SPI()->toggleCSNPin(0);
	DWT_Delay_us(10);
	
	if(addr == FLUSH_TX || addr == W_TX_PAYLOAD)
		syst->SPI()->exchData_8b(R_REGISTER + addr);
	else
		syst->SPI()->exchData_8b(W_REGISTER + addr);
	
	DWT_Delay_us(10);
	
	for(uint8_t i = 0; i < len; ++i)
	{
		syst->SPI()->exchData_8b(a_data[i]);
		DWT_Delay_us(10);
	}
	
	syst->SPI()->toggleCSNPin(1);
}


void ControlRf::writeReg(uint8_t addr, uint8_t data)
{
	DWT_Delay_us(10);
	syst->SPI()->toggleCSNPin(0);
	DWT_Delay_us(10);
	
	if(addr == FLUSH_TX || addr == W_TX_PAYLOAD)
		syst->SPI()->exchData_8b(R_REGISTER + addr);
	else
		syst->SPI()->exchData_8b(W_REGISTER + addr);
	
	DWT_Delay_us(10);
	syst->SPI()->exchData_8b(data);
	DWT_Delay_us(10);
	
	syst->SPI()->toggleCSNPin(1);
}

void ControlRf::resetRF()
{
	DWT_Delay_us(10);
	syst->SPI()->toggleCSNPin(0);
	DWT_Delay_us(10);
	writeReg(NRF_STATUS, 0x70);
	DWT_Delay_us(20);
	syst->SPI()->toggleCSNPin(1);
}

void ControlRf::transmitMsg(uint8_t* a_data, size_t len)
{	
	writeReg(FLUSH_TX, a_data, 0);
	writeReg(W_TX_PAYLOAD, a_data, len);
	
	HAL_Delay(10);
	syst->SPI()->toggleCEPin(1);
	DWT_Delay_us(20);
	syst->SPI()->toggleCEPin(0);
	HAL_Delay(10);
	
	resetRF();
}

void ControlRf::receiveMsg(uint8_t* a_outData)
{	
	readReg(R_RX_PAYLOAD, m_payloadLen, a_outData);
}

void ControlRf::setPALevel(uint8_t level)
{
	if(level != PALEVEL1 && level != PALEVEL2 && level != PALEVEL3 && level != PALEVEL4)
		return;
		
	m_paLevel = level;
	
	writeReg(RF_SETUP, (m_paLevel | m_dataRate));
}

void ControlRf::setAutoAck(bool data)
{
	m_autoAck = data;
	
	if(data)
		writeReg(EN_AA, 0x3F);
	else
		writeReg(EN_AA, 0x00);
}

void ControlRf::setDataPipsNum(uint8_t num)
{
	m_dataPipsNum = ud_std::clamp(num, MIN_DATA_PIPES, MAX_DATA_PIPES);
}

void ControlRf::setAddrLen(uint8_t len)
{		
	m_addrLen = ud_std::clamp(len, MIN_ADDR_LEN, MAX_ADDR_LEN);

	writeReg(SETUP_AW, (m_addrLen - 2));
}

void ControlRf::setChannelFreqMHZ(uint16_t freq)
{	
	m_channelFreq = ud_std::clamp(freq, MIN_CHANNEL_FREQ_MHZ, MAX_CHANNEL_FREQ_MHZ);
	
	writeReg(RF_CH, (m_channelFreq - MIN_CHANNEL_FREQ_MHZ));
}

void ControlRf::setDataRate(uint8_t rate)
{
	if(rate != DATARATE_250KBPS && rate != DATARATE_1MBPS && rate != DATARATE_2MBPS)
		return;
	
	m_dataRate = rate;
	
	writeReg(RF_SETUP, (m_paLevel | m_dataRate));
}

void ControlRf::setPayloadLen(uint8_t len)
{
	m_payloadLen = ud_std::clamp(len, MIN_PL_LEN, MAX_PL_LEN);
		
	writeReg(RX_PW_P0, m_payloadLen);
	writeReg(RX_PW_P1, m_payloadLen);
	writeReg(RX_PW_P2, m_payloadLen);
	writeReg(RX_PW_P3, m_payloadLen);
	writeReg(RX_PW_P4, m_payloadLen);
	writeReg(RX_PW_P5, m_payloadLen);
}

void ControlRf::setRetryCount(uint8_t count)
{
	m_retryCount = ud_std::clamp(count, MIN_RETRY_CNT, MAX_RETRY_CNT);

	writeReg(SETUP_RETR, (((uint8_t)((m_retryDelay  - MIN_RETRY_DELAY_US)/RETRY_DELAY_STEP_US)<<4) | m_retryCount));
}

void ControlRf::setRetryDelay(uint16_t delay)
{
	m_retryDelay = ud_std::clamp(delay, MIN_RETRY_DELAY_US, MAX_RETRY_DELAY_US);
	
	writeReg(SETUP_RETR, (((uint8_t)((m_retryDelay  - MIN_RETRY_DELAY_US)/RETRY_DELAY_STEP_US)<<4) | m_retryCount));
}

void ControlRf::enableDataPips(bool* dataPipes)
{
	uint8_t tmpReg;
	
	for(uint8_t i = 0; i < MAX_DATA_PIPES; ++i)
		m_isPipeActive[i] = dataPipes[i];
	
	tmpReg  = (m_isPipeActive[5]<<ERX_P5) | 
			  (m_isPipeActive[4]<<ERX_P4) | 
			  (m_isPipeActive[3]<<ERX_P3) |
			  (m_isPipeActive[2]<<ERX_P2) | 
			  (m_isPipeActive[1]<<ERX_P1) |
			  (m_isPipeActive[0]<<ERX_P0);
			  
	writeReg(EN_RXADDR, tmpReg);
}

void ControlRf::enableDataPipe(uint8_t idx)
{	
	uint8_t tmpReg;
	
	idx = ud_std::clamp(idx, 0, (MAX_DATA_PIPES - 1));
	m_isPipeActive[idx] = true;
	
	tmpReg  = (m_isPipeActive[5]<<ERX_P5) |
			  (m_isPipeActive[4]<<ERX_P4) |
			  (m_isPipeActive[3]<<ERX_P3) |
			  (m_isPipeActive[2]<<ERX_P2) |
			  (m_isPipeActive[1]<<ERX_P1) |
			  (m_isPipeActive[0]<<ERX_P0);
	
	writeReg(EN_RXADDR, tmpReg);
}

void ControlRf::disableDataPipe(uint8_t idx)
{
	uint8_t tmpReg;
	
	idx = ud_std::clamp(idx, 0, (MAX_DATA_PIPES - 1));
	m_isPipeActive[idx] = false;
	
	tmpReg  = (m_isPipeActive[5]<<ERX_P5) |
		  	  (m_isPipeActive[4]<<ERX_P4) |
			  (m_isPipeActive[3]<<ERX_P3) |
			  (m_isPipeActive[2]<<ERX_P2) |
			  (m_isPipeActive[1]<<ERX_P1) |
			  (m_isPipeActive[0]<<ERX_P0);
	
	writeReg(EN_RXADDR, tmpReg);
}

void ControlRf::setListeningPipe(uint8_t idx, char* pipeAddr)
{
	if(idx > (MAX_DATA_PIPES - 1))
		return;
	
	strcpy(m_rxAddrPipe[idx], pipeAddr);
	
	switch(idx)
	{
		case 0:
			writeReg(RX_ADDR_P0, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 1:
			writeReg(RX_ADDR_P1, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 2:
			writeReg(RX_ADDR_P2, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 3:
			writeReg(RX_ADDR_P3, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 4:
			writeReg(RX_ADDR_P4, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 5:
			writeReg(RX_ADDR_P5, (uint8_t*)pipeAddr, m_addrLen);
		break;
	}
}

void ControlRf::setListeningPipeAtPos(uint8_t idx, char car, uint8_t pos)
{
	char pipeAddr[MAX_ADDR_LEN] = "";
	
	if(idx > (MAX_DATA_PIPES - 1))
		return;
	
	m_rxAddrPipe[idx][pos] = car;	
	strcpy(pipeAddr, m_rxAddrPipe[idx]);
	
	switch(idx)
	{
		case 0:
			writeReg(RX_ADDR_P0, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 1:
			writeReg(RX_ADDR_P1, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 2:
			writeReg(RX_ADDR_P2, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 3:
			writeReg(RX_ADDR_P3, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 4:
			writeReg(RX_ADDR_P4, (uint8_t*)pipeAddr, m_addrLen);
		break;
		case 5:
			writeReg(RX_ADDR_P5, (uint8_t*)pipeAddr, m_addrLen);
		break;
	}	
}

void ControlRf::setSendingPipe(char* pipeAddr)
{
	memcpy(m_txAddr, pipeAddr, m_addrLen);
	writeReg(TX_ADDR, (uint8_t*)pipeAddr, m_addrLen);
}

void ControlRf::setOpMode(bool mode)
{
	if(mode) //RX
	{
		syst->SPI()->toggleCEPin(1); //stop listening
		m_currentConfigMsk |= (1<<PRIM_RX);
	}
	else
	{
		syst->SPI()->toggleCEPin(0); //stop listening
		m_currentConfigMsk &= ~(1<<PRIM_RX);
	}
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

void ControlRf::setPwr(bool mode)
{
	if(mode)
		m_currentConfigMsk |= (1<<PWR_UP);
	else
		m_currentConfigMsk &= ~(1<<PWR_UP);
		
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

void ControlRf::setCrcEnc(bool mode)
{
	if(mode)
		m_currentConfigMsk |= (1<<CRCO);
	else
		m_currentConfigMsk &= ~(1<<CRCO);
		
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

void ControlRf::setEnablingCRC(bool mode)
{
	 if(mode)
	 	m_currentConfigMsk |= (1<<EN_CRC);
	else
	 	m_currentConfigMsk &= ~(1<<EN_CRC);
	 	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

void ControlRf::setIntRt(bool mode)
{
	if(mode)
		m_currentConfigMsk |= (1<<MASK_MAX_RT);
	else
		m_currentConfigMsk &= ~(1<<MASK_MAX_RT);
		
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

void ControlRf::setIntTx(bool mode)
{
	if(mode)
		m_currentConfigMsk |= (1<<MASK_TX_DS);
	else
		m_currentConfigMsk &= ~(1<<MASK_TX_DS);
		
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

void ControlRf::setIntRx(bool mode)
{
	if(mode)
		m_currentConfigMsk |= (1<<MASK_RX_DR);
	else
		m_currentConfigMsk &= ~(1<<MASK_RX_DR);
	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);	
}

void ControlRf::setConfig(uint8_t config)
{
	m_currentConfigMsk = config;
	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

void ControlRf::startListening(void)
{
	syst->SPI()->toggleCEPin(1);
	DWT_Delay_us(10);
}

void ControlRf::stopListening(void)
{
	syst->SPI()->toggleCEPin(0);
	DWT_Delay_us(10);
}

void ControlRf::getPipAddr(uint8_t idx, char* outData)
{
	memcpy(outData, m_rxAddrPipe[idx], m_addrLen);
}

uint8_t ControlRf::getPaLevel(void) const
{
	return m_paLevel;
}

uint8_t ControlRf::getDataPipsNum(void) const
{
	return m_dataPipsNum;
}

uint8_t ControlRf::getAddrLen(void) const
{
	return m_addrLen;
}

uint8_t ControlRf::getDataRate(void) const
{
	return m_dataRate;
}

uint8_t ControlRf::getPayloadLen(void) const
{
	return m_payloadLen;
}

uint8_t ControlRf::retryCount(void) const
{
	return m_retryCount;
}

uint8_t ControlRf::retryDelay(void) const
{
	return m_retryDelay;
}

uint16_t ControlRf::getChannelFreq(void) const
{
	return m_channelFreq;
}

bool ControlRf::isAutoAck(void) const
{
	return m_autoAck;
}

bool ControlRf::isPipeActive(uint8_t idx) const
{
	if(idx > MAX_DATA_PIPES - 1)
		return false;
	
	return m_isPipeActive[idx];
}

void ControlRf::outDetails(void (*printOut) (const char*))
{
	uint8_t readRegTmp[5];
	char printStr[32];
	
	readReg(NRF_STATUS, 1, readRegTmp);
	sprintf(printStr, "NRF_STATUS: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(NRF_CONFIG, 1, readRegTmp);
	sprintf(printStr, "NRF_CONFIG: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(RX_ADDR_P0, 5, readRegTmp);
	sprintf(printStr, "RX_ADDR_P0: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x \r\n", readRegTmp[0], readRegTmp[1], readRegTmp[2], readRegTmp[3], readRegTmp[4]);
	printOut(printStr);

	readReg(RX_PW_P0, 1, readRegTmp);
	sprintf(printStr, "RX_PW_P0: 0x%02x  \r\n", readRegTmp[0]);
	printOut(printStr);

	readReg(TX_ADDR, 5, readRegTmp);
	sprintf(printStr, "TX_ADDR: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x \r\n", readRegTmp[0], readRegTmp[1], readRegTmp[2], readRegTmp[3], readRegTmp[4]);
	printOut(printStr);
	
	readReg(EN_AA, 1, readRegTmp);
	sprintf(printStr, "EN_AA: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(EN_AA, 1, readRegTmp);
	sprintf(printStr, "EN_AA: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(SETUP_AW, 1, readRegTmp);
	sprintf(printStr, "SETUP_AW: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(SETUP_RETR, 1, readRegTmp);
	sprintf(printStr, "SETUP_RETR: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(EN_RXADDR, 1, readRegTmp);
	sprintf(printStr, "EN_RXADDR: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(RF_CH, 1, readRegTmp);
	sprintf(printStr, "RF_CH: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(RF_SETUP, 1, readRegTmp);
	sprintf(printStr, "RF_SETUP: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(DYNPD, 1, readRegTmp);
	sprintf(printStr, "DYNPD: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(FEATURE, 1, readRegTmp);
	sprintf(printStr, "FEATURE: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
}
